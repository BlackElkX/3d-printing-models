translate([  0, 20, 0]) rotate([-90, 0, 0]) tekst("Myst", 1, 10);
translate([-20,  0, 0]) rotate([  0, 0, 0]) fallingMan(0.1,  0.1, 1, false);
translate([-80,  0, 0]) rotate([  0, 0, 0]) fallingMan(0.15, 0.1, 1, false);
translate([ 20,  0, 0]) rotate([  0, 0, 0]) feather(0.1,     0.1, 1, false);
translate([ 80,  0, 0]) rotate([  0, 0, 0]) feather(0.15,    0.1, 1, false);
//translate([44.1, 0, 0]) cube();

module tekst(texttoprint, fontStart1, fontSize1) {
  /*
    -🎜- 2x achtste noten omhoog
    -🎝- 2x achtste noten omlaag
    -🎵- 1x achtste noot
    -🎶- 3x achtste noten
    -🎹- pianoklavier
    -🎼- solsleutel
    -🎘- midi-keyboard
    -- gamecontroller
    -🍐- peer
    -🍒- kersen
    -🍓- aardbij
    -📷- fototoestel
    -🐑- schaap
    -🎻- altviool
    -🍎- appel
    -🍏- appel
    -🐧- pinguin
    -🐎- paard1
    -🏇- paard2
    -🐴- paard3
    -🎠- paard4
  */

  fontName1    = "GhotiMystFont:style=Bold";
  fontHeight1  = 0;
  fontBottom1  = 0;
  fontDepth1   = 1;
  labelWidth   = 0;
  color("cyan")
    rotate([90, 0, 0])
      translate([fontStart1, fontBottom1, labelWidth - fontDepth1])
        linear_extrude(fontHeight1 + fontDepth1)
          scale([1, 1])
            text(texttoprint, font = fontName1, size = fontSize1);
}

module fallingMan1(size, dikte, rotatie) {
  diffCubeSize = 100 * (size * 6);
  color("yellow")
    translate([1.7, 0, 01])
      difference() {
        rotate([90, rotatie, 180])
          scale([size, size, dikte])
            surface(file = "../logos/zw-fallenman.png", invert = true, center = true);
        translate([-diffCubeSize / 2, -19, -diffCubeSize / 2])
          cube([diffCubeSize, 11, diffCubeSize]);
      }
}
module fallingMan(size, dikte, hoogte, invert) {
  diffCubeSize = 100 * (size * 6);
  color("yellow")
    translate([0, 0, -0.1])
      difference() {
        scale([size, size, dikte])
          surface(file = "../logos/zw-fallenman1.png", invert = invert, center = true);
      translate([0, 0, -4.9])
        cube([diffCubeSize, diffCubeSize, 10], true);
      translate([0, 0, 5.1 + hoogte])
        cube([diffCubeSize, diffCubeSize, 10], true);
    }
}

module feather(size, dikte, hoogte, invert) {
  diffCubeSize = 100 * (size * 6);
  color("green")
    translate([0, 0, -0.1])
      difference() {
        scale([size, size, dikte])
          surface(file = "../logos/feather.png", invert = invert, center = true);
      translate([0, 0, -4.9])
        cube([diffCubeSize, diffCubeSize, 10], true);
      translate([0, 0, 5.1 + hoogte])
        cube([diffCubeSize, diffCubeSize, 10], true);
    }
}
