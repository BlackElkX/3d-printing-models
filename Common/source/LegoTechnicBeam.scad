/*      
  Customizable LEGO Technic Beam on Parametric LEGO Technic Beam 
  by "projunk" www.thingiverse.com/thing:203935
  and Even More Customizable Straight Beam for LEGO Technic 
  by "samkass" www.thingiverse.com/thing:1119651
  and Lego Technic: Perpendicular Beam Connector  Customizable
  by "shusy" www.thingiverse.com/thing:2503065
  Modified by Shusy
  September 2017
*/
$fn    = 90;

factor = 1;

eps    = 0.05 * factor;

correctie = 0.5;                           //         //speling uitzet dingesls printen

//Nuber of the holes
NrOfHolesX       = 8; 
NrOfHolesY       = 8;
NrOfHolesAngular = 8;        //Even number, please
SupportHeight    = 0;
Diameter1        = 4.8  + correctie;                  //binnenkant (de gaten)
Diameter1Plus    = 4.6  - correctie;                  //de plus balken
Diameter2        = 6.0;//  + correctie;               //6.2 - bovenkant (uitsparing voor de klipkes)
Pitch            = 8.00 * factor;           //ok      //afstand tussen de gaten
Step             = 6;         // [1:3] //12 //ok      //aantal gaten die de schuine balk (45 deg) zou hebben in het lange gat
Height           = 7.8  * factor;
Width            = 7.35 * factor;//7.33
Depth            = 1.24 * factor;                     //start van binnengat
MidThickness     = 2    * factor;
MiniCubeOffset   = 1.1  * factor;
CrossInnerRadius = 1.5  * factor;

roundBeam(3);

//legoBeam45degrees3pieces(NrOfHolesX, NrOfHolesY, NrOfHolesAngular, SupportHeight, Diameter1, Diameter1Plus, Diameter2, Pitch, Step, Height, Width, Depth, MidThickness, MiniCubeOffset, CrossInnerRadius, 1, false, true);

//legoBeamCube(5, Diameter1, Diameter2, Pitch, Height, Width, 1, false);

module roundBeam(holes, heightFactor = 1, noholes = false) {
//  legoBeamRound(holes, Diameter1, Diameter2, Pitch, Height / heightFactor, Width, noholes);
  legoBeamRound(holes, Diameter1, Diameter2, Pitch, Height, Width, heightFactor, noholes);
}

module legoBeamCube(NrOfHoles, Diameter1, Diameter2, Pitch, Height, Width, heightFactor = 1, noholes = false) {
  Radius1          = Diameter1 / 2;
  Radius2          = Diameter2 / 2;
  drawCubeBeam(NrOfHoles, false, false, 0, Radius1, Radius2, Pitch, Height / heightFactor, Width, noholes);
}

module legoBeamRound(NrOfHolesX, Diameter1, Diameter2, Pitch, Height, Width, heightFactor = 1, noholes = false) {
  Radius1          = Diameter1 / 2;
  Radius2          = Diameter2 / 2;
  echo(Height);
  echo(heightFactor);
  drawBeam(NrOfHolesX, false, false, 0, Radius1, Radius2, Pitch, Height / heightFactor, Width, noholes);
}

module legoBeam45degrees3pieces(NrOfHolesX, NrOfHolesY, NrOfHolesAngular, SupportHeight, Diameter1, Diameter1Plus, Diameter2, Pitch, Step, Height, Width, Depth, MidThickness, MiniCubeOffset, CrossInnerRadius, heightFactor = 1, noholes = false, flat = false) {
  Radius1          = Diameter1 / 2;
  Radius1Plus      = Diameter1Plus / 2;
  Radius2          = Diameter2 / 2;
  MiniCubeWidth    = Radius1 - MiniCubeOffset;

  translate([Pitch * Step, 0, 0])
    drawBeam(NrOfHolesX, false, false, SupportHeight, Radius1, Radius2, Pitch, Height / heightFactor, Width, noholes, flat);
  translate([0, Pitch * Step, 0])
    rotate([0, 0, 90]) 
      drawBeam(NrOfHolesY, false, false, SupportHeight, Radius1, Radius2, Pitch, Height / heightFactor, Width, noholes, flat);
  angularDrawBeam(Step + 1, Radius1, Radius2, Pitch, Width, heightFactor, noholes, flat);
}

module drawSupports(NrOfHoles, firstPlus, lastPlus, lengthPlus, Pitch, Width) { 
  translate([0, -Width / 2, 0]) {
   if (firstPlus) {
     if (lengthPlus > 1) {
        translate([(1 - 1) * Pitch, Width / 2, 0])
          axlePlusRounded(lengthPlus);
      }
    }
    if (lastPlus) {
      if (lengthPlus > 1) {
        translate([(NrOfHoles - 1) * Pitch, Width / 2, 0])
          axlePlusRounded(lengthPlus);
      }
    }
  }
}

module drawBeamDefault(NrOfHoles, firstPlus, lastPlus, lengthPlus, heightFactor = 1, noholes = false, flat = false) {
  Radius1          = Diameter1 / 2;
  Radius2          = Diameter2 / 2;
  drawBeam(NrOfHoles, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height / heightFactor, Width, noholes, flat);
}

module drawBeam(NrOfHoles, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes = false, flat = false) {
  echo(Height);
  drawBeamHigh(NrOfHoles, 1, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes, flat);
}

module drawBeamHigh(NrOfHoles, beamHeight, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes = false, flat = false) {
  echo(Height);
  drawSupports(NrOfHoles, firstPlus, lastPlus, lengthPlus, Pitch, Width);
  translate([0, -Width / 2, 0]) {
    union() {
      Length    = (NrOfHoles - 1) * Pitch;
      Thickness = (Width - 2 * Radius2) / 2;
      difference() {
        union() {
          cube([Length, Thickness, beamHeight * Height]);
          translate([0, Width - Thickness, 0])
            cube([Length, Thickness, beamHeight * Height]);
          translate([0, 0, Height / 2 - MidThickness / 2])
            cube([Length, Width, MidThickness]);
          if (flat) {
            hull() {
              translate([(0) * Pitch, Width / 2, 0])
                cylinder(r = Width / 2, h = beamHeight * Height);
              translate([(NrOfHoles - 1) * Pitch, Width / 2, 0])
                cylinder(r = Width / 2, h = beamHeight * Height);
              
            }
          } else {
            for (i = [1 : NrOfHoles]) {
              translate([(i - 1) * Pitch, Width / 2, 0]) {
                cylinder(r = Width / 2, h = beamHeight * Height);
              }
            }
          }
        }
        //holes
        if (!noholes) {
          union() {
            for (i = [1 : NrOfHoles]) {
              if (i == 1 && firstPlus) {
                if (lengthPlus <= 1) {
                  translate([(i - 1) * Pitch, Width / 2, 0])
                    plus();
                }
              } else if (i == NrOfHoles && lastPlus) {
                if (lengthPlus <= 1) {
                  translate([(i - 1) * Pitch, Width / 2, 0])
                    plus();
                }
              } else {
                translate([(i - 1) * Pitch, Width / 2, 0]) {
                  translate([0, 0, -eps])
                    cylinder(r = Radius2, h = Depth + eps);
                  translate([0, 0, -eps])
                    cylinder(r = Radius1, h = beamHeight * Height + 2 * eps);
                  translate([0, 0, Height - Depth])
                    cylinder(r = Radius2, h = Depth + eps);
                }
              }
            }
          }
        }
      }
    }
  }
}

module drawCubeBeam(NrOfHoles, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes = false) {
  drawCubeBeamHigh(NrOfHoles, 1, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes);
}

module drawCubeBeamHigh(NrOfHoles, beamHeight, firstPlus, lastPlus, lengthPlus, Radius1, Radius2, Pitch, Height, Width, noholes = false) {
  drawSupports(NrOfHoles, firstPlus, lastPlus, lengthPlus, Pitch, Width);
  translate([0, -Width / 2, 0]) {
    union() {
      Length    = (NrOfHoles - 1) * Pitch;
      Thickness = (Width - 2 * Radius2) / 2;
      difference() {
        union() {
          translate([-Width / 2, 0, 0])    
            cube([Thickness, Width, beamHeight * Height]);
           translate([(NrOfHoles * Pitch) - (Width / 2) - (Thickness * 2)  + 0.1, 0, 0])  
            cube([Thickness, Width, beamHeight * Height]);
          translate([-Width / 2, 0, 0])    
            cube([Length + Width, Thickness, beamHeight * Height]);
          translate([-Width / 2, Width - Thickness, 0])
            cube([Length + Width, Thickness, beamHeight * Height]);
          translate([-Width / 2, 0, Height / 2 - MidThickness / 2])
            cube([Length + Width, Width, MidThickness]);
          for (i = [1 : NrOfHoles]) {
            translate([(i - 1) * Pitch, Width / 2, 0]) {
              cylinder(r = Width / 2, h = beamHeight * Height);
            }
          }
        }
        //holes
        if (!noholes) {
          union() {
            for (i = [1 : NrOfHoles]) {
              if (i == 1 && firstPlus) {
                if (lengthPlus <= 1) {
                  translate([(i - 1) * Pitch, Width / 2, 0])
                    plus();
                }
              } else if (i == NrOfHoles && lastPlus) {
                if (lengthPlus <= 1) {
                  translate([(i - 1) * Pitch, Width / 2, 0])
                    plus();
                }
              } else {
                translate([(i - 1) * Pitch, Width / 2, 0]) {
                  translate([0, 0, -eps])
                    cylinder(r = Radius2, h = Depth + eps);
                  translate([0, 0, -eps])
                    cylinder(r = Radius1, h = beamHeight * Height + 2 * eps);
                  translate([0, 0, Height - Depth])
                    cylinder(r = Radius2, h = Depth + eps);
                }
              }
            }
          }
        }
      }
    }
  }
}

module drawBeamPlus(NrOfHoles, Pitch, Radius1, Radius2, Height, Width, noholes = false) {
  translate([0, -Width / 2, 0]) {
    union() {
      Length    = (NrOfHoles - 1) * Pitch;
      Thickness = (Width - 2 * Radius2) / 2;
      difference() {
        union() {      
          cube([Length, Thickness, Height]);
          translate([0, Width - Thickness, 0])
            cube([Length, Thickness, Height]);
          translate([0, 0, Height / 2 - MidThickness / 2])
            cube([Length, Width, MidThickness]);
          for (i = [1 : NrOfHoles]) {
            translate([(i - 1) * Pitch, Width / 2, 0]) {
              cylinder(r = Width / 2, h = Height);
            }
          }
        }
        //holes
        if (!noholes) {
          union() {
            for (i = [1 : NrOfHoles - 1]) {
              translate([(i - 1) * Pitch, Width / 2, 0]) {
                translate([0, 0, -eps])
                  cylinder(r = Radius2, h = Depth + eps);
                translate([0, 0, -eps])
                  cylinder(r = Radius1, h = Height + 2 * eps);
                translate([0, 0, Height - Depth])
                  cylinder(r = Radius2, h = Depth + eps);
              }
            }
            translate([(NrOfHoles - 1) * Pitch, Width / 2, 0])
              plus();
          }
        }
      }
    }
  }
}

module angularDrawBeamDefault(NrOfHoles, heightFactor = 1, noholes = false, flat = false) {
  Radius1          = Diameter1 / 2;
  Radius2          = Diameter2 / 2;
  angularDrawBeam(NrOfHoles, Radius1, Radius2, Pitch, Width, heightFactor, noholes, flat);
}

module angularDrawBeam(NrOfHoles, Radius1, Radius2, Pitch, Width, heightFactor = 1, noholes = false, flat = false) {
  Height = Height / heightFactor;
  rotate([0, 0, -45]) {
    translate([-(NrOfHoles - 1) * Pitch * sqrt(2) / 2,
                (NrOfHoles - 1) * Pitch * sqrt(2) / 2 - Width / 2,
               0]) {
      union() {
        Length    = (NrOfHoles - 1) * Pitch * sqrt(2);
        Thickness = (Width - 2 * Radius2) / 2;
        NumberOfHoles = (NrOfHoles / 2);
        difference() {
          union() {
            cube([Length, Thickness, Height]);
            translate([0, Width - Thickness, 0])
              cube([Length, Thickness, Height]);
            translate([0, 0, Height / 2 - MidThickness / 2])
              cube([Length, Width, MidThickness]);
            if (flat) {
              hull() {
                translate([0 * Pitch, Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
                translate([((NrOfHoles + 1) * Pitch) + (Pitch / 2.06), Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
              }
            } else {
              //afronding begin beam
              for (i = [0 : NumberOfHoles]) {
                translate([i * Pitch, Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
              }
              //afronding einde beam
              for (i = [0 : NumberOfHoles]) {
                translate([Length - (i * Pitch), Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
              }
              //binnenste met afronding, waar lang gat in komt
              /*hull() {
                translate([Pitch * (NumberOfHoles + 1), Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
                translate([Length - Pitch * (NumberOfHoles + 1), Width / 2, 0])
                  cylinder(r = Width / 2, h = Height);
              }//*/
            }
          }
          //holes
          if (!noholes) {
            union() {
              //Gaten aan het begin
              for (i = [0 : NumberOfHoles]) {
                translate([i * Pitch, Width / 2, 0]) {
                  translate([0, 0, -eps])
                    cylinder(r = Radius2, h = Depth + eps);
                  translate([0, 0, -eps])
                    cylinder(r = Radius1, h = Height + 2 * eps);
                  translate([0, 0, Height - Depth])
                    cylinder(r = Radius2, h = Depth + eps);
                }
              }
              //Gaten aan het einde
              for (i = [0 : NumberOfHoles]) {
                translate([Length - (i * Pitch), Width / 2, 0]) {
                  translate([0, 0, -eps])
                    cylinder(r = Radius2, h = Depth + eps);
                  translate([0, 0, -eps])
                    cylinder(r = Radius1, h = Height + 2 * eps);
                  translate([0, 0, Height - Depth])
                    cylinder(r = Radius2, h = Depth + eps);
                }
              }
              //Lang gat in midden:
              //Onderkant weghalen
              hull() {
                translate([Pitch * (NumberOfHoles + 1), Width / 2, -eps])
                  cylinder(r = Radius2, h = Depth + eps);
                translate([Length - Pitch * (NumberOfHoles + 1), Width / 2, -eps])
                  cylinder(r = Radius2, h = Depth + eps);
              }
              //Binnenkant weghalen
              hull() {
                translate([Pitch * (NumberOfHoles + 1), Width / 2, -eps])
                  cylinder(r = Radius1, h = Height + 2 * eps);
                translate([Length - Pitch * (NumberOfHoles + 1), Width / 2, -eps])
                  cylinder(r = Radius1, h = Height + 2 * eps);
              }
              //Bovenkant weghalen
              hull() {
                translate([Pitch * (NumberOfHoles + 1), Width / 2, Height - Depth])
                  cylinder(r = Radius2, h = Depth + eps);
                translate([Length - Pitch * (NumberOfHoles + 1), Width / 2, Height - Depth])
                  cylinder(r = Radius2, h = Depth + eps);
              }//*/
            }
          }
        }
      }
    }
  }
}

module plus() {
  Plus_Width = 2.0  * 1.0;
  Overhang   = 0.05 * 1.0;
  Radius1Plus = Diameter1Plus / 2;
  union() {
    translate([-Plus_Width / 2, -Radius1Plus, -Overhang]) 
      cube([Plus_Width, Diameter1Plus, Height + Overhang * 2]);
    translate([-Radius1Plus, -Plus_Width / 2, -Overhang]) 
      cube([Radius1Plus * 2, Plus_Width, Height + Overhang * 2]);
  }
}

module axlePlus(Height) {
  AxleHeight = Height * Pitch;
  Plus_Width = 2.0  * 1.0;
  Overhang   = 0.05 * 1.0;
  union() {
    translate([-Plus_Width / 2, -Radius1Plus, -Overhang]) 
      cube([Plus_Width, Radius1Plus * 2, AxleHeight + Overhang * 2]);
    translate([-Radius1Plus, -Plus_Width / 2, -Overhang]) 
      cube([Radius1Plus * 2, Plus_Width, AxleHeight + Overhang * 2]);
  }
}

module axleRoundInner(Height) {
  AxleHeight = Height * Pitch;
  Plus_Width = 2.0  * 1.0;
  Overhang   = 0.05 * 1.0;
  cylinder(h = AxleHeight + Overhang * 2, r = Radius1Plus);
}

module axleRoundOuter(Height) {
  AxleHeight = Height * Pitch;
  Plus_Width = 2.0  * 1.0;
  Overhang   = 0.05 * 1.0;
  cylinder(h = AxleHeight + Overhang * 2, r = Radius2);
}

module axleRoundHollow(Height) {
  difference() {
    axleRoundOuter(Height);
    translate([0, 0, -0.1])
    axleRoundInner(Height + 0.2);
  }
}

module axlePlusRounded(Height) {
  difference() {
    axlePlus(Height);
    translate([0, 0, -0.1])
      axleRoundHollow(Height + 0.2);
  }
}


function fnPitch()     = Pitch;
function fnHeight()    = Height;
function fnLegoWidth() = Width;
