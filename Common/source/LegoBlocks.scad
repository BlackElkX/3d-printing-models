/*
gaten opgemeten:
6.01 uitsparing diameter
4.70 binnen diameter
7.42 buiten diameter
0.72 diepe uitsparing
nop
4.88 diameter
1.88 hoog
blok
103.32 voor 13 lang
127.49 voor 16 lang
7.86 hoog
7.82 breed
*/

oneblockwidth  = 7.82;
oneblockheight = 7.86;
oneblocknopdia = 4.88;
oneblocknophgt = 1.88;
oneblockthick  = (oneblockwidth - oneblocknopdia) / 2;
oneblocknopinn = 2.33;
oneblockspedia = oneblockthick * 2;
/*
translate([0, -30, 0]) block(11, 1);
translate([0, -10, 0]) block(11, 3);
translate([0,  10, 0]) block(1,  1);
translate([0,  30, 0]) block(1,  3);
//*/
/*
translate([0,  50, 0]) block(11, 1, true);
translate([0,  70, 0]) block(11, 3, true);
translate([0,  90, 0]) block(1,  1, true);
translate([0, 110, 0]) block(1,  3, true);
//*/
/*
translate([0, 130, 0]) block(11, 1, true, true);
translate([0, 150, 0]) block(11, 3, true, true);
translate([0, 170, 0]) block(1,  1, true, true);
translate([0, 190, 0]) block(1,  3, true, true);
//*/
//*
translate([0, -90, 0]) block(4, 1, false, true);
translate([0, -70, 0]) block(4, 3, false, true);
translate([0, -50, 0]) block(1,  1, false, true);
translate([0, -30, 0]) block(1,  3, false, true);
//*/
/*
for (x = [0 : 15]) {
  for (y = [0 : 15]) {
    placeBlock(x, y, 0, 1, 1, true, true);
  }
}
//*/

module plate(xnobs, ynobs, heightfactor, isround = false, isopen = false) {
  for (x = [0 : xnobs - 1]) {
    for (y = [0 : ynobs - 1]) {
      placeBlock(x, y, 0, 1, heightfactor, isround, isopen);
    }
  }
}

module placeBlock(x, y, z, nopqty, heightfactor, isround = false, isopen = false) {
  newx = (oneblockwidth) * x;
  newy = (oneblockwidth) * y;
  newz = (oneblockheight / heightfactor) * z;
  translate([newx, newy, newz]) block(nopqty, heightfactor, isround = false, isopen = false);
}

module block(nopqty, heightfactor, isround = false, isopen = false) {
  height      = oneblockheight / heightfactor;
  heightinner = height - oneblockthick;
  innerblock  = oneblockwidth - (oneblockthick * 2);
  innerstart  = -(oneblockwidth * (nopqty / 2)) + oneblockwidth / 2;
  difference() {
    union() {
      if (isround) {
        hull() {
          translate([innerstart + (oneblockwidth * 0), 0, 0])
            cylinder(height, d = oneblockwidth, $fn = 90);
          translate([innerstart + (oneblockwidth * (nopqty - 1)), 0, 0])
            cylinder(height, d = oneblockwidth, $fn = 90);
        }
      } else {
        translate([0, 0, height / 2]) {
          cube([oneblockwidth * nopqty, oneblockwidth, height], true);
        }
      }
      for (index = [0 : nopqty - 1]) {
        translate([innerstart + (oneblockwidth * index), 0, height]) cylinder(oneblocknophgt, d = oneblocknopdia, $fn = 90);
      }
    }
    union() {
      for (index = [0 : nopqty - 1]) {
        translate([innerstart + (oneblockwidth * index), 0, 0]) {
          if (!isopen) {
            if (isround) {
               translate([0, 0, -oneblockthick]) cylinder(height, d = innerblock, $fn = 90);
            } else {
              translate([0, 0, heightinner / 2]) cube([innerblock, innerblock, height], true);
            }
          }
          translate([0, 0, heightinner]) cylinder(oneblocknophgt, d = oneblocknopinn, $fn = 90);
        }
      }
      if (isopen) {
        hull() {
          if (isround) {
            translate([innerstart + (oneblockwidth * 0), 0, -oneblockthick]) cylinder(height, d = innerblock, $fn = 90);
            translate([innerstart + (oneblockwidth * (nopqty - 1)), 0, -oneblockthick]) cylinder(height, d = innerblock, $fn = 90);
          } else {
            translate([innerstart + (oneblockwidth * 0), 0, heightinner / 2]) cube([innerblock, innerblock, height], true);
            translate([innerstart + (oneblockwidth * (nopqty - 1)), 0, heightinner / 2]) cube([innerblock, innerblock, height], true);
          }
        }
      }
    }
  }
  color("green")
  if (isopen) {
    if (nopqty > 1) {
      for (index = [1 : nopqty - 1]) {
        translate([innerstart - (oneblockwidth / 2) + (oneblockwidth * index), 0, 0]) cylinder(height - 0.2, d = oneblockspedia, $fn = 90);
      }
    }
  }
}