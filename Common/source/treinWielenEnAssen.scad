//demo's
demokeAssen();
//translate([0, -10, 0]) koppelingHouderNEM();
//translate([0, -20, 0]) koppelingHaakNEM();

module wiel(dikte, rijvlakRad1, rijvlakRad2, flensDikte, flensRad, spiegelbeeld) {
  if (spiegelbeeld) {
    cylinder(dikte, rijvlakRad1, rijvlakRad2);
    translate([0, 0, dikte - flensDikte])
      cylinder(flensDikte, rijvlakRad1, flensRad);
  } else {
    cylinder(dikte, rijvlakRad2, rijvlakRad1);
    cylinder(flensDikte, flensRad, rijvlakRad1);
  }
}

module assen(wielAsAfstand, wielAsMidden, wielAsLengte, wielAsRadius, wielAfstand, wielDikte,
          wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius, wielFlensDiameter,
          wielIsolatieAfstand, wielIsolatieRadius, tandwielLengte, tandwielRadius, bearingDikte, bearingRadius,
          speling, spelingWielBlok, drawWheel, drawIsolation, drawBearings, drawPins, showBlock) {
  translate([0, wielAsMidden, 0])
    as(wielAsMidden, wielAsLengte, wielAsRadius, wielAfstand, wielDikte,
       wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius, wielFlensDiameter,
       wielIsolatieAfstand, wielIsolatieRadius, tandwielLengte, tandwielRadius, bearingDikte, bearingRadius,
       speling, spelingWielBlok, drawWheel, drawIsolation, drawBearings, drawPins, showBlock);
  translate([wielAsAfstand, wielAsMidden, 0])
    as(wielAsMidden, wielAsLengte, wielAsRadius, wielAfstand, wielDikte,
       wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius, wielFlensDiameter,
       wielIsolatieAfstand, wielIsolatieRadius, tandwielLengte, tandwielRadius, bearingDikte, bearingRadius,
       speling, spelingWielBlok, drawWheel, drawIsolation, drawBearings, drawPins, showBlock);
}

module as(wielAsMidden, wielAsLengte, wielAsRadius, wielAfstand, wielDikte,
          wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius, wielFlensDiameter,
          wielIsolatieAfstand, wielIsolatieRadius, tandwielLengte, tandwielRadius, bearingDikte, bearingRadius,
          speling, spelingWielBlok, drawWheel, drawIsolation, drawBearings, drawPins, showBlock) {
  color("orange")
    translate([0, -wielAsMidden, 0])
      rotate([0, 90, 90])
        cylinder(wielAsLengte, wielAsRadius + speling, wielAsRadius + speling);
  translate([0, -tandwielLengte / 2, 0])
    tandwiel(tandwielLengte, tandwielRadius, speling);
  color("purple")
  if (drawWheel) {
    translate([0, -(wielAfstand / 2) - wielDikte, 0])
      rotate([0, 90, 90])
        wiel(wielDikte, wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius + speling, true);
    translate([0, (wielAfstand / 2), 0])
      rotate([0, 90, 90])
        wiel(wielDikte, wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius + speling, false);
  }
  color("indigo")
  if (drawIsolation) {
    translate([0, -(wielIsolatieAfstand / 2) - wielDikte, 0])
      rotate([0, 90, 90])
        cylinder(wielDikte, wielIsolatieRadius, wielIsolatieRadius);
    translate([0, (wielIsolatieAfstand / 2), 0])
      rotate([0, 90, 90])
        cylinder(wielDikte, wielIsolatieRadius, wielIsolatieRadius);
  }
  color("blue")
  if (drawPins) {
    translate([0, -(wielIsolatieAfstand / 2) - wielDikte, 0])
      rotate([0, 90, 90])
        cylinder(wielDikte, wielIsolatieRadius, wielIsolatieRadius);
    translate([0, (wielIsolatieAfstand / 2), 0])
      rotate([0, 90, 90])
        cylinder(wielDikte, wielIsolatieRadius, wielIsolatieRadius);
  }
  color("lime")
  if (drawBearings) { //bearingDikte, bearingRadius
    translate([0, -(wielIsolatieAfstand / 2), 0])
      rotate([0, 90, 90])
        cylinder(bearingDikte, bearingRadius, bearingRadius);
    translate([0, (wielIsolatieAfstand / 2) - bearingDikte, 0])
      rotate([0, 90, 90])
        cylinder(bearingDikte, bearingRadius, bearingRadius);
  }
  if (showBlock) {
    translate([-((wielFlensDiameter + speling) / 2), -(wielIsolatieAfstand / 2) - spelingWielBlok - wielDikte - speling, -((wielFlensDiameter + speling) / 2)])
      cube([wielFlensDiameter + speling, wielDikte + speling, wielFlensDiameter + speling]);
    translate([-((wielFlensDiameter + speling) / 2),  (wielIsolatieAfstand / 2) + spelingWielBlok,                       -((wielFlensDiameter + speling) / 2)])
      cube([wielFlensDiameter + speling, wielDikte + speling, wielFlensDiameter + speling]);
  }
}

module tandwiel(tandwielLengte, tandwielRadius, speling) {
  color("cyan")
  rotate([0, 90, 90]) {
    translate([0, 0, -(speling / 2)])
    cylinder(tandwielLengte + speling, tandwielRadius + speling, tandwielRadius + speling);
  }
}

//NEM standaarden
//koppelingshouder NEM362
gatBreedte = 3.2;
gatHoogte  = 1.75;
gatLengte  = 7.1;
randDikte  = 1;

module koppelingHouderNEM() {
  difference() {
    translate([ 0,   -((gatBreedte + (randDikte * 2)) / 2), 0])
      cube([gatLengte, gatBreedte + (randDikte * 2), gatHoogte + (randDikte * 2)]);
    translate([-0.1, -((gatHoogte  + (randDikte * 2)) / 2), randDikte])
      cube([gatLengte + 0.1,   gatBreedte, gatHoogte]);
  }
}

module koppelingHaakNEM() {
  translate([0, -2.6, 0]) cube([30, 5.2, 2]);
}


module demokeAssen() {
  $fn = 64;
  //Speling bewegende delen
  speling              = 0;//0.9;
  //spelingSchroeven     = 0.3;
  spelingWielBlok      = 0.3;
  onderBlokExtra       = 1.5;
  checkSpeling         = false;
  checkDoorsnede       = false;
  checkSchroeven       = false;
  //gebruikte tandwielen
  tandwielDiameter     =  3.82;
  tandwielLengte       =  4.5;
  tandwielBoorgatDia   =  1.5;
  //Gebruikte wielassen
  wielAsDiameter       =  1.5;
  wielAsAfstand        = 22.0;
  wielIsolatieAfstand  =  9.43;
  wielIsolatieDiameter =  4;
  wielAfstand          = 10.11;
  wielFlensDiameter    = 10;
  wielDikte            =  4.60;
  wielFlensDikte       =  0.69;
  wielRijvlakDiameter1 =  7.95; //rand van wiel
  wielRijvlakDiameter2 =  8.07; //kant van de flens
  wielAsBearingDia     =  4.3;
  wielAsBearingDikte   =  1.7;

  tandwielRadius       = (tandwielDiameter    / 2);

  wielAsLengte         = (wielAfstand + (wielDikte * 2)) + 1;
  wielAsMidden         = (wielAsLengte         / 2);
  wielAsRadius         = (wielAsDiameter       / 2);
  wielIsolatieRadius   = (wielIsolatieDiameter / 2);
  wielFlensRadius      = (wielFlensDiameter    / 2);
  wielRijvlakRadius1   = (wielRijvlakDiameter1 / 2);
  wielRijvlakRadius2   = (wielRijvlakDiameter2 / 2);
  wielAsBearingRadius  = (wielAsBearingDia     / 2);
  wielAsBearingAfstand = (wielIsolatieAfstand - (wielAsBearingDikte * 2));

  drawWheel     = true;
  drawIsolation = true;
  drawBearings  = true;
  drawPins      = true;
  showBlock     = true;
  assen(wielAsAfstand,      wielAsMidden,       wielAsLengte,   wielAsRadius,       wielAfstand,         wielDikte,
        wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius,    wielFlensDiameter,   wielIsolatieAfstand,
        wielIsolatieRadius, tandwielLengte,     tandwielRadius, wielAsBearingDikte, wielAsBearingRadius, speling,
        spelingWielBlok,    drawWheel,          drawIsolation,  drawBearings,       drawPins,            showBlock);
  translate([15, wielAsMidden, -1]) {
    color("red")        translate([0, 0,  0]) cube([50, 0.05,                 1], true);
    color("lightblue")  translate([0, 0, -1]) cube([50, tandwielLengte,       1], true);
    color("lightgreen") translate([0, 0, -2]) cube([50, wielAsBearingAfstand, 1], true);
    color("pink")       translate([0, 0, -3]) cube([50, wielAfstand,          1], true);
    color("orange")     translate([0, 0, -4]) cube([50, wielAsLengte,         1], true);
  }
}
