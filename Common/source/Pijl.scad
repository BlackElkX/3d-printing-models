$fn = 360;

pijl(10, 15, 1, 1);

module pijl(been, lengte, rad, hoogte) {
  hull() {
    cylinder(hoogte, rad, rad);
    translate([been, 0, 0]) cylinder(hoogte, rad, rad);
  }
  hull() {
    cylinder(hoogte, rad, rad);
    translate([0, been, 0]) cylinder(hoogte, rad, rad);
  }
  hull() {
    cylinder(hoogte, rad, rad);
    translate([lengte, lengte, 0]) cylinder(hoogte, rad, rad);
  }
}