translate([0,   0, 0]) dubbeleStroomafnemer(22, 0.3,  0, 3, 2,  0, 1.5, 0.5, 4, 3, 1);
translate([0,   6, 0]) dubbeleStroomafnemer(22, 0.3, 85, 3, 2, 10, 1.5, 0.5, 4, 3, 1);
translate([0,  -6, 0]) dubbeleStroomafnemer(22, 0.3, 85, 3, 2, 10, 1.5, 0.5, 4, 3, 2);
translate([0, -12, 0]) dubbeleStroomafnemer(22, 0.3,  0, 3, 2,  0, 1.5, 0.5, 4, 3, 2);

module dubbeleStroomafnemer(lengte, dikte, plooiing, sleperlengte, sleperplooi, sleperplooiing, schroefy, schroefr, montagelengte, montagebreedte, doorvoerbreedte) {
  translate([0, -schroefy, 0]) {
    stroomafnemer(lengte, dikte, plooiing, sleperlengte, sleperplooi, sleperplooiing, schroefy, schroefr, montagelengte, montagebreedte, doorvoerbreedte);
    mirror([180, 0, 0]) stroomafnemer(lengte, dikte, plooiing, sleperlengte, sleperplooi, sleperplooiing, schroefy, schroefr, montagelengte, montagebreedte, doorvoerbreedte);
  }
}

module stroomafnemer(lengte, dikte, plooiing, sleperlengte, sleperplooi, sleperplooiing, schroefy, schroefr, montagelengte, montagebreedte, doorvoerbreedte) {
  difference() {
    union() {
      cube([lengte / 2, doorvoerbreedte, dikte]);
      cube([montagelengte / 2, montagebreedte, dikte]);
    }
    translate([0, schroefy, -dikte]) cylinder(r = schroefr, h = dikte * 3, $fn = 64);
    translate([(lengte / 2) - 3, doorvoerbreedte, -dikte]) cube([4, 3, dikte * 3]);
  }
  translate([(lengte / 2) - 2, doorvoerbreedte, 0]) cube([2, dikte , dikte]);
  translate([(lengte / 2) - 2, doorvoerbreedte + dikte, 0]) rotate([plooiing, 0, 0]) {
    cube([2, sleperplooi - dikte, dikte]);
    translate([0, sleperplooi - dikte, 0]) rotate([sleperplooiing, 0, 0]) cube([2, sleperlengte - sleperplooi, dikte]);
  }
}

//oude code, niet gebruiken, ter documentatie:
module stroomafnemers() {
  rand = 0.8;
  //stroomafnemers
  translate([-5,  (wielFlensDikte + (wielAfstand / 2) + (0 + rand)), 0]) 
    cube([(wielAsAfstand + wielFlensDiameter * 2) + 10, 1.2, 0.4]);
  translate([-5, -(wielFlensDikte + (wielAfstand / 2) + (1 + rand)), 0]) 
    cube([(wielAsAfstand + wielFlensDiameter * 2) + 10, 1.2, 0.4]);
}

//oude code, niet gebruiken, ter documentatie:
module stroomafnemers2() {
  rand = 1.0710;
  //stroomafnemersV2
  translate([6,  (wielFlensDikte + (wielAfstand / 2) + (-1.8 + rand)), -0.1]) {
    difference() {
      cube([(wielAsAfstand + wielFlensDiameter * 2) - 13, 0.1, 1.5]);
      translate([7, -0.1, -0.1]) cube([(wielAsAfstand + wielFlensDiameter * 2) - 27, 1, 2]);
    }
    difference() {
      union() {
        translate([0, -1, 0]) cube([(wielAsAfstand + wielFlensDiameter * 2) - 13, 1, 0.1]);
        translate([8.5, -1, 0]) cube([(wielAsAfstand + wielFlensDiameter * 2) - 30, 4, 0.1]);
      }
    }
  }
}
