module schroefGat(schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ, hoogte, radius, verzinkhoogte, verzinkradius, top, faktor = 0.1, omgekeerd = false) {
  translate([schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ]) {
    cylinder(hoogte, radius, radius, false);
    if (omgekeerd) {
      translate([0, 0, 0])
        cylinder(verzinkhoogte + faktor, verzinkradius + faktor, radius, false);
    } else {
      translate([0, 0, top])
        cylinder(verzinkhoogte + faktor, radius, verzinkradius + faktor, false);
    }
  }
}

module moerGat(moerGatPlaatsX, moerGatPlaatsY, moerGatPlaatsZ, hoogte, radius, hoeken, draaiing) {
  translate([moerGatPlaatsX, moerGatPlaatsY, moerGatPlaatsZ])
    rotate([0, 0, draaiing])
      cylinder(hoogte, radius, radius, $fn = hoeken);
}