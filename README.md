# 3D Printing models

All models I create to print out on my custom made 3d-printer.
On github, the firmware, based on marlin, can be found.

An X-Y-core custom modifyed tronxy printer XS5A-330, with 3 in 1 out hotend.

This is more or less a backup and sharing place for all designs I made or adapted that I printed.
All code is licenced as GPL 2.0 if not mentionned in code.

## Getting started

Use OpenScad to edit, export as STL and import in your favorite slicing software.

