$fn  = 64;
use <dinclip.scad>;
lengte  = 85;
breedte = 50;
tussen  =  5;
p2633br = 70;

hp   = 3;
hv   = 10;

dx1  = 23.56;
dy1  = 27.68;
dia1 =  3.15;
xTo1 =  2.815;
x1   = (((lengte / 2) - dx1) / 2);
y1   = ((breedte - dy1) / 2);

dx2  = 30.17;
dy2  = 25.00;
dia2 =  2.91;
xTo2 =  5.995;
x2   = x1 + dx1 + xTo1 + tussen + xTo2;
y2   = ((breedte - dy2) / 2);

dx3  = 68.00;
dy3  = 61.00;
dia3 =  3.44;
x3   = (((lengte / 2) - dx1) / 2);
y3   = ((p2633br - dy3) / 2);

difference() {
  union() {
    cube([lengte, breedte + p2633br, hp]);
    verdikkingen(hp, hv);
  }
  gaten(-1, hv + hp + 2);
  translate([lengte/2, 25, -50]) rotate([0, 90, 90]) holesPCB(1.5);
  translate([lengte/2, breedte + p2633br - 25, -50]) rotate([0, 90, 90]) holesPCB(1.5);
  translate([x3 + 10, breedte + y3 - 8, -1]) cube([dx3 - 20, dy3 - 20, hp + 2]); 
  translate([x3 + 10, breedte + p2633br - 18, -1]) cube([dx3 - 20, dy3 - 20, hp + 2]); 
}

module verdikkingen(hp, hv) {
  color("green")
  translate([x1, y1, hp]) {
    translate([0, 0, 0])
      cylinder(hv/2, dia1, dia1);
    translate([0, dy1, 0])
      cylinder(hv/2, dia1, dia1);
    translate([dx1, 0, 0])
      cylinder(hv/2, dia1, dia1);
    translate([dx1, dy1, 0])
      cylinder(hv/2, dia1, dia1);
  }
  color("lime")
  translate([x2, y2, hp]) {
    translate([0, 0, 0])
      cylinder(hv/2, dia2, dia2);
    translate([0, dy2, 0])
      cylinder(hv/2, dia2, dia2);
    translate([dx2, 0, 0])
      cylinder(hv/2, dia2, dia2);
    translate([dx2, dy2, 0])
      cylinder(hv/2, dia2, dia2);
  }
  color("blue")
  translate([x3, breedte + y3, hp]) {
    translate([0, 0, 0])
      cylinder(hv/2, dia3, dia3);
    translate([0, dy3, 0])
      cylinder(hv/2, dia3, dia3);
    translate([dx3, 0, 0])
      cylinder(hv/2, dia3, dia3);
    translate([dx3, dy3, 0])
      cylinder(hv/2, dia3, dia3);
  }
}

module gaten(hp, hv) {
  color("green")
  translate([x1, y1, hp]) {
    translate([0, 0, -2])
      cylinder(hv, dia1 / 2, dia1 / 2);
    translate([0, dy1, 0])
      cylinder(hv, dia1 / 2, dia1 / 2);
    translate([dx1, 0, 0])
      cylinder(hv, dia1 / 2, dia1 / 2);
    translate([dx1, dy1, 0])
      cylinder(hv, dia1 / 2, dia1 / 2);
  }
  color("lime")
  translate([x2, y2, hp]) {
    translate([0, 0, 0])
      cylinder(hv, dia2 / 2, dia2 / 2);
    translate([0, dy2, 0])
      cylinder(hv, dia2 / 2, dia2 / 2);
    translate([dx2, 0, 0])
      cylinder(hv, dia2 / 2, dia2 / 2);
    translate([dx2, dy2, 0])
      cylinder(hv, dia2 / 2, dia2 / 2);
  }
  color("blue")
  translate([x3, breedte + y3, hp]) {
    translate([0, 0, 0])
      cylinder(hv, dia3 / 2, dia3 / 2);
    translate([0, dy3, 0])
      cylinder(hv, dia3 / 2, dia3 / 2);
    translate([dx3, 0, 0])
      cylinder(hv, dia3 / 2, dia3 / 2);
    translate([dx3, dy3, 0])
      cylinder(hv, dia3 / 2, dia3 / 2);
  }
}