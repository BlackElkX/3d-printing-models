$fn = 64;

clip(0.005, 15, 0.5);

module clip(EPSILON, CLIP_H, xScale) {
  //this clip can be easily de-mounted without a screw driver
  //holes are extremly "long", because i'm lazy
  translate([3, 0, 0])
  difference() {
    color("green")
    union() {
      scale([1, 1, 1])
        linear_extrude(height = CLIP_H, center = true, convexity = 5) {
          import(file = "DIN_miki.dxf", layer = "0", $fn = 64);
      }
    }
    translate([-50, -25, -(CLIP_H/2)-1])
      cube([40, 55, CLIP_H+2]);
      union() {
        //big holes for screw-driver
        translate([0,10, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 50, r1 = 3, r2 = 3, center = true);
        translate([0,0, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 50, r1 = 3, r2 = 3, center = true);
        translate([0,-10, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 50, r1 = 3, r2 = 3, center = true);
      }
  }
  difference() {
    union() {
      scale([0.5, 1, 1])
        linear_extrude(height = CLIP_H, center = true, convexity = 5) {
          import(file = "DIN_miki.dxf", layer = "0", $fn = 64);
      }
    }
    translate([-6.8, -25, -(CLIP_H/2)-1])
      cube([20, 55, CLIP_H+2]);

    translate([7, 0, 0])
      holesPCB(1.5);
  }
}

module holesPCB(r) {
      union() {
        //pre-holes for PCB mounting
        translate([-70,15, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,10, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,5, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,0, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,-5, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,-10, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
        translate([-70,-15, 0])
          rotate(90, [0, 1, 0])
            cylinder(h = 100, r1 = r, r2 = r, center = true);
      }
}