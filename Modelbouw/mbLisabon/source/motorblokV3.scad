use <../../../Common/source/Schroefgat.scad>;
use <../../../Common/source/treinWielenEnAssen.scad>;

$fn = 64;

decoderWidth  =  8.6;
decoderHeight =  3.1;
decoderLength = 11.3; //kabels komen hier uit.
decoderCables =  2.6;

//Speling bewegende delen
speling              = 0.5;
spelingSchroeven     = 0.3;
onderBlokExtra       = 1.5;
//checkSpeling         = true;
//checkDoorsnede       = true;
//checkSchroeven       = true;
checkSpeling         = false;
checkDoorsnede       = false;
checkSchroeven       = false;
vijzen               = "hoekenEnMidden";//"hoeken" of "hoekenEnMidden" of "opEenDerde"

uitsparingen         = false;
kortMotorBlok        = true;

//Gebruikte motor
motorDiameter        =  8;
motorLengte          = 16.55;
motorAsDiameter      =  1.5;
motorAsLengteVoor    =  5.3;
motorAsLengteAchter  =  5;

//vliegwiel
vliegwielDiameter    =  7.5;
vliegwielLengte      =  5.5;
vliegwielBoorgatDia  =  1.5;
vliegwielAfstand     =  2;

//Gebruite wormwielen
wormwielDiameter     =  3.47;
wormwielLengte       =  4.5;
wormwielBoorgatDia   =  1.5;
wormwielVerschuiving = -0.57;

//gebruikte tandwielen
tandwielDiameter     =  3.82;
tandwielLengte       =  4.5;
tandwielBoorgatDia   =  1.5;

//overbrenging worm naar tandwiel
tandwielEnWormHoogte =  6.52;

//Gebruikte wielassen
wielAsDiameter       =  1.5;
wielAsAfstand        = 22.0;
wielIsolatieAfstand  =  9.43;
wielIsolatieDiameter =  4.3;
wielAfstand          = 10.11;
wielFlensDiameter    = 10;
wielDikte            =  4.60;
wielFlensDikte       =  0.69;
wielRijvlakDiameter1 =  7.95; //rand van wiel
wielRijvlakDiameter2 =  8.07; //kant van de flens
wielAsBearingDia     =  4.3;
wielAsBearingDikte   =  1.7;

//Beschikbare ruimte voor motorblok
motorblokLengte      = 52;         //maximum is 52.32;
motorblokBreedte     = 17;
hoogteInterieur      =  2.65;
hoogteChasisPlaat    =  1.04;
hoogteChasis         =  2.47;

//moer en bouten
schroefDiameter      =  2;         //DIN 934 - M2 > 2
schroefVerzinkDia    =  3.8;       //DIN 965 - M2 > 3.8
schroefVerzinkHoogte =  1.2;       //DIN 965 - M2 > 1.2
moerDikte            =  1.6;       //DIN 934 - M2 > 1.6
moerDiameter         =  5.0;       //DIN 934 - M2 > 4.32
moerHoeken           =  6;

schroefVoor          =  6.5;    //2.6;
schroefTotRand       =  2.1;
moerDikteExtra       =  0.4417;
moerZFactor          =  2.7613;

//niet gebruikt, maar wel handig te weten.
afstandChasisSporen  =  2.14; 
tandenPitch          =  0.94248;

//Berekende waardes
motorRadius          = (motorDiameter / 2);
motorwielAsMidden    = (motorDiameter / 2);
motorAsRadius        = (motorAsDiameter     / 2);
motorAsLengte        = (motorLengte + motorAsLengteVoor + motorAsLengteAchter);
motorAsVerlengd      = motorAsLengte + ((vliegwielLengte * 2) + vliegwielAfstand * 2);

motorblokHoogte      = (hoogteInterieur + hoogteChasisPlaat + hoogteChasis + onderBlokExtra);

vliegwielRadius      = (vliegwielDiameter   / 2);
vliegwielBoorgatRad  = (vliegwielBoorgatDia / 2);

wormwielRadius       = (wormwielDiameter    / 2);
wormwielBoorgatRad   = (wormwielBoorgatDia  / 2);
tandwielRadius       = (tandwielDiameter    / 2);
tandwielGrijpafstand = ((wormwielDiameter + tandwielDiameter) - tandwielEnWormHoogte);

wielAsLengte         = (wielAfstand + (wielDikte * 2)) + 1;
wielAsMidden         = (wielAsLengte         / 2);// - (tandwielLengte / 2);
wielAsRadius         = (wielAsDiameter       / 2);
wielIsolatieRadius   = (wielIsolatieDiameter / 2);
wielFlensRadius      = (wielFlensDiameter    / 2);
wielRijvlakRadius1   = (wielRijvlakDiameter1 / 2);
wielRijvlakRadius2   = (wielRijvlakDiameter2 / 2);
wielAsBearingRadius  = (wielAsBearingDia     / 2);

asHoogteAfstand      = (wormwielRadius + tandwielRadius) - tandwielGrijpafstand;

schroefRadius        = (schroefDiameter   / 2);
schroefVerzinkRadius = (schroefVerzinkDia / 2);
schroefMidden        = motorblokLengte / 2;
schroefTotAchter     = motorblokLengte - schroefVoor;

echo("De totale motorblokhoogte is ", motorblokHoogte);

blokX = 0;
blokY = -(motorblokBreedte / 2);
blokZ = -(asHoogteAfstand + wielAsRadius + (hoogteChasisPlaat / 1.5));

//onderkant
translate([0, -motorblokBreedte - 2, onderBlokExtra]) onderkant(speling,  spelingSchroeven, -blokZ);
//middendeel
translate([0,                     0,              0]) middendeel(speling, spelingSchroeven, asHoogteAfstand);
//bovenkant
translate([0,  motorblokBreedte + 2,              0]) bovenkant(speling,  spelingSchroeven, motorblokHoogte - asHoogteAfstand - (wielAsRadius + (hoogteChasisPlaat / 1.5)), 180);// - 0.9265

demo();

//demo's
module demo() {
///*
  translate([77,  -5, 0]) motor(0);
  translate([89, -25, 0]) worm(0);
  translate([89, -15, 0]) vliegwiel(0);
  translate([80, -30, 0]) tekenAssen(0, false, true, true, true, true);
  translate([80, -50, 0]) tekenAssen(0, true,  true, true, true, false);
  translate([70,  15, 0]) mechaniek(0,  false, true, true, true, true);
  translate([70,  55, 0]) mechaniek(0,  true,  true, true, true, false);
  translate([65,  15, 0]) schroefgaten(0);

  translate([70,  35, 0]) mechaniek(speling, false, true, true, true, true);
  translate([70,  75, 0]) mechaniek(speling, true,  true, true, true, false);
  translate([65,  35, 0]) schroefgaten(spelingSchroeven);

  translate([60, -40, 0]) samengesteld();
//*/  
  translate([0,   60, 0]) rotate([ 0, 0, 0]) mechaniek(0, true, true, true, true, false);
  translate([0,   40, 0]) rotate([90, 0, 0]) mechaniek(0, true, true, true, true, false);
}

module samengesteld() {
  color("red")   translate([-60,      0,      0    ]) onderkant(speling,  spelingSchroeven, 0);
  color("green") translate([-60.001, -0.001, -0.001]) middendeel(speling, spelingSchroeven, 0);
  color("blue")  translate([-60.002, -0.002, -0.002]) bovenkant(speling,  spelingSchroeven, 0, 0);
  translate([-60, -30, 0]) completeBlock(speling, spelingSchroeven);
}

//modules
module onderkant(speling, spelingSchroeven, hoogte) {
  translate([0, 0, hoogte]) {
    difference() {
      completeBlock(speling, spelingSchroeven);
      translate([blokX - 1, blokY - 1, blokZ + wielAsRadius + (hoogteChasisPlaat / 1.5)])
        cube([motorblokLengte + 2, motorblokBreedte + 2, motorblokHoogte]);
    }
    if (checkSpeling) {
      translate([blokX + (((motorblokLengte / 2) - (motorAsVerlengd / 2))), 0, 0])
        color("white")
          mechaniek(0, true, true, true, true, false);
    }
    if (checkSchroeven) {
      color("lime")
      schroefgaten(0);
    }
  }
}

module middendeel(speling, spelingSchroeven, hoogte) {
  translate([0, 0, hoogte]) {
    difference() {
      completeBlock(speling, spelingSchroeven);
      translate([blokX - 1, blokY - 1, (blokZ + wielAsRadius + (hoogteChasisPlaat / 1.5)) + asHoogteAfstand])
        cube([motorblokLengte + 2, motorblokBreedte + 2, motorblokHoogte]);
      translate([blokX - 1, blokY - 1, -(motorblokHoogte + asHoogteAfstand)])
        cube([motorblokLengte + 2, motorblokBreedte + 2, motorblokHoogte]);
    }
    if (checkSpeling) {
      translate([blokX + (((motorblokLengte / 2) - (motorAsVerlengd / 2))), 0, 0])
        color("white")
          mechaniek(0, true, true, true, true, false);
    }
    if (checkSchroeven) {
      color("lime")
      schroefgaten(0);
    }
  }
}

module bovenkant(speling, spelingSchroeven, hoogte, draaiing) {
  translate([0, 0, hoogte]) {
    rotate([draaiing, 0, 0]) {
      difference() {
        completeBlock(speling, spelingSchroeven);
        translate([blokX - 1, blokY - 1, -motorblokHoogte])
          cube([motorblokLengte + 2, motorblokBreedte + 2, motorblokHoogte]);
      }
      if (checkSpeling) {
        translate([blokX + (((motorblokLengte / 2) - (motorAsVerlengd / 2))), 0, 0])
          color("white")
            mechaniek(0, true, true, true, true, false);
      }
      if (checkSchroeven) {
        color("lime")
        schroefgaten(0);
      }
    }
  }
}

module completeBlock(speling, spelingSchroeven) {
  difference() {
    translate([blokX, blokY, blokZ - onderBlokExtra])
      cube([motorblokLengte, motorblokBreedte, motorblokHoogte + onderBlokExtra]);
    translate([blokX + (((motorblokLengte / 2) - (motorAsVerlengd / 2))), 0, 0])
      mechaniek(speling, false, false, true, true, true);
    schroefgaten(spelingSchroeven);
    if (checkDoorsnede) {
      translate([-10, 0, -10]) cube([80, 40, 20]);
    }
    if (uitsparingen) {
      //uitsparingen
      translate([-1,                  0, -6]) cylinder(10, 3.5, 3.5);
      translate([motorblokLengte + 1, 0, -6]) cylinder(10, 3.5, 3.5);
    } 
    if (kortMotorBlok) {
      //korte motorblok
      translate([-1,                  0, -6]) cube([7, 20, 20], true);
      translate([motorblokLengte + 1, 0, -6]) cube([7, 20, 20], true);
    }
  }
  if (checkSchroeven) {
    color("lime")
    schroefgaten(0);
  }
}

module schroefgaten(spelingSchroeven) {
  lengte = motorblokHoogte * 2;
  top    = blokZ - 0.01;

  if (vijzen == "hoeken") {
    // op de kop en op de staart
    translate([schroefVoor, 0,  -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven, true);
    }
    translate([schroefTotAchter, 0, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven, true);
    }
  } else if (vijzen == "hoekenEnMidden") {
    // in de hoeken
    translate([schroefVoor, (motorblokBreedte / 2) - schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([schroefTotAchter, (motorblokBreedte / 2) - schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([schroefVoor, -(motorblokBreedte / 2) + schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([schroefTotAchter, -(motorblokBreedte / 2) + schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    //in het midden
    translate([schroefMidden, (motorblokBreedte / 2) - schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([schroefMidden, -(motorblokBreedte / 2) + schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
  } else if (vijzen == "opEenDerde") {
    //op een derde
    translate([motorblokLengte / 3, (motorblokBreedte / 2) - schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([motorblokLengte / 3, -(motorblokBreedte / 2) + schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }

    translate([(motorblokLengte / 3) * 2, (motorblokBreedte / 2) - schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
    translate([(motorblokLengte / 3) * 2, -(motorblokBreedte / 2) + schroefTotRand, -onderBlokExtra]) {
      boutEnMoer(top, lengte, schroefRadius, schroefVerzinkHoogte, schroefVerzinkRadius, moerDikte, moerDiameter, moerHoeken, spelingSchroeven);
    }
  }
}

module boutEnMoer(top, lengte, radius, verzinkHoogte, verzinkRadius, moerdikte, moerdiameter, moerhoeken, spelingSchroeven, draaien = false) {
  schroefGat(0, 0, top, lengte, radius + spelingSchroeven, verzinkHoogte, verzinkRadius, lengte - verzinkHoogte, spelingSchroeven, true);
  if (draaien) {
    moerGat(0, 0, motorblokHoogte - moerdikte * moerZFactor, moerdikte + moerDikteExtra, moerdiameter / 2, moerhoeken, 180 / moerhoeken);
  } else {
    moerGat(0, 0, motorblokHoogte - moerdikte * moerZFactor, moerdikte + moerDikteExtra, moerdiameter / 2, moerhoeken, 0);
  }
}

module mechaniek(speling, drawWheel, drawIsolation, drawBearings, drawPins, showBlock) {
  translate([(vliegwielLengte + vliegwielAfstand), 0, 0]) {
    motor(speling);
    translate([(wormwielLengte / 2) - (wielAsRadius / 2), -(wielAsLengte / 2), -asHoogteAfstand])
      tekenAssen(speling, drawWheel, drawIsolation, drawBearings, drawPins, showBlock);
  }
  stroomafnemers();
}

module stroomafnemers() {
  rand = 0.8;
  //stroomafnemers
  translate([-5,  (wielFlensDikte + (wielAfstand / 2) + (0 + rand)), 0]) 
    cube([(wielAsAfstand + wielFlensDiameter * 2) + 10, 1.2, 0.4]);
  translate([-5, -(wielFlensDikte + (wielAfstand / 2) + (1 + rand)), 0]) 
    cube([(wielAsAfstand + wielFlensDiameter * 2) + 10, 1.2, 0.4]);
}

module motor(speling, midden = true) {
  rotate([0, 90, 0]) {
    //motoras
    color("yellow")
    translate([0, 0, 0])
    cylinder(motorAsLengte, motorAsRadius + speling, motorAsRadius + speling);

    //de motor zelf, zonder speling, moet vast zitten.
    color("lime")
    translate([0, 0, motorAsLengteVoor])
      cylinder(motorLengte, motorRadius, motorRadius);//geen speling!

    //asverlenging voor vliegwielen
    color("magenta")
    translate([0, 0, -vliegwielLengte - vliegwielAfstand - speling / 2])
      cylinder(motorAsVerlengd + speling, motorAsRadius + speling, motorAsRadius + speling);
  }
  
  //kabel uitsparingen
  color("brown")
  if (midden) {
    translate([motorAsLengteVoor + motorLengte,  0, -0.1]) {
      translate([0, -1, 0])
        cube([1, 2, 8]);
    }
  } else {
    translate([motorAsLengteVoor + motorLengte,  0, -0.1]) {
      translate([0,  2.1, 0])
        cube([0.5, 0.5, 8]);
    translate([0, -3.1, 0])
        cube([0.5, 0.5, 8]);
    }
  }

  //wormwielen
  translate([wormwielVerschuiving, 0, 0]) {
    worm(speling);
    translate([wielAsAfstand, 0, 0])
      worm(speling);
  }
  
  //vliegwielen
  translate([-(vliegwielLengte + vliegwielAfstand), 0, 0])
    vliegwiel(speling);
  translate([motorAsLengte + vliegwielAfstand, 0, 0])
    vliegwiel(speling);
}

module worm(speling) {
  color("green") 
  rotate([0, 90, 0])
  difference() {
    translate([0, 0, -(speling / 2)])
    cylinder(wormwielLengte + speling, wormwielRadius + speling, wormwielRadius + speling);
    translate([0, 0, -(speling / 2) - 0.2])
      cylinder(wormwielLengte + speling + 0.4, wormwielBoorgatRad, wormwielBoorgatRad);
  }
}

module vliegwiel(speling) {
  color("blue") 
  rotate([0, 90, 0])
  difference() {
    translate([0, 0, -(speling / 2)])
      cylinder(vliegwielLengte + (speling), vliegwielRadius + speling, vliegwielRadius + speling);
    translate([0, 0, -(speling / 2) - 0.2])
      cylinder(vliegwielLengte + (speling * 2) + 0.4, vliegwielBoorgatRad, vliegwielBoorgatRad);
  }
}

module tekenAssen(speling, drawWheel, drawIsolation, drawBearings, drawPins, showBlock) {
  assen(wielAsAfstand, wielAsMidden, wielAsLengte, wielAsRadius, wielAfstand, wielDikte,
        wielRijvlakRadius1, wielRijvlakRadius2, wielFlensDikte, wielFlensRadius, wielFlensDiameter,
        wielIsolatieAfstand, wielIsolatieRadius, tandwielLengte, tandwielRadius, wielAsBearingDikte, wielAsBearingRadius,
        speling, drawWheel, drawIsolation, drawBearings, drawPins, showBlock);
}

module decoder(decoderWidth, decoderHeight, decoderLength) {
  cube([decoderWidth, decoderHeight, decoderLength]);
  cube([decoderWidth, decoderHeight, decoderLength + 10]);
}