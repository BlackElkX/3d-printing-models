schaalFactor = 1/87;

$fn = 64;

//opgelet! dit moet in schaal zijn!
spoorhoogte    = 10.5;
rijdraadhoogte = 65.5;
rijdraadsteun  = 18.5; //18 viesmann rijdraad + 0.5 oog afstand tot profiel

//profielmast
profielDikte            =   289.999 * schaalFactor;
profielBreedte          =    87     * schaalFactor;
profielExtra            =   870     * schaalFactor;
dwarsProfielDikte       =   261     * schaalFactor;
dwarsProfielLengte      =    87     * schaalFactor;//120 voor 2 sporen
dwarsProfielAansluiting =   435     * schaalFactor;
profielAansluitingDikte =   289.999 * schaalFactor;//4

//berekende waarden:
staandProfielEnkel();

translate([0, 10, 0])
staandProfielEnkelNsporen(2);

translate([0, 20, 0])
staandProfielEnkelNsporen(3);

translate([0, 30, 0])
staandProfielEnkelNsporen(4);

translate([0, 40, 0])
staandProfielEnkelNsporen(9);

translate([0, 50, 0])
staandProfielEnkelNsporen(10);

//de modules
module staandProfielEnkel() {
  staandProfielEnkelNsporen(2);
}

function determineExtraHeight(qtyRails) = (
  (qtyRails == 2) ? profielExtra     :
  (qtyRails <= 4) ? profielExtra * 4 :
  (qtyRails <= 9) ? profielExtra * 6 :
  profielExtra * 8
);

module staandProfielEnkelNsporen(aantalSporen) {
  echo(aantalSporen);
  extra = determineExtraHeight(aantalSporen);
  echo(extra);
  profielHoogte      = (spoorhoogte + rijdraadhoogte + rijdraadsteun + extra);
  dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;

  difference() {
    staanProfiel(profielHoogte, profielDikte, profielBreedte, dwarsProfielHoogte);
    translate([0, 8, 0])
    dwarsProfiel(profielHoogte, profielDikte, dwarsProfielDikte, dwarsProfielHoogte);
  }
}

module profiel(length, width, plateWidth, plateHeight) {
  cube([length, plateWidth, plateHeight]);
  translate([0, width-plateWidth, 0])
  cube([length, plateWidth, plateHeight]);
  translate([0, 0, ((plateHeight / 2) - (plateWidth / 2))])
  cube([length, width, plateWidth]);
}


module staanProfiel(length, width, plateWidth, dwarsProfielHoogte) {
  profiel(length, width, plateWidth, width);
  translate([(length - dwarsProfielHoogte) - dwarsProfielDikte, -width, -((profielAansluitingDikte - width) / 2)])
  cube([dwarsProfielDikte, (width * 2) + 2, profielAansluitingDikte]);
}

module dwarsProfiel(length, width, plateWidth, dwarsProfielHoogte) {
  translate([length - dwarsProfielHoogte, -width, (width - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  profiel((dwarsProfielLengte + width * 3), dwarsProfielDikte, plateWidth, dwarsProfielDikte);
}
