schaalFactor = 1/87;

//use <Houder.scad>;
use <Dwarsprofiel.scad>;
use <StaandProfiel.scad>;
use <Voet.scad>;

//opgelet! dit moet in schaal zijn!
spoorhoogte    = 10.5;
rijdraadhoogte = 65.5;
rijdraadsteun  = 18.5; //18 viesmann rijdraad + 0.5 oog afstand tot profiel

//houderZafstand = (profielDikte / 2) - (1 / 2);

profielDikte       =   289.999 * schaalFactor;
dwarsProfielDikte  =   261     * schaalFactor;
dwarsProfielLengte = 10440     * schaalFactor;

AfstandHouderProfiel = 8;
AfstandTussenHouders = 63;

//berekende waarden:
profielHoogte      = spoorhoogte + rijdraadhoogte + rijdraadsteun + determineExtraHeight(2);
dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;

//voeten
translate([20, 20, 0])
tekenVoet(0.1);

translate([20, 40, 0])
tekenVoet(0.2);

brug();

//de modules
module brug() {
  //staande profielen
  staandProfielEnkel();

  translate([0, dwarsProfielLengte + profielDikte, profielDikte])
  rotate([180, 0, 0])
  staandProfielEnkel();

  //dwarsprofiel
  translate([profielHoogte - dwarsProfielHoogte,
             -profielDikte,
             (profielDikte - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  tekenDwarsProfiel();
}