schaalFactor = 1/87;

$fn = 64;

//profielmast
profielDikte            =  289.999 * schaalFactor;
profielBreedte          =   87     * schaalFactor;
profielExtra            =  870     * schaalFactor;

//profielvoet
plaatHoogte             =  174 * schaalFactor;
plaatLengte             = 1740 * schaalFactor;
plaatBreedte            =  870 * schaalFactor;
voetHoogte              =  609 * schaalFactor;
voetBreedte             =  435 * schaalFactor;
voetDikte               =  435 * schaalFactor;
schroefDiameter         =   87 * schaalFactor;
schroefVerzinkHoogte    =   87 * schaalFactor;
schroefDiameterVerzinkt =  174 * schaalFactor;
schroefGatPlaatsX       =  348 * schaalFactor;
schroefGatPlaatsZ       =  -87 * schaalFactor;

//voeten
tekenVoet(0.1);

module tekenVoet(speling) {
  voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte,
       profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte, speling);
}

module voet(length, width, height, plateLength, plateWidth, plateHeight, profileWidth,
            profilePlateWidth, profilePlateHeight, screwDiameter, screwDiameterZink, screwZinkHeight, speling) {
  difference() {
    voetMassief(length, width, height, plateLength, plateWidth, plateHeight);
    
    translate([(plateLength / 2) - (profileWidth / 2), (plateWidth / 2) - (profilePlateHeight / 2), height + 1])
    rotate([0, 90, 0])
    profiel(height + 2, profileWidth, profilePlateWidth, profilePlateHeight, speling);
    translate([schroefGatPlaatsX, plateWidth / 2, schroefGatPlaatsZ])
    voetSchroefGat(plateHeight + (schroefGatPlaatsX / 2), schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
    translate([plateLength - schroefGatPlaatsX, plateWidth / 2, schroefGatPlaatsZ])
    voetSchroefGat(plateHeight + (schroefGatPlaatsX / 2), schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
  }
}

module voetMassief(length, width, height, plateLength, plateWidth, plateHeight) {
 cube([plateLength, plateWidth, plateHeight]);
 translate([(plateLength / 2) - (length / 2), (plateWidth / 2) - (width / 2), 0])
 cube([length, width, height]);
}

module voetSchroefGat(hoogte, diameter, verzinkhoogte, verzinkdiameter, top) {
  faktor = 0.1;
  cylinder(hoogte, diameter, diameter, false);
  translate([0, 0, top])
  cylinder(verzinkhoogte + faktor, diameter, verzinkdiameter + faktor, false);
}

module profiel(length, width, plateWidth, plateHeight, speling) {
  translate([0, -(speling), -(speling / 2)]) {
    cube([length, plateWidth + speling, plateHeight + speling]);
    translate([0, width-plateWidth + speling, 0])
    cube([length, plateWidth + speling, plateHeight + speling]);
    translate([0, 0, ((plateHeight / 2) - (plateWidth / 2) + (speling / 2))])
    cube([length, width, plateWidth]);
  }
}