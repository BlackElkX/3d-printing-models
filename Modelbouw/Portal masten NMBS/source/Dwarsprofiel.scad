schaalFactor = 1/87;

$fn = 64;

//opgelet! dit moet in schaal zijn!
spoorhoogte    = 10.5;
rijdraadhoogte = 65.5;
rijdraadsteun  = 18.5; //18 viesmann rijdraad + 0.5 oog afstand tot profiel

//profielmast
profielDikte            =   289.999 * schaalFactor;
profielBreedte          =    87     * schaalFactor;
profielExtra            =   870     * schaalFactor;
dwarsProfielDikte       =   261     * schaalFactor;
dwarsProfielLengte      = 10440     * schaalFactor;//120 voor 2 sporen

//berekende waarden:
profielHoogte      = spoorhoogte + rijdraadhoogte + rijdraadsteun + profielExtra;
dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;

//dwarsprofiel
profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);

//de modules
module tekenDwarsProfiel() {
  profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);
}

module profiel(length, width, plateWidth, plateHeight) {
  cube([length, plateWidth, plateHeight]);
  translate([0, width-plateWidth, 0])
  cube([length, plateWidth, plateHeight]);
  translate([0, 0, ((plateHeight / 2) - (plateWidth / 2))])
  cube([length, width, plateWidth]);
}

module dwarsProfiel() {
  translate([profielHoogte - dwarsProfielHoogte, -profielDikte, (profielDikte - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);
}
