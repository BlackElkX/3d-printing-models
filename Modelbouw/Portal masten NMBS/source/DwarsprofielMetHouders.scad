schaalFactor = 1;

$fn = 64   * schaalFactor;
$fa =  2   / schaalFactor;
$fs =  0.1 / schaalFactor;

spoorhoogte    = 10.5 * schaalFactor;
rijdraadhoogte = 65.5 * schaalFactor;
rijdraadsteun  = 18.5 * schaalFactor; //18 viesmann rijdraad + 0.5 oog afstand tot profiel

//profielmast
profielDikte            =   3.333 * schaalFactor;
profielBreedte          =   1     * schaalFactor;
profielExtra            =  10     * schaalFactor;
dwarsProfielDikte       =   3     * schaalFactor;
dwarsProfielLengte      = 120     * schaalFactor;
dwarsProfielAansluiting =   5     * schaalFactor;
profielAansluitingDikte =   3.333 * schaalFactor;//4

//profielvoet
plaatHoogte             =  2 * schaalFactor;
plaatLengte             = 20 * schaalFactor;
plaatBreedte            = 10 * schaalFactor;
voetHoogte              =  7 * schaalFactor;
voetBreedte             =  5 * schaalFactor;
voetDikte               =  5 * schaalFactor;
schroefDiameter         =  1 * schaalFactor;
schroefVerzinkHoogte    =  1 * schaalFactor;
schroefDiameterVerzinkt =  2 * schaalFactor;
schroefGatPlaatsX       =  4 * schaalFactor;
schroefGatPlaatsZ       = -1 * schaalFactor;

//rijdraadogen
rijdraadOogAfstandLinks  = 35 * schaalFactor;
rijdraadOogAfstandRechts = 35 * schaalFactor;

//houders
houderHoogte         = 23     * schaalFactor;
houderBreedteBoven   =  7     * schaalFactor;
houderBreedteBeneden =  2     * schaalFactor;
houderDikte          =  1     * schaalFactor;
houderPlaatDikte     =  0.333 * schaalFactor;
houderDraadGatDia    =  0.058 * schaalFactor;
houderBasis          =  1     * schaalFactor;

AfstandHouderProfiel =  8     * schaalFactor;
AfstandTussenHouders = 54     * schaalFactor;

kabelhouderDiameter  =  1.35  * schaalFactor;

//berekende waarden:
profielHoogte      = spoorhoogte + rijdraadhoogte + rijdraadsteun + profielExtra;
dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;
houderZafstand     = (profielDikte / 2) - (houderDikte / 2);

translate([0, 8, 0])
dwarsProfiel();

//Houders
translate([(profielHoogte - dwarsProfielHoogte - houderPlaatDikte) * schaalFactor, profielDikte + AfstandHouderProfiel, houderZafstand])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

translate([(profielHoogte - dwarsProfielHoogte - houderPlaatDikte) * schaalFactor, profielDikte + AfstandHouderProfiel + AfstandTussenHouders, houderZafstand])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

//de modules
module profiel(length, width, plateWidth, plateHeight) {
  cube([length, plateWidth, plateHeight]);
  translate([0, width-plateWidth, 0])
  cube([length, plateWidth, plateHeight]);
  translate([0, 0, ((plateHeight / 2) - (plateWidth / 2))])
  cube([length, width, plateWidth]);
}


module dwarsProfiel() {
  translate([profielHoogte - dwarsProfielHoogte, -profielDikte, (profielDikte - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);
}

module houder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  difference() {
    houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte);
    houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter);  
  } 
}

module houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte) {
  rotateDegrees = 14;

  translate([0, -(houderBasisValue), 0])
  cube([houderBasisValue/* * schaalFactor */, (breedteBoven + (houderBasisValue * 2))/* * schaalFactor */, dikte/* * schaalFactor */]);
  
  translate([0.2, -(0.2), 0])
  rotate([0, 0, rotateDegrees])
  cube([12, plaatDikte, dikte]);


  translate([0.2, (0.2 + (breedteBoven - plaatDikte))/* * schaalFactor */, 0])
  rotate([0, 0, -rotateDegrees])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);
  
  translate([11/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([11/* * schaalFactor */, (((breedteBoven / 2) + (breedteBeneden / 2)) - plaatDikte)/* * schaalFactor */, 0])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);

  verschuivingY = (((breedteBoven / 2) - (breedteBeneden / 2)) / 2) + (plaatDikte / 2);
  translate([((hoogte / 4) - plaatDikte)/* * schaalFactor */, verschuivingY/* * schaalFactor */, 0])
  cube([(plaatDikte * 2)/* * schaalFactor */,
        ((((breedteBoven / 2) - (breedteBeneden / 2)) + (breedteBeneden / 2)) + (plaatDikte * 2))/* * schaalFactor */,
        dikte/* * schaalFactor */]);

  translate([((hoogte / 2) - plaatDikte)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 3)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([(((hoogte / 4) * 3) - plaatDikte)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 2)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([(hoogte - plaatDikte * 3)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 3)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);
}

module houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  translate([(hoogte - (plaatDikte * 3) / 2)/* * schaalFactor */, (breedteBoven/* * schaalFactor */), (dikte / 2)/* * schaalFactor */])
  rotate([90, 0, 0])
  cylinder(breedteBoven/* * schaalFactor */, houderDraadGatDiameter/* * schaalFactor */, houderDraadGatDiameter/* * schaalFactor */, false);
  
  overlap = 1;
  translate([0.2, overlap, -0.1])
  cube([dikte/* * schaalFactor */, (breedteBoven - overlap * 2)/* * schaalFactor */, (dikte + 0.2)/* * schaalFactor */]);
}

/*
module kabelRing(outsideDiameter, insideDiameter, Width) {
  rotate_extrude(convexity = 10)
  translate([1, 0, 0])
  circle(r = insideDiameter);
}*/