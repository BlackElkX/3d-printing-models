schaalFactor  = 1/87;
draadSteun    = true;
$fn = 64;

//houders
houderHoogte         = 2001     * schaalFactor;// 23
houderBreedteBoven   =  609     * schaalFactor;//  7
houderBreedteBeneden =  174     * schaalFactor;//  2
houderDikte          =   87     * schaalFactor;//  1
houderPlaatDikte     =   28.971 * schaalFactor;//  0.333
houderDraadGatDia    =   11.745 * schaalFactor;//  0.135
houderBasis          =   87     * schaalFactor;//  1

rotateDegrees        = 13.8;
rotateTranslate      =   17.4   * schaalFactor;//  0.2
schuineLengte        = 1044     * schaalFactor;// 12
rechteLengte         =  957     * schaalFactor;// 11

kabelhouderDiameter  =   11.745 * schaalFactor;//  0.135
kabelhouderLengte    =  870     * schaalFactor;// 10
kabelhouderOverlap   =   29.58  * schaalFactor;//  0.34

//Houders
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

module tekenHouder(xMinusHouderPlaatDikte = false, tekenDraadSteun = true, schaal) {
  transX = 0;
  draadSteun = tekenDraadSteun;
  schaalFactor = schaal;
  if (xMinusHouderPlaatDikte) {
    transX = -houderPlaatDikte;
  }
  translate([transX, 0, 0])
  houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);
}
//de modules
module houder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  difference () {
    houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte);
    houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter);
  }
  if (draadSteun) {
    draadhouder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter);
  }
}

module houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte) {
  difference() {
    translate([0, -houderBasisValue, 0])
    cube([houderBasisValue, (breedteBoven + (houderBasisValue * 2)), dikte]);
    translate([0.1, kabelhouderOverlap, -0.1])
    cube([dikte, (breedteBoven - kabelhouderOverlap * 2), (dikte + 0.2)]);
  }
  
  translate([rotateTranslate, -rotateTranslate, 0])
  rotate([0, 0, rotateDegrees])
  cube([schuineLengte, plaatDikte, dikte]);

  translate([rotateTranslate, (rotateTranslate + (breedteBoven - plaatDikte)), 0])
  rotate([0, 0, -rotateDegrees])
  cube([schuineLengte, plaatDikte, dikte]);
  
  translate([schuineLengte, ((breedteBoven / 2) - (breedteBeneden / 2)), 0])
  cube([rechteLengte, plaatDikte, dikte]);

  translate([schuineLengte, (((breedteBoven / 2) + (breedteBeneden / 2)) - plaatDikte), 0])
  cube([rechteLengte, plaatDikte, dikte]);

  verschuivingY = (((breedteBoven / 2) - (breedteBeneden / 2)) / 2) + (plaatDikte / 2);
  translate([((hoogte / 4) - plaatDikte), verschuivingY, 0])
  cube([(plaatDikte * 2),
        ((((breedteBoven / 2) - (breedteBeneden / 2)) + (breedteBeneden / 2)) + (plaatDikte * 2)),
        dikte]);

  translate([((hoogte / 2) - plaatDikte), ((breedteBoven / 2) - (breedteBeneden / 2)), 0])
  cube([(plaatDikte * 3), breedteBeneden, dikte]);

  translate([(((hoogte / 4) * 3) - plaatDikte), ((breedteBoven / 2) - (breedteBeneden / 2)), 0])
  cube([(plaatDikte * 2), breedteBeneden, dikte]);

  translate([(hoogte - plaatDikte * 3), ((breedteBoven / 2) - (breedteBeneden / 2)), 0])
  cube([(plaatDikte * 3), breedteBeneden, dikte]);
}

module draadhouder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  translate([(hoogte - (plaatDikte * 3) / 2), (breedteBoven/2) + kabelhouderLengte, (dikte / 2)])
  rotate([90, 0, 0])
  cylinder(kabelhouderLengte, kabelhouderDiameter, kabelhouderDiameter, false);
}

module houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  if (!draadSteun) {
    translate([0, -5 * schaalFactor, 0])
    draadhouder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter);
  }
}
