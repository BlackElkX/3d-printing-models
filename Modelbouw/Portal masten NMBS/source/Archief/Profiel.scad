$fn = 64;
$fa = 1;
$fs = 0.5;

spoorhoogte    = 10.5;
rijdraadhoogte = 65.5;
rijdraadsteun  = 18.5; //18 viesmann rijdraad + 0.5 oog afstand tot profiel



//profielmast
profielDikte            = 3.33;
profielBreedte          = 1;
profielExtra            = 10;
dwarsProfielDikte       = 3;
dwarsProfielLengte      = 120;
dwarsProfielAansluiting = 5;
profielAansluitingDikte = 4;

//profielvoet
plaatHoogte             = 2;
plaatLengte             = 20;
plaatBreedte            = 10;
voetHoogte              = 7;
voetBreedte             = 5;
voetDikte               = 5;
schroefDiameter         = 1;
schroefVerzinkHoogte    = 1;
schroefDiameterVerzinkt = 2;

//rijdraadogen
rijdraadOogAfstandLinks  = 35;
rijdraadOogAfstandRechts = 35;

//berekende waarden:
profielHoogte      = spoorhoogte + rijdraadhoogte + rijdraadsteun + profielExtra;//90;
dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;

//staande profielen
profiel(profielHoogte, profielDikte, profielBreedte, profielDikte);
translate([0, dwarsProfielLengte, 0])
profiel(profielHoogte, profielDikte, profielBreedte, profielDikte);

//dwarsprofiel
translate([profielHoogte - dwarsProfielHoogte, -profielDikte, (profielDikte - dwarsProfielDikte) / 2])
rotate([0, 0, 90])
profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);

translate([(profielHoogte - dwarsProfielHoogte) - dwarsProfielDikte, -profielDikte, -((profielAansluitingDikte - profielDikte) / 2)])
cube([dwarsProfielDikte, (profielDikte * 2) + 2, profielAansluitingDikte]);

translate([(profielHoogte - dwarsProfielHoogte) - dwarsProfielDikte, dwarsProfielLengte - 2, -((profielAansluitingDikte - profielDikte) / 2)])
cube([dwarsProfielDikte, (profielDikte * 2) + 2, profielAansluitingDikte]);

//voeten
translate([10, 5, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

translate([10, 16, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

//Houders
translate([10, 30, 0])
houder(23, 7, 2, 1, 1/3);

translate([10, 40, 0])
houder(23, 7, 2, 1, 1/3);

/*
//kabelringen
translate([profielHoogte - dwarsProfielHoogte + 0.5, profielDikte + rijdraadOogAfstandLinks, 0])
kabelRing(1, 0.2, 1);

translate([profielHoogte - dwarsProfielHoogte + 0.5, dwarsProfielLengte - rijdraadOogAfstandRechts, 0])
kabelRing(1, 0.2, 1);
*/

//de modules
module profiel(length, width, plateWidth, plateHeight) {
  cube([length, plateWidth, plateHeight]);
  translate([0, width-plateWidth, 0])
  cube([length, plateWidth, plateHeight]);
  translate([0, 0, ((plateHeight / 2) - (plateWidth / 2))])
  cube([length, width, plateWidth]);
}

module voet(length, width, height, plateLength, plateWidth, plateHeight, profileWidth, profilePlateWidth, profilePlateHeight, screwDiameter, screwDiameterZink, screwZinkHeight) {
  difference() {
    voetMassief(length, width, height, plateLength, plateWidth, plateHeight);
    
    translate([(plateLength / 2) - (profileWidth / 2), (plateWidth / 2) - (profilePlateHeight / 2), height + 1])
    rotate([0, 90, 0])
    profiel(height + 2, profileWidth, profilePlateWidth, profilePlateHeight);
    translate([4, plateWidth / 2, -1])
    voetSchroefGat(plateHeight + 2, schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
    translate([plateLength - 4, plateWidth / 2, -1])
    voetSchroefGat(plateHeight + 2, schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
  }
}

module voetMassief(length, width, height, plateLength, plateWidth, plateHeight) {
 cube([plateLength, plateWidth, plateHeight]);
 translate([(plateLength / 2) - (length / 2), (plateWidth / 2) - (width / 2), 0])
 cube([length, width, height]);
}

module voetSchroefGat(hoogte, diameter, verzinkhoogte, verzinkdiameter, top) {
  faktor = 0.1;
  cylinder(hoogte, diameter, diameter, false);
  translate([0, 0, top])
  cylinder(verzinkhoogte + faktor, diameter, verzinkdiameter + faktor, false);
}

module houder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte) {
  difference() {
    houderframe(hoogte, breedteBoven, breedteBeneden, dikte);
    houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte);  
  }
  
  verschuivingY = (((breedteBoven / 2) - (breedteBeneden / 2)) / 2) + (plaatDikte / 2);
  translate([(hoogte / 4) - plaatDikte, verschuivingY, 0])
  cube([plaatDikte * 2, (((breedteBoven / 2) - (breedteBeneden / 2)) + (breedteBeneden/2)) + (plaatDikte * 2), dikte]);

  translate([(hoogte / 2) - plaatDikte, (breedteBoven / 2) - (breedteBeneden / 2), 0])
  cube([plaatDikte * 3, breedteBeneden, dikte]);

  translate([((hoogte / 4) * 3) - plaatDikte, (breedteBoven / 2) - (breedteBeneden / 2), 0])
  cube([plaatDikte * 2, breedteBeneden, dikte]);
}

module houderframe(hoogte, breedteBoven, breedteBeneden, dikte) {
  translate([0, -1, 0])
  cube([1, breedteBoven + 2, dikte]);
  translate([0, 0, dikte/2])
  scale([1, 1, dikte])
  polygon(points = [[0, 0], [0, breedteBoven], [hoogte/2, (breedteBoven / 2) + (breedteBeneden / 2)], [hoogte, (breedteBoven / 2) + (breedteBeneden / 2)], [hoogte, (breedteBoven / 2) - (breedteBeneden / 2)], [hoogte / 2, (breedteBoven / 2) - (breedteBeneden / 2)]]);
}

module houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte) {
  translate([0, 0, dikte / 2])
  scale([1, 1, dikte])
  polygon(points = [[plaatDikte, plaatDikte], [plaatDikte, breedteBoven - plaatDikte],
                    [(hoogte / 2), (breedteBoven / 2) + (breedteBeneden / 2) - plaatDikte], 
                    [hoogte - (plaatDikte*3), (breedteBoven / 2) + (breedteBeneden / 2) - plaatDikte], 
                    [hoogte - (plaatDikte*3), (breedteBoven / 2) - (breedteBeneden / 2) + plaatDikte], 
                    [(hoogte / 2), (breedteBoven / 2) - (breedteBeneden / 2) + plaatDikte]]);
  translate([hoogte - (plaatDikte * 3) / 2, (breedteBoven), dikte / 2])
  rotate([90, 0, 0])
  cylinder(breedteBoven, 0.058, 0.058, false);
}


/*
module kabelRing(outsideDiameter, insideDiameter, Width) {
  rotate_extrude(convexity = 10)
  translate([1, 0, 0])
  circle(r = insideDiameter);
}*/