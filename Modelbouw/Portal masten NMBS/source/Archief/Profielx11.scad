schaalFactor = 1;

$fn = 64   * schaalFactor;
$fa =  2   / schaalFactor;
$fs =  0.1 / schaalFactor;

spoorhoogte    = 10.5 * schaalFactor;
rijdraadhoogte = 65.5 * schaalFactor;
rijdraadsteun  = 18.5 * schaalFactor; //18 viesmann rijdraad + 0.5 oog afstand tot profiel

//profielmast
profielDikte            =   3.333 * schaalFactor;
profielBreedte          =   1     * schaalFactor;
profielExtra            =  40     * schaalFactor;
dwarsProfielDikte       =   3     * schaalFactor;
dwarsProfielLengte      = 243.333 * schaalFactor;
dwarsProfielAansluiting =   5     * schaalFactor;
profielAansluitingDikte =   3.333 * schaalFactor;//4

//profielvoet
plaatHoogte             =  2 * schaalFactor;
plaatLengte             = 20 * schaalFactor;
plaatBreedte            = 10 * schaalFactor;
voetHoogte              =  7 * schaalFactor;
voetBreedte             =  5 * schaalFactor;
voetDikte               =  5 * schaalFactor;
schroefDiameter         =  1 * schaalFactor;
schroefVerzinkHoogte    =  1 * schaalFactor;
schroefDiameterVerzinkt =  2 * schaalFactor;
schroefGatPlaatsX       =  4 * schaalFactor;
schroefGatPlaatsZ       = -1 * schaalFactor;

//rijdraadogen
rijdraadOogAfstandLinks  = 35 * schaalFactor;
rijdraadOogAfstandRechts = 35 * schaalFactor;

//houders
houderHoogte         = 23     * schaalFactor;
houderBreedteBoven   =  7     * schaalFactor;
houderBreedteBeneden =  2     * schaalFactor;
houderDikte          =  1     * schaalFactor;
houderPlaatDikte     =  0.333 * schaalFactor;
houderDraadGatDia    =  0.058 * schaalFactor;
houderBasis          =  1     * schaalFactor;

//berekende waarden:
profielHoogte      = spoorhoogte + rijdraadhoogte + rijdraadsteun + profielExtra;
dwarsProfielHoogte = spoorhoogte + rijdraadhoogte + (profielDikte / 2) + rijdraadsteun;

//dwarsprofiel
translate([(100 + profielExtra) * schaalFactor, 0 * schaalFactor, 0])
rotate([0, 0, 90])
profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);

//staande profielen
translate([(10 + profielExtra) * schaalFactor, 55 * schaalFactor, 0])
rotate([0, 0, 90])
staandProfielEnkel();

translate([(25 + profielExtra) * schaalFactor, 55 * schaalFactor, 0])
rotate([0, 0, 90])
staandProfielEnkel();

//voeten
translate([(10 + profielExtra) * schaalFactor, 20 * schaalFactor, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

translate([(10 + profielExtra) * schaalFactor, 35 * schaalFactor, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

translate([(35 + profielExtra) * schaalFactor, 20 * schaalFactor, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

translate([(35 + profielExtra) * schaalFactor, 35 * schaalFactor, 0])
voet(voetDikte, voetBreedte, voetHoogte, plaatLengte, plaatBreedte, plaatHoogte, profielDikte, profielBreedte, profielDikte, schroefDiameter, schroefDiameterVerzinkt, schroefVerzinkHoogte);

//Houders
translate([(60 + profielExtra) * schaalFactor, 26 * schaalFactor, 0])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

translate([(60 + profielExtra) * schaalFactor, 41 * schaalFactor, 0])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

translate([(90 + profielExtra) * schaalFactor, 26 * schaalFactor, 0])
rotate([0, 0, 180])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

translate([(90 + profielExtra) * schaalFactor, 41 * schaalFactor, 0])
rotate([0, 0, 180])
houder(houderHoogte, houderBreedteBoven, houderBreedteBeneden, houderDikte, houderPlaatDikte, houderDraadGatDia, houderBasis);

brug();

/*
//kabelringen
translate([profielHoogte - dwarsProfielHoogte + 0.5, profielDikte + rijdraadOogAfstandLinks, 0])
kabelRing(1, 0.2, 1);

translate([profielHoogte - dwarsProfielHoogte + 0.5, dwarsProfielLengte - rijdraadOogAfstandRechts, 0])
kabelRing(1, 0.2, 1);
*/

//de modules
module staandProfielEnkel() {
  difference() {
    staanProfiel(profielHoogte, profielDikte, profielBreedte, profielDikte);
    translate([0, 8, 0])
    dwarsProfiel();
  }
}

module brug() {
  //staande profielen
  profiel(profielHoogte, profielDikte, profielBreedte, profielDikte);

  translate([0, dwarsProfielLengte, 0])
  profiel(profielHoogte, profielDikte, profielBreedte, profielDikte);

  //dwarsprofiel
  translate([profielHoogte - dwarsProfielHoogte,
             -profielDikte,
             (profielDikte - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);

  //dwarsbalkhouder
  translate([(profielHoogte - dwarsProfielHoogte) - dwarsProfielDikte, -profielDikte, -((profielAansluitingDikte - profielDikte) / 2)])
  cube([dwarsProfielDikte, (profielDikte * 2) + 2, profielAansluitingDikte]);

  translate([(profielHoogte - dwarsProfielHoogte) - dwarsProfielDikte, dwarsProfielLengte - 2, -((profielAansluitingDikte - profielDikte) / 2)])
  cube([dwarsProfielDikte, (profielDikte * 2) + 2, profielAansluitingDikte]);
}

module profiel(length, width, plateWidth, plateHeight) {
  cube([length, plateWidth, plateHeight]);
  translate([0, width-plateWidth, 0])
  cube([length, plateWidth, plateHeight]);
  translate([0, 0, ((plateHeight / 2) - (plateWidth / 2))])
  cube([length, width, plateWidth]);
}


module staanProfiel(length, width, plateWidth, plateHeight) {
  profiel(profielHoogte, profielDikte, profielBreedte, profielDikte);
  translate([(profielHoogte - dwarsProfielHoogte) - dwarsProfielDikte, -profielDikte, -((profielAansluitingDikte - profielDikte) / 2)])
  cube([dwarsProfielDikte, (profielDikte * 2) + 2, profielAansluitingDikte]);
}

module dwarsProfiel() {
  translate([profielHoogte - dwarsProfielHoogte, -profielDikte, (profielDikte - dwarsProfielDikte) / 2])
  rotate([0, 0, 90])
  profiel((dwarsProfielLengte + profielDikte * 3), dwarsProfielDikte, profielBreedte, dwarsProfielDikte);
}
module voet(length, width, height, plateLength, plateWidth, plateHeight, profileWidth, profilePlateWidth, profilePlateHeight, screwDiameter, screwDiameterZink, screwZinkHeight) {
  difference() {
    voetMassief(length, width, height, plateLength, plateWidth, plateHeight);
    
    translate([(plateLength / 2) - (profileWidth / 2), (plateWidth / 2) - (profilePlateHeight / 2), height + 1])
    rotate([0, 90, 0])
    profiel(height + 2, profileWidth, profilePlateWidth, profilePlateHeight);
    translate([schroefGatPlaatsX, plateWidth / 2, schroefGatPlaatsZ])
    voetSchroefGat(plateHeight + (schroefGatPlaatsX / 2), schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
    translate([plateLength - schroefGatPlaatsX, plateWidth / 2, schroefGatPlaatsZ])
    voetSchroefGat(plateHeight + (schroefGatPlaatsX / 2), schroefDiameter, screwZinkHeight, screwDiameterZink, plateHeight);
  }
}

module voetMassief(length, width, height, plateLength, plateWidth, plateHeight) {
 cube([plateLength, plateWidth, plateHeight]);
 translate([(plateLength / 2) - (length / 2), (plateWidth / 2) - (width / 2), 0])
 cube([length, width, height]);
}

module voetSchroefGat(hoogte, diameter, verzinkhoogte, verzinkdiameter, top) {
  faktor = 0.1;
  cylinder(hoogte, diameter, diameter, false);
  translate([0, 0, top])
  cylinder(verzinkhoogte + faktor, diameter, verzinkdiameter + faktor, false);
}

module houder(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  difference() {
    houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte);
    houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter);  
  } 
}

module houderframe(hoogte, breedteBoven, breedteBeneden, dikte, houderBasisValue, plaatDikte) {
  rotateDegrees = 14;

  translate([0, -(houderBasisValue), 0])
  cube([houderBasisValue/* * schaalFactor */, (breedteBoven + (houderBasisValue * 2))/* * schaalFactor */, dikte/* * schaalFactor */]);
  
  translate([0.2, -(0.2), 0])
  rotate([0, 0, rotateDegrees])
  cube([12, plaatDikte, dikte]);


  translate([0.2, (0.2 + (breedteBoven - plaatDikte))/* * schaalFactor */, 0])
  rotate([0, 0, -rotateDegrees])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);
  
  translate([11/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([11/* * schaalFactor */, (((breedteBoven / 2) + (breedteBeneden / 2)) - plaatDikte)/* * schaalFactor */, 0])
  cube([12/* * schaalFactor */, plaatDikte/* * schaalFactor */, dikte/* * schaalFactor */]);

  verschuivingY = (((breedteBoven / 2) - (breedteBeneden / 2)) / 2) + (plaatDikte / 2);
  translate([((hoogte / 4) - plaatDikte)/* * schaalFactor */, verschuivingY/* * schaalFactor */, 0])
  cube([(plaatDikte * 2)/* * schaalFactor */,
        ((((breedteBoven / 2) - (breedteBeneden / 2)) + (breedteBeneden / 2)) + (plaatDikte * 2))/* * schaalFactor */,
        dikte/* * schaalFactor */]);

  translate([((hoogte / 2) - plaatDikte)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 3)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([(((hoogte / 4) * 3) - plaatDikte)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 2)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);

  translate([(hoogte - plaatDikte * 3)/* * schaalFactor */, ((breedteBoven / 2) - (breedteBeneden / 2))/* * schaalFactor */, 0])
  cube([(plaatDikte * 3)/* * schaalFactor */, breedteBeneden/* * schaalFactor */, dikte/* * schaalFactor */]);
}

module houdergaten(hoogte, breedteBoven, breedteBeneden, dikte, plaatDikte, houderDraadGatDiameter, houderBasisValue) {
  translate([(hoogte - (plaatDikte * 3) / 2)/* * schaalFactor */, (breedteBoven/* * schaalFactor */), (dikte / 2)/* * schaalFactor */])
  rotate([90, 0, 0])
  cylinder(breedteBoven/* * schaalFactor */, houderDraadGatDiameter/* * schaalFactor */, houderDraadGatDiameter/* * schaalFactor */, false);
  
  overlap = 1;
  translate([0.2, overlap, -0.1])
  cube([dikte/* * schaalFactor */, (breedteBoven - overlap * 2)/* * schaalFactor */, (dikte + 0.2)/* * schaalFactor */]);
}

/*
module kabelRing(outsideDiameter, insideDiameter, Width) {
  rotate_extrude(convexity = 10)
  translate([1, 0, 0])
  circle(r = insideDiameter);
}*/