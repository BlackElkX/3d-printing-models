use <LegoTechnicBeam.scad>;
use <Basis.scad>;
use <rpi4-draws.scad>;

NrOfHolesX       =  5;         // 5
NrOfHolesY       = 14;         //14
NrOfHolesAngular =  8;         // 8   //Even number, please
SupportHeight    =  0;
Step             =  6;         //12   //[1:3]
Pitch            =  8   * 1;
Width            =  7.5 * 1;

//  legoRand(true, false, false, false);
//  translate([-100, 10, -1])
//  cube([200, 400, 10]);

//plaat();
//raspberry();

translate([(fnLegoWidth() / 2) - 80, (fnLegoWidth() / 2), 1]) {
  translate([0, Pitch * Step, 0])
    translate([0, (NrOfHolesY - 1) * Pitch, 0])
    drawBeam(12, true, false, 0);
}

module plaat() {
  x1 = (NrOfHolesX * 5) + 2;//27
  y1 = 7;
  x2 = x1 + (NrOfHolesAngular * 5.7125);         // 72.7;
  y2 = y1 + (NrOfHolesAngular * 5.7125);         // 52.7;
  y3 = y2 + ((NrOfHolesY * 2) - 1) * 7.6037037;  //258;
  y4 = y3 + (y2 - 7);                            //304;
  echo("x1", x1);
  echo("x2", x2);
  echo("y1", x1);
  echo("y2", y2);
  echo("y3", y3);
  echo("y4", y4);
  color("green")
  translate([0, 0, 2])
  linear_extrude(1)
    polygon([[x1, y1], [x2, y2], [x2, y3], [x1, y4],
             [-x1, y4], [-x2, y3], [-x2, y2], [-x1, y1]]);
}

module legoRand(hoek1a, hoek1b, hoek2a, hoek2b) {
  verschuiving = -0.25;
  if (hoek1a) {
    translate([-verschuiving, 0, 0])
      hoek1(false, true, false);
  }
  if (hoek1b) {
    translate([verschuiving, 0, 0])
      mirror([1, 0, 0])
        hoek1(false, false, false);
  }
  if (hoek2a || hoek2b) {
    translate([0, (((Pitch * (NrOfHolesY - 1)) + (Pitch * Step  )) * 2) + (verschuiving * 2) + (Pitch * 1), 0])
    rotate([0, 0, 180]) {
      if (hoek2a) {
        translate([-verschuiving, 0, 0])
          hoek2(false, false, false);
      }
      if (hoek2b) {
        translate([verschuiving, 0, 0])
          mirror([1, 0, 0])
            hoek2(false, false, false);
      }
    }
  }
}

module raspberry() {
  translate([30, 110, 3])
    rotate([0, 0, 180])
      show();
}

module hoek1(drawX, drawY, drawS) {
  translate([(fnLegoWidth() / 2) - 80, (fnLegoWidth() / 2), 1]) {
    if (drawX) {
      translate([Pitch * Step, 0, 0])
        drawBeam(NrOfHolesX, true, false, SupportHeight);
    }
    if (drawY) {
      translate([0, Pitch * Step, 0])
        rotate([0, 0, 90])
          drawBeam(NrOfHolesY, true, true, SupportHeight);
    }
    if (drawS) {
      translate([10, 10, 0]) {
        AngularDrawBeam(Step + 1);
        AngularBeam(NrOfHolesAngular);
      }
    }
  }
}

module hoek2(drawX, drawY, drawS) {
  translate([(fnLegoWidth() / 2) - 80, (fnLegoWidth() / 2), 1]) {
    if (drawX) {
      translate([Pitch * Step, 0, 0])
        drawBeam(NrOfHolesX, true, false, SupportHeight);
    }
    if (drawY) {
      translate([0, Pitch * Step, 0])
        rotate([0, 0, 90])
          drawBeam(NrOfHolesY, true, false, SupportHeight);
    }
    if (drawS) {
      translate([10, 10, 0]) {
        AngularDrawBeam(Step + 1);
        AngularBeam(NrOfHolesAngular);
      }
    }
  }
}
