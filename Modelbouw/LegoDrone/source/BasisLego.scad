use <../../../Common/source/LegoTechnicBeam.scad>;
use <../../../Common/source/Pijl.scad>;
use <../../../Common/source/TextAndLogos.scad>;

use <Basis.scad>;

NrOfHolesX       =   5;         //5
NrOfHolesY       =  14;         //14
NrOfHolesAngular =   8;         //8   //Even number, please
heightFactorNr   =   3;
SupportHeight    =   0;
Step             =   6; // [1:3] //12
Pitch            =   8 * 1;
plateHeight      =   1;
signLegLength    =  55;
signLength       = 195;
signRadiusPoints =   2;
logoFromCenter   =  35;
logoFromBase     = 235;
logoSize         =   0.15;
logoHeight       =   0.1;
startHeight      =   0;

signHeight       = plateHeight + 0.2;
midY             = (fnLegoWidth() / 2);
midX             = midY - 80;

legoRand(startHeight, heightFactorNr, false, true);
//plaatPijl();
//raspberry();
//plaatDesign(plateHeight);

//drawBeamDefault(NrOfHolesX, true, false, SupportHeight, heightFactorNr, false, false);

module plaatPijl() {
  difference() {
    plaat();
    translate([0, 0, -0.1])
      plaatDesign(signHeight);
  }
}

module plaatDesign(height) {
  translate([0, 2.45, 0])
  difference() {
    union() {
      translate([0, 15, 0])
        rotate([0, 0, 45])
          pijl(signLegLength, signLength, signRadiusPoints, height);
      translate([-logoFromCenter, logoFromBase, 0])
        fallingMan(logoSize, logoHeight, height, false);
      translate([logoFromCenter, logoFromBase, 0])
        mirror([1, 0, 0])
          fallingMan(logoSize, logoHeight, height, false);
    }
    translate([-100, 0, -10])
      cube([200, 400, 10]);
  }
}

module plaat() {
  x1 = (NrOfHolesX * 5) + 2;//27
  y1 = 7;
  x2 = x1 + (NrOfHolesAngular * 5.7125);//72.7;
  y2 = y1 + (NrOfHolesAngular * 5.7125);//52.7;
  y3 = y2 + ((NrOfHolesY * 2) - 1) * 7.6037037;//258;
  y4 = y3 + (y2 - 7);//304;
  echo("x1", x1);
  echo("x2", x2);
  echo("y1", x1);
  echo("y2", y2);
  echo("y3", y3);
  echo("y4", y4);
  color("green")
  translate([0, 0, 0])
  linear_extrude(1)
    polygon([[x1, y1], [x2, y2], [x2, y3], [x1, y4],
             [-x1, y4], [-x2, y3], [-x2, y2], [-x1, y1]]);
}

module legoRand(startHeight = 0, heightFactor = 1, noholes = false, flat = false) {
  verschuiving = -0.25;
  translate([-verschuiving, 0, 0])
    hoek1(startHeight, heightFactor, noholes, flat);
  translate([verschuiving, 0, 0])
    mirror([1, 0, 0])
      hoek1(startHeight, heightFactor, noholes, flat);
  translate([0, (((Pitch * (NrOfHolesY - 1)) + (Pitch * Step)) * 2) + (verschuiving * 2) + (Pitch * 1), 0])
    rotate([0, 0, 180]) {
      translate([-verschuiving, 0, 0])
        hoek2(startHeight, heightFactor, noholes, flat);
      translate([verschuiving, 0, 0])
        mirror([1, 0, 0])
          hoek2(startHeight, heightFactor, noholes, flat);
  }
}

module raspberry() {
  translate([30, 110, 3])
    rotate([0, 0, 180])
      show();
}

module hoek1(startHeight, heightFactor, noholes, flat) {
  translate([midX, midY, startHeight]) {
    translate([Pitch * Step, 0, 0])
      drawBeamDefault(NrOfHolesX, true, false, SupportHeight, heightFactor, noholes, flat);
    translate([0, Pitch * Step, 0])
      rotate([0, 0, 90])
        drawBeamDefault(NrOfHolesY, true, true, SupportHeight, heightFactor, noholes, flat);
    angularDrawBeamDefault(Step + 1, heightFactor, noholes, flat);
    //angularBeam(NrOfHolesAngular);
  }
}

module hoek2(startHeight, heightFactor, noholes, flat) {
  translate([midX, midY, startHeight]) {
    translate([Pitch * Step, 0, 0])
      drawBeamDefault(NrOfHolesX, true, false, SupportHeight, heightFactor, noholes, flat);
    translate([0, Pitch * Step, 0])
      rotate([0, 0, 90])
        drawBeamDefault(NrOfHolesY, true, false, SupportHeight, heightFactor, noholes, flat);
    angularDrawBeamDefault(Step + 1, heightFactor, noholes, flat);
    //angularBeam(NrOfHolesAngular);
  }
}