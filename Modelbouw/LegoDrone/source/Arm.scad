use <Basis.scad>;

armLengte         = 30;
batteryDia        = 2.15;
batteryLength     = 7.1;
grensSpelingArmen = 0.0;

//startY = -4.55;

armCompleet(0, true, true);
translate([10, 10, 0]) armCompleet(0, true, true);

module alleArmen() {
  for (armNr = [1 : fnArmAantal()]) {
    armhoek = (360 / fnArmAantal()) * armNr; 
    rotate([0, 0, armhoek])
      armCompleet();
  }
}

module armCompleet(startY, links, rechts) {
  translate([0, fnRadMinusApo(), 0])
  difference() {
    armSupport(startY, links, rechts);
    translate([0, 0, 2])
      rotate([90, 0, 0])
        batterijPlaats();
    translate([-2, -12.15 + startY, 0])
      cube([4, 4, 4]);
  }
  difference() {
    translate([0, -8.85 - startY, 2])
      armVerlenging(startY);
    translate([0, -8.85 - startY - armLengte - 1, -1])
      motorhouderbinnen();
  }
  translate([0, -8.85 - startY - armLengte, 0])
    motor(startY);
}

module armSupport(startY, links, rechts) {
  difference() {
    armBuitenkant(startY, links, rechts);
    armBinnenkant(startY);
    translate([0, fnMiddenGat(), 2])
      gaten(225, 1);
  }
  armVerdikking(startY, links, rechts);
}

module armBuitenkant(startY, links, rechts) {
  difference() {
    armCylinder(startY);
    armZijkanten(startY, links, rechts);
    color("orange")
    translate([-4, -7.45 + startY, 4])
      cube([8, 10, 2]);
    color("purple")
    translate([-4, -7.45 + startY, -2])
      cube([8, 10, 2]);
  }
}

module armCylinder(startY) {
  translate([0, startY, 2])
    rotate([90, 0, 0])
      cylinder(1, 3.5, 3.5, $fn = 64);
  translate([0, startY - 1, 2])
    rotate([90, 0, 0])
      cylinder(10, 3.5, 1, $fn = 64);
}

module armZijkanten(startY, links, rechts) {
  if (links) {
    color("red")
    translate([2.168 - grensSpelingArmen, startY + 4.55, -2])
      rotate([0, 0, 180 + 22.5])
        cube([2, 10, 8]);
  }
  if (rechts) {
    color("blue")
    translate([0 + grensSpelingArmen, startY + 4.55, -2])
      rotate([0, 0, 90 + 67.5])
        cube([2, 10, 8]);
  }
}

module armBinnenkant(startY) {
  translate([0, /*startY + */2, fnBasisDikte()])
    scale([1, 1, fnBasisDikte() * 2])
      armBuitenkant(startY);
}

module armVerdikking(startY, links, rechts) {
  difference() {
    armCylinder(startY);
    armZijkanten(startY, links, rechts);
    color("lime")
    translate([-4, -7.56 + startY, 0])
      cube([8, 10, 4]);
    color("gray")
    translate([-4, -6, -(2 + fnBasisDikte())])
      cube([8, 10, 2]);
    color("black")
    translate([0, fnMiddenGat(), -fnBasisDikte()])
      plaatRond();//plaatRond();
    color("gray")
    translate([-4, -6, 4 + fnBasisDikte()])
      cube([8, 10, 2]);
    color("black")
    translate([0, fnMiddenGat(), 4])
      plaatRond();
  }
}

module armVerlenging(startY) {
  radOut = (batteryDia / 2) + fnBasisDikte();
  rotate([90, 0, 0])
  difference() {
    cylinder(armLengte, radOut, radOut, $fn = 64);
    translate([0, -13.7, 2])
      rotate([90, 0, 0])
        batterijPlaats();
  }
}

module batterijPlaats() {
  radIn  = batteryDia / 2;
  translate([0, 0, -2.5])
    cylinder(armLengte + 5, radIn, radIn, $fn = 64);
}

module motor() {
  translate([0, -1, 0])
  difference() {
    motorhouderbuiten();
    motorhouderbinnen();
  }
}

module motorhouderbuiten() {
  cylinder(5, 2, 2, $fn = 64);
  sphere(2, $fn = 64);
}

module motorhouderbinnen() {
  translate([0, 0, fnBasisDikte()])
    cylinder(5, 2 - fnBasisDikte(), 2 - fnBasisDikte(), $fn = 64);
  sphere(2-fnBasisDikte(), $fn = 64);
  translate([0, 1, 2]) 
    rotate([90, 0, 180])
      batterijPlaats();
}