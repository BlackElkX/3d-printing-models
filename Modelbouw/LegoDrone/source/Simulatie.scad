use <Basis.scad>;
use <Arm.scad>;

//basisPlaatRond();
//alleArmen();

basisPlaatRechthoek();

gatenRechthoek(fnLangeZijde() / 4);
//translate([0, fnLangeZijde(), 0])
//  rotate([0, 0, 180])
//    gatenRechthoek(fnLangeZijde() / 4);

module gatenRechthoek(verschuivingLangeZijde) {
  translate([0, verschuivingLangeZijde, fnBasisDikte()])
    armke(45);
//  armke(90);
//  armke(270);
//  translate([0, verschuivingLangeZijde, 0])
//    armke(315);
}


module armke(armhoek) {
  //translate([fnApothema(), -fnApothema(), 0])
  rotate([0, 0, armhoek])
    armCompleet(-4.55);
}
