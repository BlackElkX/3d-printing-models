use <LegoTechnicBeam.scad>;

  $fn = 512;

/*
lengte       = 20;
breedte      =  5;
tussenruimte =  3;
/*
translate([-8, 0, 0])
  drawBeam(2, false, false, 0);

translate([-8, 4 * fnPitch(), 0])
  drawBeam(2, false, false, 0);
*/

arm(40, 5, 7, 2, 50, 54, 31, 3, 6, 25, 4, 3, 7);

module arm(lengte, breedte, tussenruimte, tussenhoogte, motorBinnen, motorBuiten, motorHoogte, supportHoogte, supportBreedte, supportDiameter, schroefAantal, schroefDiameter, schroefAfstandCenter) {
  difference() {
    ladders(lengte, breedte, tussenruimte, tussenhoogte);
    translate([lengte * fnPitch(), ((breedte * fnPitch()) / 2) - (fnLegoWidth() / 2), -30])
      cylinder(motorHoogte + 32, motorBinnen / 2, motorBinnen / 2);
  }
  translate([lengte * fnPitch(), ((breedte * fnPitch()) / 2) - (fnLegoWidth() / 2), -(motorHoogte - fnHeight())])
    motor(motorBinnen, motorBuiten, motorHoogte, supportHoogte, supportBreedte, supportDiameter, schroefAantal, schroefDiameter, schroefAfstandCenter);
}

module ladders(lengte, breedte, tussenruimte, tussenhoogte) {
  ladder(lengte, breedte, tussenruimte);
  translate([0, 0, -(fnHeight() * (tussenhoogte + 1))])
    ladder(lengte, breedte, tussenruimte);
  for (position = [0 : lengte - 1]) {
    if (((position % (tussenruimte + 1)) == 0) && !(position == 0)) {
      translate([(position) * fnPitch(), fnLegoWidth() / 2, -(fnHeight() - fnLegoWidth() / 2)])
        rotate([90, 90, 0])
          drawCubeBeamHigh(tussenhoogte, 1, false, false, 0);
      translate([(position) * fnPitch(), ((breedte - 1) * fnPitch()) + fnLegoWidth() / 2, -(fnHeight() - fnLegoWidth() / 2)])
        rotate([90, 90, 0])
          drawCubeBeamHigh(tussenhoogte, 1, false, false, 0);
    }
  }
}

module ladders2(lengte, breedte, tussenruimte, tussenhoogte) {
  ladder(lengte, breedte, tussenruimte);
  translate([0, 0, -(fnHeight() * (tussenhoogte + 1))])
    ladder(lengte, breedte, tussenruimte);
  for (position = [0 : lengte - 1]) {
    if ((position % (tussenruimte + 1)) == 0) {
      translate([(position) * fnPitch(), 0, -(fnHeight() * (tussenhoogte + 1))])
        //rotate([0, 0, 90])
          drawCubeBeamHigh(1, tussenhoogte + 2, false, false, 0);
      translate([(position) * fnPitch(), (breedte - 1) * fnPitch(), -(fnHeight() * (tussenhoogte + 1))])
        //rotate([0, 0, 90])
          drawCubeBeamHigh(1, tussenhoogte + 2, false, false, 0);
    }
  }
}

module ladder(lengte, breedte, tussenruimte) {
  for (position = [0 : lengte - 1]) {
    if ((position % (tussenruimte + 1)) == 0) {
      translate([(position) * fnPitch(), 0, 0])
        rotate([0, 0, 90])
          drawBeam(breedte, false, false, 0);
    }
  }

  drawBeam(lengte, false, false, 0);
   
  translate([0, 4 * fnPitch(), 0])
    drawBeam(lengte, false, false, 0);
}

module motor(motorBinnen, motorBuiten, motorHoogte, supportHoogte, supportBreedte, supportDiameter, schroefAantal, schroefDiameter, schroefAfstandCenter) {
  difference() {
    cylinder(motorHoogte, motorBuiten / 2, motorBuiten / 2);
    translate([0, 0, -1])
      cylinder(motorHoogte + 2, motorBinnen / 2, motorBinnen / 2);
  }
  difference() {
    union() {
      translate([-(supportBreedte / 2), -(motorBuiten / 2), 0])      cube([supportBreedte, motorBuiten, supportHoogte]);
      translate([-(motorBuiten / 2), -(supportBreedte / 2), 0])      cube([motorBuiten, supportBreedte, 3]);
      cylinder(supportHoogte, supportDiameter / 2, supportDiameter / 2);
    }
    schroeven(supportHoogte, schroefAantal, schroefDiameter, schroefAfstandCenter);
    translate([0, 0, -1])
    difference() {
      cylinder(motorHoogte + 2, motorBuiten, motorBuiten);
      translate([0, 0, -1])
        cylinder(motorHoogte + 4, motorBuiten / 2, motorBuiten / 2);
    }
  }
}
module schroeven(supportHoogte, schroefAantal, schroefDiameter, schroefAfstandCenter) {
  for (hoek = [1 : schroefAantal]) {
    rotate([0, 0, ((360 / schroefAantal) * hoek)])
      translate([schroefAfstandCenter, 0, -1])
        cylinder(supportHoogte + 2, schroefDiameter / 2, schroefDiameter / 2);
  }
}