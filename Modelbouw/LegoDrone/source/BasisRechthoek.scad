use <../../../Common/source/LegoTechnicBeam.scad>;

$fn       = 64;
basisvorm = 8;

schaal    = 1;
armAantal = 8;
starthoek = (360 / basisvorm) / 2;

basisLengte      = 30.00 * schaal;
basisBreedte     = 15.00 * schaal;
basisdikte       =  0.40 * schaal;
schroefDiameter  =  0.25 * schaal;
schroefAfstand1X =  0.75 * schaal;
schroefAfstand2X =  0.75 * schaal;
schroefAfstand3X =  schroefAfstand1X;
schroefAfstand1Y =  1.40 * schaal;
schroefAfstand2Y =  0.00 * schaal;
schroefAfstand3Y = -schroefAfstand1Y;

basisDiameter = (((basisBreedte / 2) / cos(starthoek)) * 2);
basisRadius   = (basisDiameter / 2);
apothema      = cos(starthoek) * basisRadius;
langeZijde    = basisLengte  - (apothema * 2);
korteZijde    = basisBreedte - (apothema * 2);

echo("apothema is ", apothema);
echo("totale lengte is  ", (langeZijde + (apothema * 2)));
echo("totale breedte is ", (korteZijde + (apothema * 2)));

translate([0, apothema, 0])
  basisPlaatRechthoek();

translate([20, apothema, 0])
  basisPlaatRond();

module basisPlaatRechthoek() {
  difference() {
    plaatRechthoek(langeZijde, korteZijde);
    gatenRechthoek(langeZijde / 4);
    translate([0, langeZijde, 0])
    rotate([0, 0, 180])
    gatenRechthoek(langeZijde / 4);
  }
}

module plaatRechthoek(langeZijde, korteZijde) {
  hull() {
    plaatRond();
    translate([0, langeZijde, 0])
      plaatRond();
    translate([korteZijde, langeZijde, 0])
      plaatRond();
    translate([korteZijde, 0, 0])
      plaatRond();
  }
}

module gatenRechthoek(verschuivingLangeZijde) {
  translate([0, verschuivingLangeZijde, 0])
  gaten(135, 1);
  gaten(180, 1);
  gaten(270, 1);
  translate([0, verschuivingLangeZijde, 0])
  gaten(315, 1);
}

module basisPlaatRond() {
  difference() {
    plaatRond();
    gatenRond();
  }
}

module plaatRond() {
  rotate([0, 0, starthoek])
    linear_extrude(basisdikte)
      circle(basisRadius, $fn = basisvorm);
}

module armAansluitingGaten() {
  translate ([(basisRadius - schroefDiameter) - schroefAfstand1X, schroefAfstand1Y, 0])
    circle(schroefDiameter / 2);
  translate ([(basisRadius - schroefDiameter) - schroefAfstand2X, schroefAfstand2Y, 0])
    circle(schroefDiameter / 2);
  translate ([(basisRadius - schroefDiameter) - schroefAfstand3X, schroefAfstand3Y, 0])
    circle(schroefDiameter / 2);
}

module gatenRond() {
  gaten(0, armAantal);
}
module gaten(starthoek, armen) {
  rotate([0, 0, starthoek])
  translate([0, 0, -5])
  linear_extrude(basisdikte + 10) {
    for (arm = [1 : armen]) {
      armhoek = (360 / armAantal) * arm; 
      rotate([0, 0, armhoek])
        armAansluitingGaten();
    }
  }
}

function fnBasisvorm() = basisvorm;

function fnArmAantal() = armAantal;

function fnBasisDikte() = basisdikte;