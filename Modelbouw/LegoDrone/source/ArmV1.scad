use <Basis.scad>;

armLengte         = 30;
batteryDia        = 2.15;
batteryLength     = 7.1;
grensSpelingArmen = 0.0;

translate([0, 0, 0]) armCompleet();

module armCompleet() {
  difference() {
    armSupport();
    translate([0, -13.7, 2])
      rotate([90, 0, 0])
        batterijPlaats();
    translate([-2, -17.7, 0])
      cube([4, 4, 4]);
  }
  difference() {
    translate([0, -13.4, 2])
      armVerlenging();
    translate([0, -13.4 - armLengte - 1, -1])
      motorhouderbinnen();
  }
  translate([0, -13.4 - armLengte, 0])
    motor();
}

module alleArmen() {
  for (armNr = [1 : fnArmAantal()]) {
    armhoek = (360 / fnArmAantal()) * armNr; 
    rotate([0, 0, armhoek])
      armCompleet();
  }
}

module armSupport() {
  difference() {
    armBuitenkant();
    armBinnenkant();
    gatenRond();
  }
  armVerdikking();
}

module armBinnenkant() {
  translate([0, 2, fnBasisDikte()])
    scale([1, 1, fnBasisDikte() * 2])
      armBuitenkant();
}

module armBuitenkant() {
  difference() {
    armCylinder();
    armZijkanten();
    color("orange")
    translate([-4, -12, 4])
      cube([8, 10, 2]);
    color("purple")
    translate([-4, -12, -2])
      cube([8, 10, 2]);
  }
}

module armVerdikking() {
  difference() {
    armCylinder();
    armZijkanten();
    color("lime")
    translate([-4, -12, 0])
      cube([8, 10, 4]);
    color("gray")
    translate([-4, -12, -(2 + fnBasisDikte())])
      cube([8, 10, 2]);
    color("black")
    translate([0, 0, -fnBasisDikte()])
      plaatRond();
    color("gray")
    translate([-4, -12, 4 + fnBasisDikte()])
      cube([8, 10, 4]);
    color("black")
    translate([0, 0, 4])
      plaatRond();
  }
}

module armZijkanten() {
  color("red")
  translate([2.168 - grensSpelingArmen, 0, -2])
    rotate([0, 0, 180 + 22.5])
      cube([2, 10, 8]);
  color("blue")
  translate([0 + grensSpelingArmen, 0, -2])
    rotate([0, 0, 90 + 67.5])
      cube([2, 10, 8]);
}

module armCylinder() {
  translate([0, -4.55, 2])
    rotate([90, 0, 0])
      cylinder(1, 3.5, 3.5, $fn = 64);
  translate([0, -5.55, 2])
    rotate([90, 0, 0])
      cylinder(10, 3.5, 1, $fn = 64);
}

module armVerlenging() {
  radOut = (batteryDia / 2) + fnBasisDikte();
  rotate([90, 0, 0])
  difference() {
    cylinder(armLengte, radOut, radOut, $fn = 64);
    translate([0, -13.7, 2])
      rotate([90, 0, 0])
        batterijPlaats();
  }
}

module batterijPlaats() {
  radIn  = batteryDia / 2;
  translate([0, 0, -2.5])
    cylinder(armLengte + 5, radIn, radIn, $fn = 64);
}

module motor() {
  translate([0, -1, 0])
  difference() {
    motorhouderbuiten();
    motorhouderbinnen();
    batterijPlaats();
  }
}

module motorhouderbuiten() {
  cylinder(5, 2, 2, $fn = 64);
  sphere(2, $fn = 64);
}

module motorhouderbinnen() {
  translate([0, 0, fnBasisDikte()])
    cylinder(5, 2-fnBasisDikte(), 2-fnBasisDikte(), $fn = 64);
  sphere(2-fnBasisDikte(), $fn = 64);
}