hoogte  = 53;
breedte = 150;


aeronefPoint000 = [  0,  0,  0];
aeronefPoint001 = [ 75,  0, 26];
aeronefPoint002 = [  0,  0, 52];
aeronefPoint003 = [-75,  0, 26];
aeronefPoint004 = [  0, 26,  0];
aeronefPoint005 = [ 75, 26, 26];
aeronefPoint006 = [  0, 26, 52];
aeronefPoint007 = [-75, 26, 26];
aeronefPoint008 = [  0, 62,  0];
aeronefPoint009 = [ 56, 62, 26];
aeronefPoint010 = [  0, 62, 52];
aeronefPoint011 = [-56, 62, 26];

/*
aeronefPoint012 = [];
aeronefPoint013 = [];
aeronefPoint014 = [];
aeronefPoint015 = [];
aeronefPoint016 = [];
aeronefPoint017 = [];
aeronefPoint018 = [];
aeronefPoint019 = [];
aeronefPoint020 = [];
aeronefPoint021 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint02 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
aeronefPoint03 = [];
*/

aeronefFacebottom          = [ 0,  1,  2,  3];
aeronefFacefront           = [ 4,  5,  1,  0];
//aeronefFacetop             = [ 7,  6,  5,  4];
aeronefFaceright           = [ 5,  6,  2,  1];
aeronefFaceback            = [ 6,  7,  3,  2];
aeronefFaceleft            = [ 7,  4,  0,  3];
aeronefFaceWingLeftTop     = [ 6,  5,  9, 10];
aeronefFaceWingLeftBottom  = [ 4,  5,  9,  8];
aeronefFaceWingRightTop    = [ 6,  7, 11, 10];
aeronefFaceWingRightBottom = [ 4,  7, 11,  8];

aeronefPoints = [aeronefPoint000,
                 aeronefPoint001,
                 aeronefPoint002,
                 aeronefPoint003,
                 aeronefPoint004,
                 aeronefPoint005,
                 aeronefPoint006,
                 aeronefPoint007,
                 aeronefPoint008, 
                 aeronefPoint009,
                 aeronefPoint010,
                 aeronefPoint011/*,
                 aeronefPoint012,
                 aeronefPoint013,
                 aeronefPoint014,
                 aeronefPoint015,
                 aeronefPoint016,
                 aeronefPoint017 */
                 ];
               
aeronefFaces  = [aeronefFacebottom,
                 aeronefFacefront,
                 //aeronefFacetop,
                 aeronefFaceright,
                 aeronefFaceback,
                 aeronefFaceleft,
                 aeronefFaceWingLeftTop,    
                 aeronefFaceWingLeftBottom, 
                 aeronefFaceWingRightTop,   
                 aeronefFaceWingRightBottom
                 ];
                
polyhedron(aeronefPoints, aeronefFaces);