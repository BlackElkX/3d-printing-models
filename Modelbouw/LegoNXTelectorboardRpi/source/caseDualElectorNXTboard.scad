use <../../LegoDrone/source/LegoTechnicBeam.scad>;

$fn = 64;

breedte         = 65.70;
lengte          = 89.71;
hoogte          = 54;
wand            =  3.4;
i2cBreedte      = 12.28;
i2cHoogte       = 14.9;
i2c2i2c         =  3.13;
wand2i2c        =  5.66;
bodem2print     = 13.53;
printDikte      =  1.7;
print2print     = 15.44;
wand2power      = 34.99;
powerBreedte    =  9;
powerHoogte     = 11;
ioLengte        = 70;
ioHoogte        =  8.3;
rpicon2print    = 12;
rpilintdikte    =  2;
rpicon2wand     = 12.5;
rpiconlengte    = 52;
diameter        =  7.1;
schroefdiameter =  3;
col1            =  2.37;
row1            =  7.34;
col2            = 51.13;
row2            = 65.84;
schroeflengte   =  0.7;
demo            = false;
roundedLego     = true;

print2deksel    = (hoogte - (wand + bodem2print + printDikte + print2print + printDikte));
dekseldikte     = wand;

kaske(breedte, lengte, hoogte, wand, i2cBreedte, i2cHoogte, i2c2i2c, wand2i2c, bodem2print, printDikte, print2print, wand2power, powerBreedte, powerHoogte, ioLengte, ioHoogte, diameter, schroefdiameter, col1, row1, col2, row2, schroeflengte, print2deksel, dekseldikte, rpicon2print, rpilintdikte, rpicon2wand, rpiconlengte, demo);
legoFrame(5, 6.3, 8, 7.8, 7.5, 11, 14, 5);

module legoFrame(dia1, dia2, pitch, height, width, holesX, holesY, holesZ) {
  if (roundedLego) {
    translate([-(height / 2), -(width / 2), 0])
      legoBeamRound(holesX, dia1, dia2, pitch, height, width);
    translate([-(height / 2), -(width / 2), 0])
      rotate([0, 0, 90])
        legoBeamRound(holesY, dia1, dia2, pitch, height, width);
    translate([-(height / 2), -(width / 2) + (pitch * (holesY - 1)), 0])
      legoBeamRound(holesX, dia1, dia2, pitch, height, width);
    translate([-(height / 2) + (pitch * (holesX - 1)), -(width / 2), 0])
      rotate([0, 0, 90])
        legoBeamRound(holesY, dia1, dia2, pitch, height, width);
  } else {
    translate([-(height / 2), -(width / 2), 0])
      legoBeamCube(holesX, dia1, dia2, pitch, height, width);
    translate([-(height / 2), -(width / 2), 0])
      rotate([0, 0, 90])
        legoBeamCube(holesY, dia1, dia2, pitch, height, width);
    translate([-(height / 2), -(width / 2) + (pitch * (holesY - 1)), 0])
      legoBeamCube(holesX, dia1, dia2, pitch, height, width);
    translate([-(height / 2) + (pitch * (holesX - 1)), -(width / 2), 0])
      rotate([0, 0, 90])
        legoBeamCube(holesY, dia1, dia2, pitch, height, width);//*/
  }
  translate([0, -(width / 2) + pitch, (width / 2) + height])
    rotate([0, -90, 0])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);
  
  translate([0, -(width / 2) + pitch * (holesY - 2), (width / 2) + height])
    rotate([0, -90, 0])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);

  translate([pitch * (holesX - 1), -(width / 2) + pitch, (width / 2) + height])
    rotate([0, -90, 0])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);
  
  translate([pitch * (holesX - 1), -(width / 2) + pitch * (holesY - 2), (width / 2) + height])
    rotate([0, -90, 0])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);

  translate([-(width / 2) + pitch, 0, (width / 2) + height])
    rotate([0, -90, 90])
      legoBeamCube(1, dia1, dia2, pitch, height, width);

  translate([-(width / 2) + (pitch * (holesX - 2)), 0, (width / 2) + height])
    rotate([0, -90, 90])
      legoBeamCube(1, dia1, dia2, pitch, height, width);

  translate([-(width / 2) + pitch, (pitch * (holesY - 1)), (width / 2) + height])
    rotate([0, -90, 90])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);

  translate([-(width / 2) + (pitch * (holesX - 2)), (pitch * (holesY - 1)), (width / 2) + height])
    rotate([0, -90, 90])
      legoBeamCube(holesZ, dia1, dia2, pitch, height, width);
}

module kaske(breedte, lengte, hoogte, wand, i2cBreedte, i2cHoogte, i2c2i2c, wand2i2c, bodem2print, printDikte, print2print, wand2power, powerBreedte, powerHoogte, ioLengte, ioHoogte, diameter, schroefdiameter, col1, row1, col2, row2, schroeflengte, print2deksel, dekseldikte, rpicon2print, rpilintdikte, rpicon2wand, rpiconlengte, demo) {
  diffblokdikte = 20;
  difference() {
    cube([(breedte + (wand * 2)), (lengte + (wand * 2)), hoogte]);
    translate([wand, wand, wand])
      cube([breedte, lengte, hoogte]);
    translate([wand2i2c, -1, wand + bodem2print + printDikte])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + i2cBreedte + i2c2i2c, -1, wand + bodem2print + printDikte])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + ((i2cBreedte + i2c2i2c) * 2), -1, wand + bodem2print + printDikte])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + ((i2cBreedte + i2c2i2c) * 3), -1, wand + bodem2print + printDikte])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);

    translate([wand2i2c, -1, wand + bodem2print + print2print + (printDikte * 2)])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + i2cBreedte + i2c2i2c, -1, wand + bodem2print + print2print + (printDikte * 2)])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + ((i2cBreedte + i2c2i2c) * 2), -1, wand + bodem2print + print2print + (printDikte * 2)])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    translate([wand2i2c + ((i2cBreedte + i2c2i2c) * 3), -1, wand + bodem2print + print2print + (printDikte * 2)])
      cube([i2cBreedte, diffblokdikte, i2cHoogte]);
    
    translate([wand2power, lengte - wand - 1, wand + bodem2print + printDikte])
      cube([powerBreedte, diffblokdikte, powerHoogte]);

    translate([wand2power, lengte - wand - 1, wand + bodem2print + print2print + (printDikte * 2)])
      cube([powerBreedte, diffblokdikte, powerHoogte]);

    translate([breedte - 1, lengte - wand - ioLengte, wand + bodem2print + printDikte])
      cube([diffblokdikte, ioLengte, ioHoogte]);

    translate([breedte - 1, lengte - wand - ioLengte, wand + bodem2print + print2print + (printDikte * 2)])
      cube([diffblokdikte, ioLengte, ioHoogte]);

    translate([-1, lengte - wand - rpicon2wand - rpiconlengte, wand + bodem2print + print2print + (printDikte * 2) + rpicon2print])
      cube([diffblokdikte, rpiconlengte, rpilintdikte + diffblokdikte]);
  }

  schroefhouders(diameter, schroefdiameter, lengte, bodem2print, wand, col1, row1, col2, row2, schroeflengte);

  if (demo) {
    color("blue")
      schroefhoudersTussenPrints(diameter, schroefdiameter, lengte, bodem2print, wand, col1, row1, col2, row2, printDikte, print2print, schroeflengte);
    translate([0, 0, bodem2print + printDikte + print2print + printDikte])
      color("red")
        deksel(print2deksel, dekseldikte, breedte, lengte, hoogte, wand, diameter, schroefdiameter, col1, row1, col2, row2, schroeflengte);
  } else {
    color("blue")
    translate([80, -60, -(2 + bodem2print + printDikte) + 0.7])
      schroefhoudersTussenPrints(diameter, schroefdiameter, lengte, bodem2print, wand, col1, row1, col1 + 10, row1 + 10, printDikte, print2print, schroeflengte);

    color("red")
    translate([-20, 0, print2deksel + dekseldikte + wand])
      rotate([0, 180, 0])
        deksel(print2deksel, dekseldikte, breedte, lengte, hoogte, wand, diameter, schroefdiameter, col1, row1, col2, row2, schroeflengte);
  }
}

module schroefhouders(diameter, schroefdiameter, lengte, bodem2print, wand, col1, row1, col2, row2, schroeflengte) {
  schroefhouder(col1, row1, diameter, schroefdiameter, lengte, bodem2print, wand, schroeflengte);
  schroefhouder(col2, row1, diameter, schroefdiameter, lengte, bodem2print, wand, schroeflengte);
  schroefhouder(col1, row2, diameter, schroefdiameter, lengte, bodem2print, wand, schroeflengte);
  schroefhouder(col2, row2, diameter, schroefdiameter, lengte, bodem2print, wand, schroeflengte);
}

module schroefhoudersTussenPrints(diameter, schroefdiameter, lengte, bodem2print, wand, col1, row1, col2, row2, printDikte, print2print, schroeflengte) {
  translate([0, 0, bodem2print + printDikte]) {
    schroefhouder(col1, row1, diameter, schroefdiameter, lengte, print2print, wand, schroeflengte);
    schroefhouder(col2, row1, diameter, schroefdiameter, lengte, print2print, wand, schroeflengte);
    schroefhouder(col1, row2, diameter, schroefdiameter, lengte, print2print, wand, schroeflengte);
    schroefhouder(col2, row2, diameter, schroefdiameter, lengte, print2print, wand, schroeflengte);
  }  
}

module schroefhouder(x, y, diameter, schroefdiameter, lengte, hoogte, wand, schroeflengte) {
  translate([x + (diameter / 2) + wand, lengte - (y + (diameter / 2)), wand])
  //difference() 
  {
    cylinder(hoogte, diameter / 2, diameter / 2);
    translate([0, 0, -schroeflengte])
      cylinder(hoogte + (schroeflengte * 2), schroefdiameter / 2, schroefdiameter / 2);
  }
}

module deksel(print2deksel, dekseldikte, breedte, lengte, hoogte, wand, diameter, schroefdiameter, col1, row1, col2, row2, schroeflengte) {
  schroefhouders(diameter, schroefdiameter, lengte, print2deksel, wand, col1, row1, col2, row2, schroeflengte);
  translate([0, 0, print2deksel + wand])
    cube([breedte + wand * 2, lengte + wand * 2, wand]);
  translate([wand, wand, print2deksel])
    cube([breedte, lengte, wand]);
}
