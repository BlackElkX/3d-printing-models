use <../../../Common/source/Schroefgat.scad>;

$fn  = 64;

hoogte   = 7.83;
diameter = 3.63;
diavast  = 6.50;

sokkelbodem1(25, -30, 20, 60, hoogte, diavast);
translate([0, 0, hoogte])
rotate([0, 180, 0])
sokkelboven1(-10, -30, 20, 60, hoogte, diameter);

module sokkelbodem1(x, y, l, b, h, d) {
  ysg1 = (((y + b) - y) / 4) + y;
  yg   = (((y + b) - y) / 2) + y;
  ysg2 = yg + (ysg1 - y);
  difference() {
    color("lime")
      sokkel(x, y, l, b, h, d);
    color("red")
      translate([x - d - 1, y - d - 1, h - 1])
        cube([l + (d * 2) + 2, b + (d * 2) + 2, h]);
    color("blue")
      schroefGat((((x + l) - x) / 2) + x, ysg1, -1, h + 2, 3, 2, 5, h - 2);
    color("blue")
      schroefGat((((x + l) - x) / 2) + x, ysg2, -1, h + 2, 3, 2, 5, h - 2);
  }
}

module sokkelboven1(x, y, l, b, h, d) {
  difference() {
    sokkel(x, y, l, b, h, d);
    color("green")
      translate([x - d - 2, y - d - 2, -1])
        cube([l + (d * 2) + 4, b + (d * 2) + 4, h]);
  }
  xg = (((x + l) - x) / 2) + x;
  yg = (((y + b) - y) / 2) + y;
  color("yellow")
  difference() {
    translate([0, 0, 1])
      gatrond(xg, yg, h - 3, diavast - 0.01);
    translate([0, 0, -1])
      gatrond(xg, yg, h + 2, diameter);
  }
}

module sokkelbodem2(x, y, l, b, h, d) {
  ysg1 = (((y + b) - y) / 4) + y;
  yg   = (((y + b) - y) / 2) + y;
  ysg2 = yg + (ysg1 - y);
  difference() {
    color("lime")
      sokkel(x, y, l, b, h, d);
    color("red")
      translate([x - d - 1, y - d - 1, h - 1])
        cube([l + (d * 2) + 2, b + (d * 2) + 2, h]);
    color("purple")
      difference() {
        translate([x - d - 1, y - d - 1, -1])
          cube([l + (d * 2) + 2, b + (d * 2) + 2, h + 1]);
        translate([x, y, -3])
          cube([l, b, h + 4]);
      }
    color("blue")
      schroefGat((((x + l) - x) / 2) + x, ysg1, -1, h + 2, 3, 2, 5, h - 2);
    color("blue")
      schroefGat((((x + l) - x) / 2) + x, ysg2, -1, h + 2, 3, 2, 5, h - 2);
  }
}

module sokkelboven2(x, y, l, b, h, d) {
  difference() {
    sokkel(x, y, l, b, h, d);
    color("green")
      translate([x, y, -1])
        cube([l, b, h]);
  }
  xg = (((x + l) - x) / 2) + x;
  yg = (((y + b) - y) / 2) + y;
  color("yellow")
  difference() {
    translate([0, 0, 1])
      gatrond(xg, yg, h - 3, diavast - 0.01);
    translate([0, 0, -1])
      gatrond(xg, yg, h + 2, diameter);
  }
}

module sokkel(x, y, l, b, h, d) {
  xg = (((x + l) - x) / 2) + x;
  yg = (((y + b) - y) / 2) + y;
  difference() {
    sokkelplaat(x, y, x + l, y + b, x, y + b, x + l, y, h, 5, 4);
    gatrond(xg, yg, h, d);
  }
}

module sokkelplaat(x1, y1, x2, y2, x3, y3, x4, y4, h, r1, r2) {
  hull() {
    translate([x1, y1, 0])
      cylinder(h, r1, r2);
    translate([x2, y2, 0])
      cylinder(h, r1, r2);
    translate([x3, y3, 0])
      cylinder(h, r1, r2);
    translate([x4, y4, 0])
      cylinder(h, r1, r2);
  }
}

module gatrond(x, y, h, d) {
  translate([x, y, -1])
    cylinder(h + 2, d / 2, d / 2);
}

