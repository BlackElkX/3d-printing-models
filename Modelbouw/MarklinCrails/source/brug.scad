use <Kop.scad>;

bridgelength  = 360 + 40.3;
length        = bridgelength;

translate([0, 0, 0])
  rotate([0, 0, 0])
    spoorbrug(bridgelength - 40.3);

module spoorbrug(lengte) {
  difference() {
    spoorbedding(lengte);
    railbedholes();
    translate([0, lengte, 0])
      rotate([0, 0, 180])
        railbedholes();
  }
}