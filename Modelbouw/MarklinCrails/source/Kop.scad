$fn = 64;

schaalFactor = 1;

spoorlengte            = 23;
kliphouderDikte        =  1.75 * schaalFactor;
profielBreedte         = 40    * schaalFactor;
profielBovenLinks      =  6    * schaalFactor;
profielHoogte          =  7.83 * schaalFactor;
profielHoogteBinnen    =  5.77 * schaalFactor;
profielBovenRechts     = profielBreedte - profielBovenLinks;
//nok hole
profielNokGatHoogte    = profielHoogte          - ( 5.72 * schaalFactor);
profielNokGatRechts    = profielBreedte         - ( 1.53 * schaalFactor);
profielNokGatRechts2   = profielBreedte         - ( 3.13 * schaalFactor);
manKlikRechtsBinnen    = profielBreedte         - ( 9.55 * schaalFactor);
profielNokGatBreedte   = manKlikRechtsBinnen    + ( 2.70 * schaalFactor);
//male klip
manKlikLinksBovenIn    = manKlikRechtsBinnen    - ( 7.04 * schaalFactor);
manKlikLinksBoven      = manKlikLinksBovenIn    - ( 1.58 * schaalFactor);
manKlikLinksBenedenIn  = manKlikRechtsBinnen    - ( 6.41 * schaalFactor);
manKlikLinksBeneden    = manKlikLinksBenedenIn  - ( 1.21 * schaalFactor);
//female klip
vrwKlikRechtsBoven     = manKlikLinksBoven      - ( 2.25 * schaalFactor);
vrwKlikRechtsBeneden   = manKlikLinksBeneden    - ( 4.09 * schaalFactor);
vrwKlikRechtsBenedenIn = vrwKlikRechtsBeneden   - ( 0.92 * schaalFactor);
vrwKlikRechtsBovenIn   = vrwKlikRechtsBoven     - ( 0.92 * schaalFactor);
vrwKlikLinksBovenIn    = vrwKlikRechtsBovenIn   - (10.22 * schaalFactor);
vrwKlikLinksBenedenIn  = vrwKlikRechtsBenedenIn - ( 8.60 * schaalFactor);
vrwKlikLinksBeneden    = vrwKlikLinksBenedenIn  - ( 0.92 * schaalFactor);
vrwKlikLinksBoven      = vrwKlikLinksBovenIn    - ( 0.92 * schaalFactor);
//nok
profielNokBoven        = profielBovenLinks;
//spoorbedding
spoorafstandlinks      = 3.8;
spoorafstandrechts     = spoorafstandlinks + 3.60;
profielLinksBinnen     = 1.53 * schaalFactor;
profielRechtsBinnen    = profielBreedte         - ( 1.53 * schaalFactor);
spoor1links            = profielBovenLinks  + (spoorafstandlinks  * schaalFactor);
spoor1rechts           = profielBovenLinks  + (spoorafstandrechts * schaalFactor);
spoor2links            = profielBovenRechts - (spoorafstandrechts * schaalFactor);
spoor2rechts           = profielBovenRechts - (spoorafstandlinks  * schaalFactor);
spoorhoogte            = profielHoogte      + ( 0.22 * schaalFactor);

//male klip itselves
klipdiepteTotaal       =  7.07 * schaalFactor;
klipdiepte             =  7.12 * schaalFactor;
kliphoogte             =  5.88 * schaalFactor;
klipdikte              = 10.39 * schaalFactor;
klipdikteBinnen        =  6.30 * schaalFactor;
kliphoogteVoor         =  1.85 * schaalFactor;
klipdiepteVoor         =  3.60 * schaalFactor;
klipdiepteVoorWeg      =  2.05 * schaalFactor;
klipborgdiameter       =  1.44 * schaalFactor;
klipborgafstand        = (4.01 * schaalFactor) + (klipborgdiameter / 2);
klipborghoogte         =  1.47 * schaalFactor;//1.52
klipbreedteBorg        =  8.39 * schaalFactor;
klipbreedteVooraan     =  7.29 * schaalFactor;
//rotation
kliphoek               =  9.7;
kliphoekVooraan        =  7;
//hulpdinken
klipOnderaanX = 1;
spelingMn     = 0.01;
spelingVr     = 0.03;

nokLengte    = 2.49 * schaalFactor;
nokHoogte    = 2.10 * schaalFactor;
nokDikte     = 2.22 * schaalFactor;
nokLinks     = nokDikte + (2.95 * schaalFactor);
nokAfronding = nokDikte / 2;

//railverbinder
railverbindingsbreedteuitsparing = 4.44;
railverbindingsbreedte = 3.42;
railverbindingshoogte  = 1.39; // 2.63
railverbindingsplaat   = 0.58;

all();

//effectief tekenen
module all() {
  head();
  railbed(true);
}
module head() {
  translate([spelingMn, 0, 0])
    scale([1 - spelingMn, 1 - spelingMn, 1 - spelingMn])
      maleClip();
  difference() {
    profile();
    translate([10.5, 1.85, (profielHoogte - railverbindingshoogte) + 0.1])
      rotate([0, 0, 180])
        railuitsparing();
    translate([(spelingVr * 16), 0, 0])
      scale([1 + spelingVr * 2, 1 + spelingVr * 2, 1 + spelingVr * 2])
        rotate([0, 0, 180])
          femaleClip();
  }
}

module railbed(holes) {
  difference() {
    spoorbedding(spoorlengte);
    if (holes) {
      railbedholes();
    }
  }
}

module railbedholes() {
  translate([-9.8, 14, 0])
    cube([2.5, 2.5, 10]);
  translate([7.3, 14, 0])
    cube([2.5, 2.5, 10]);
}
module maleClip() {
  translate([manKlikLinksBoven - (profielBreedte / 2), -klipdiepte, 0])
    klipMannelijk();
  translate([4, -6.9, 1.3])
    linear_extrude(1.2) {
      scale([1, 1])
      text("3-1", size = 3.5);
    }
}

module femaleClip() {
  translate([manKlikLinksBoven - (profielBreedte / 2), -klipdiepte, 0])
  klipVrouwelijk();
}

module profile() {
  rotate([90, 0, 0]) {
    kliphouder(kliphouderDikte * 2.5);
    translate([(nokLinks - (profielBreedte / 2)), 0, 0])
      rotate([0, -90, 0])
        nok();
  }
}

module kliphouder(dikte) {
  translate([-(profielBreedte / 2), 0, -(dikte)])
  linear_extrude(dikte) {
    polygon(points = [
      [0, 0], [profielBovenLinks, profielHoogte],
      [profielBovenRechts, profielHoogte], [profielBreedte, 0],
      [profielNokGatRechts, 0], [profielNokGatRechts2, profielNokGatHoogte],
      [profielNokGatBreedte, profielNokGatHoogte], [profielNokGatBreedte, 0],
      [manKlikRechtsBinnen, 0], [manKlikRechtsBinnen, profielHoogteBinnen],
      [manKlikLinksBovenIn, profielHoogteBinnen], [manKlikLinksBenedenIn, 0],
    ]);
  }
}

module kliphouderdetail(dikte) {
  translate([-(profielBreedte / 2), 0, -(dikte)])
  linear_extrude(dikte) {
    polygon(points = [
      [0, 0], [profielBovenLinks, profielHoogte],
      [profielBovenRechts, profielHoogte], [profielBreedte, 0],
      [profielNokGatRechts, 0], [profielNokGatRechts2, profielNokGatHoogte],
      [profielNokGatBreedte, profielNokGatHoogte], [profielNokGatBreedte, 0],
      [manKlikRechtsBinnen, 0], [manKlikRechtsBinnen, profielHoogteBinnen],
      [manKlikLinksBovenIn, profielHoogteBinnen], [manKlikLinksBenedenIn, 0],
      [manKlikLinksBeneden, 0], [manKlikLinksBoven, profielHoogteBinnen],
      [vrwKlikRechtsBoven, profielHoogteBinnen], [vrwKlikRechtsBeneden, 0],
      [vrwKlikRechtsBenedenIn, 0], [vrwKlikRechtsBovenIn,  profielHoogteBinnen],
      [vrwKlikLinksBovenIn, profielHoogteBinnen], [vrwKlikLinksBenedenIn, 0],
      [vrwKlikLinksBeneden, 0], [vrwKlikLinksBoven, profielHoogteBinnen],
      [profielNokBoven, profielHoogteBinnen], [profielNokBoven, 0]
    ]);
  }
}

module spoorbedding(spoorlengte) {
  dikte = kliphouderDikte * 2.5;
  rotate([90, 0, 0]) {
    translate([-(profielBreedte / 2), 0, -spoorlengte - (dikte/2)])
    linear_extrude(spoorlengte) {
      polygon(points = [
        [0, 0], [profielBovenLinks, profielHoogte],
        [spoor1links,  profielHoogte], [spoor1links,  spoorhoogte],
        [spoor1rechts, spoorhoogte],   [spoor1rechts, profielHoogte],
        [spoor2links,  profielHoogte], [spoor2links,  spoorhoogte],
        [spoor2rechts, spoorhoogte],   [spoor2rechts, profielHoogte],
        [profielBovenRechts, profielHoogte], [profielBreedte, 0],
        [profielRechtsBinnen, 0], [profielBovenRechts, profielHoogteBinnen],
        [profielNokBoven, profielHoogteBinnen], [profielLinksBinnen, 0]
      ]);
    }
  }
}

module nok() {
  cube([nokLengte, nokHoogte, nokDikte]);
  translate([nokLengte, nokHoogte, nokAfronding]) rotate([90, 0, 0])
  cylinder(nokHoogte, nokAfronding, nokAfronding);
}

module klipVrouwelijk() {
  color("red")
  difference() {
    extra = 0.1;
    translate([0, extra, 0])
    cube([klipdikte, klipdiepte + extra, kliphoogte]);
    klipBuitenWegsnijden();
  }
  color("blue") 
  difference() {
    borg();
    translate([-3, -1, kliphoogte])
      cube([15, 12, kliphoogte]);
    klipBinnenWegsnijden();
  } 
  /*//hulp voor ontwerpen
  color("green")
  translate([klipOnderaanX + ((klipbreedteBorg - klipbreedteVooraan) / 2), -1, 0])
  cube([klipbreedteVooraan, 1, 1]);
  color("lime")
  translate([klipOnderaanX, 4, 0])
  cube([klipbreedteBorg, 1, 1]);
  */
}

module klipMannelijk() {
  difference() {
    klipVrouwelijk();
    klipBinnenWegsnijden();
    translate([-1, -1, -1])
      cube([12, 12, 1]);
  }
}

module borg() {
  translate([klipborghoogte, klipborgafstand, -0.2])
    rotate([0, -kliphoek, 0])
      cylinder(10, klipborgdiameter / 2, klipborgdiameter / 2);
  translate([klipdikte - klipborghoogte, klipborgafstand, -0.2]) 
    rotate([0, kliphoek, 0])
      cylinder(10, klipborgdiameter / 2, klipborgdiameter / 2);
}

module klipBinnenWegsnijdenSchuin() {
  translate([2.22, 2.1, kliphoogteVoor])
    rotate([0, -kliphoek, 0])
      cube([1, 10, 11]);
  translate([klipdikte - 2.22, 2.1, 0])
    rotate([0, kliphoek, kliphoogteVoor])
      cube([1, 10, 11]);
}

module klipBinnenWegsnijden() {
  translate([(klipdikte - klipdikteBinnen) / 2, klipdiepteVoor, -0.5])
    cube([klipdikteBinnen, klipdiepte + 1, kliphoogte + 1]);
  translate([(klipdikte - klipdikteBinnen) / 2, -klipdiepteVoor, kliphoogteVoor])
    cube([klipdikteBinnen, klipdiepte + 1, kliphoogte + 1]);
  translate([-0.5, -0.5, kliphoogteVoor])
    cube([klipdikte + 1, klipdiepteVoorWeg + 0.5, 5]);
}

module klipBuitenWegsnijden() {
  translate([0.02, -0.1, -0.2])
    rotate([0, -kliphoek, 0])
      cube([1, 11, 11]);
  translate([klipdikte - 1.02, -0.1, -0.2])
    rotate([0, kliphoek, 0])
      cube([1, 11, 11]);
  //schuinvoor
  verschuiving = 0.6;
  translate([verschuiving, -0.1, -0.4])
    rotate([0, -kliphoek, kliphoekVooraan])
      cube([1, 4.5, 12]);
  translate([klipdikte - (1 + verschuiving), -0.1, -0.4])
    rotate([0, kliphoek, -kliphoekVooraan])
      cube([1, 4.5, 12]);
}


module railuitsparing() {
  difference() {
    cube([railverbindingsbreedteuitsparing, 1.90, railverbindingshoogte]);
    translate([-1, -0.5, -1])
    rotate([-10, 0, 0])
      cube([railverbindingsbreedteuitsparing + 2, 2.90, railverbindingshoogte]);
  }
}

module railverbinder() {
  difference() {
    cube([railverbindingsbreedte, 1.90, railverbindingshoogte]);
    translate([railverbindingsplaat, -0.1, railverbindingshoogte - 0.83])
      cube([3.42 - (railverbindingsplaat * 2), 2.1, railverbindingshoogte]);
    translate([-1, -0.5, -1])
    rotate([-10, 0, 0])
      cube([5, 2.90, railverbindingshoogte]);
  }
}