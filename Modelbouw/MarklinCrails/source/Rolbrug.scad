use <Kop.scad>;

distanceR1R2 = 77.5;
distanceR2R3 = 77.5;
distanceR3R4 = 64.3;
distanceR4R5 = 64.3;
distanceBrug = 50;

startposition = 35;
width         = 320;
bridgelength  = 280;
length        = bridgelength;

brug();

translate([260, 53, 0])
  rotate([0, 0, 90])
    spoorbrug(bridgelength - 40.3);

translate([350, 0, 0])
  rotate([0, 0, 0])
    spoorbrug(360);

module brug() {
  difference() {
    alles();
    translate([10, 25, -1])
      cube([300, length - 40, 17]);
    gaten();
  }
}

module alles() {
  translate ([startposition, 0, 0])
  for (count = [0 : 4]) {
    translate([count * distanceBrug, 0, 0]) {
      all();
    }
  }
  translate ([20 + (distanceBrug * 4) + distanceR3R4, 0, 0]) {
    all();
  }
  
  translate ([startposition, length + 10, 0])
  for (count = [0 : 5]) {
    translate([count * distanceBrug, 0, 0])
      rotate([0, 0, 180]) {
        all();
      }
  }

  translate([0, 5, 0])
    difference() {
      cube([320, length, 5]);
      translate([10, 20, -1])
        cube([300, length - 40, 7]);
      difference() {
        translate([2, 2, -1])
          cube([318, length - 4, 5]);
        translate([8, 18, -2])
          cube([304, length - 34, 7]);
      }
    }
}

module gaten() {
  translate ([startposition, 0, 0])
  for (count = [0 : 4]) {
    translate([count * distanceBrug, 0, 0]) {
      railbedholes();
    }
  }
  translate ([20 + (distanceBrug * 4) + distanceR3R4, 0, 0]) {
    railbedholes();
  }
  
  translate ([startposition, length + 10, 0])
  for (count = [0 : 5]) {
    translate([count * distanceBrug, 0, 0])
      rotate([0, 0, 180]) {
        railbedholes();
      }
  }
}

module spoorbrug(lengte) {
  difference() {
    spoorbedding(lengte);
    railbedholes();
    translate([0, lengte, 0])
      rotate([0, 0, 180])
        railbedholes();
  }
}