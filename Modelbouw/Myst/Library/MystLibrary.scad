$fn = 128;

library(1, true, true, true, true);

module library(scaleFactor, drawBase, drawBuilding, drawRoof, drawPortal) {
  plateWidth                  = scaleFactor * 101;
  plateLength                 = scaleFactor * 135;
  plateHeight                 = scaleFactor *   8;
  
  openSpaceWidth              = scaleFactor *  80;
  openSpaceLength             = scaleFactor * 100;

  stairRand                   = scaleFactor *   3.1;
  stairWidth                  = scaleFactor *  40;
  stairDepth                  = scaleFactor *  17.6;
  stairHeight                 = scaleFactor *   8;
  stairSteps                  = scaleFactor *   3;

  buildingWidth               = scaleFactor *  88;
  buildingLength              = scaleFactor * 109;
  buildingHeight              = scaleFactor *  33.18;
  buildingBottom1Length       = scaleFactor * 111;
  buildingBottom2Length       = scaleFactor * 110;
  buildingBottom1Width        = scaleFactor *  90;
  buildingBottom2Width        = scaleFactor *  89;
  buildingBottom1Height       = scaleFactor *   5;
  buildingBottom2Height       = scaleFactor *   2;
  buildingToMidthHeight       = scaleFactor *  13.3;
  buildingMidthLength         = scaleFactor * 110;
  buildingMidthWidth          = scaleFactor *  89;
  buildingMidthHeight         = scaleFactor *   2.85;
  buildingTopWidth            = scaleFactor *  87;
  buildingTopLength           = scaleFactor * 107;
  buildingTopHeight           = scaleFactor *   8.82;//16.82;// 50 - 33.18; 22.07
  buildingToPrismHeight       = scaleFactor *  23.73;//31.61 - 8.19 = 

  portalLength                = scaleFactor *  24;
  portalWidth                 = scaleFactor *  67.82;//?? pillar to pillar
  portalPillarHeightTotal     = scaleFactor *  29.15;
  portalPillarHeightBottom1   = scaleFactor *   1.35;
  portalPillarHeightBottom2   = scaleFactor *   0.72;
  portalPillarHeightTop1      = scaleFactor *   1.63;
  portalPillarHeightTop2      = scaleFactor *   0.72;
  portalPillarDiagonalBottom  = scaleFactor *   8;
  portalPillarDiagonalTop     = scaleFactor *   5.8;
  portalPillarDiagonalBottom1 = scaleFactor *  13.17;
  portalPillarDiagonalBottom2 = scaleFactor *  10.39;
  portalPillarDiagonalTop1    = scaleFactor *   7.0;
  portalPillarDiagonalTop2    = scaleFactor *   8.18;
  
  portalWallWidth             = scaleFactor *  10.03;
  portalWallThickness         = scaleFactor *   6.47;
  portalWallDistance          = scaleFactor *  15.6;
  portalWallHeight            = scaleFactor *  21;
  
  portalWidthBottom           = scaleFactor *  47.5;
  portalWidthTop1             = scaleFactor *  62.36;
  portalWidthTop2             = scaleFactor *  62.76;
  portalDepthBottom           = scaleFactor *  15;
  portalDepthMidPlus          = scaleFactor *   1;
  portalDepthTop1             = scaleFactor *  22.7;
  portalDepthTop2             = scaleFactor *  23.18;

  pillarHeightTotal           = scaleFactor *  42;
  pillarHeightBottom1         = scaleFactor *   1;
  pillarHeightBottom2         = scaleFactor *   1;
  pillarHeightTop1            = scaleFactor *   3;
  pillarHeightTop2            = scaleFactor *   1;
  pillarDiagonalBottom        = scaleFactor *   8.5;
  pillarDiagonalTop           = scaleFactor *   6;
  pillarDiagonalBottom1       = scaleFactor *  13;
  pillarDiagonalBottom2       = scaleFactor *  11;
  pillarDiagonalTop1          = scaleFactor *   7.4;
  pillarDiagonalTop2          = scaleFactor *   8.4;
  pillar2Building             = scaleFactor *  10;

  wallThickness               = scaleFactor *   4;

  roofHeight                  = scaleFactor *   4;
  roofWidth                   = scaleFactor *  91;
  roofLength                  = scaleFactor * 112;
  topHeight                   = scaleFactor *  12;
  topLength                   = scaleFactor *  71.80;
  topBaseWidth                = scaleFactor *  55.13;

  doorWidth                   = scaleFactor *  13.50;
  doorDepth                   = scaleFactor *   2.8;
  doorHeight                  = scaleFactor *  21;

  windowWidth                 = scaleFactor *  13.87;
  windowHeight                = scaleFactor *  24.18;
  windowTopWidth              = scaleFactor *  12.28;
  windowTopHeight             = scaleFactor *   6.56;
  windowDistance              = scaleFactor *   2;
  windowThickness             = scaleFactor *   2;
  window2Building             = scaleFactor *  16.26;

  if (drawBase) {
    base(plateWidth, plateLength, plateHeight, stairWidth, stairDepth, stairHeight, stairSteps, stairRand);
  }
  if (drawBuilding) {
    difference() {
      union() {
        building(buildingWidth, buildingLength, buildingHeight, plateHeight, portalLength, plateHeight, wallThickness,
                 buildingBottom1Length, buildingBottom2Length, buildingBottom1Width, buildingBottom2Width, buildingBottom1Height, buildingBottom2Height,
                 buildingToMidthHeight, buildingMidthLength, buildingMidthWidth, buildingMidthHeight, buildingTopWidth, buildingTopLength, buildingTopHeight,
                 buildingToPrismHeight, doorWidth, doorHeight);

        pillars(buildingWidth, buildingLength, buildingHeight, portalLength, plateHeight,
                pillarHeightTotal, pillarHeightBottom1, pillarHeightBottom2, pillarHeightTop1,
                pillarHeightTop2, pillarDiagonalBottom, pillarDiagonalTop, pillarDiagonalBottom1,
                pillarDiagonalBottom2, pillarDiagonalTop1, pillarDiagonalTop2, pillar2Building);

        windows(windowWidth, windowHeight, windowTopWidth, windowTopHeight, windowDistance, windowThickness, window2Building, buildingWidth, buildingLength, portalLength,
                plateHeight + buildingBottom1Height + buildingBottom2Height, wallThickness);
      }
      translate([-((buildingWidth / 2) - (wallThickness / 2)), -((buildingLength / 2) - (wallThickness / 2)) - (portalLength / 2), -1])
        cube([buildingWidth - wallThickness, buildingLength - wallThickness, buildingHeight + 50]);
    }
  }
  if (drawPortal) {
    portalPillars(portalWidth, portalLength, plateHeight, plateLength,
                  portalPillarHeightTotal, portalPillarHeightBottom1, portalPillarHeightBottom2, portalPillarHeightTop1, 
                  portalPillarHeightTop2, portalPillarDiagonalBottom, portalPillarDiagonalTop, portalPillarDiagonalBottom1,
                  portalPillarDiagonalBottom2, portalPillarDiagonalTop1, portalPillarDiagonalTop2);

    portalWalls(portalWallWidth, portalWallThickness, portalWallHeight, portalWallDistance, portalWidth, portalLength, doorWidth, doorDepth, buildingLength, plateHeight);
    
    portalRoof(portalWidthBottom, portalWidthTop1, portalWidthTop2, portalDepthBottom,
               portalDepthMidPlus, portalDepthTop1, portalDepthTop2, plateHeight + portalWallHeight, buildingLength, portalLength);
  }
  if (drawRoof) {
    roof(plateHeight + buildingHeight + buildingTopHeight + buildingMidthHeight, portalLength,
         roofWidth, roofLength, roofHeight, topBaseWidth, topLength, topHeight, buildingWidth, buildingLength, wallThickness);
  }
}
module base(plateWidth, plateLength, plateHeight, stairWidth, stairDepth, stairHeight, stairSteps, stairRand) {
  color("brown")
  baseplate(plateWidth, plateLength, plateHeight);
  color("gray")
  translate([0, ((plateLength / 2) + (stairDepth / 2)), 0])
    stair(stairWidth, stairDepth, stairHeight, stairSteps, stairRand);
}

module baseplate(width, length, height) {
  translate([-(width / 2), -(length / 2), 0])
    cube([width, length, height]);
}

module stair(width, depth, height, steps, rand) {
  depthStep  = depth / steps;
  heightStep = height / steps;
  widthStep  = width - (rand * 2);
  translate([-(width / 2), -(depth / 2), 0]) {
    difference() {
      cube([width, depth, height]);
      for (i = [1 : steps]) {
        translate([((width - widthStep) / 2), depth - (depthStep * i) - 0.01, (heightStep * i) + 0.01])
          cube([widthStep, depthStep + 0.02, height]);
      }
    }
    translate([width - (rand / 2), depth, 0])
      cylinder(height, rand / 2, rand / 2);
    translate([(rand / 2), depth, 0])
      cylinder(height, rand / 2, rand / 2);
  }
}

module pillar(hTot, hBot1, hBot2, hTop1, hTop2, dBot, dTop, dBot1, dBot2, dTop1, dTop2) {
  hRest = hTot - (hBot1 + hBot2 + hTop1 + hTop2);
  color("green") {
    translate([0, 0, hBot1 + hBot2])
      cylinder(hRest,       dBot  / 2, dTop  / 2);
    cylinder(hBot1,         dBot1 / 2, dBot1 / 2);
    cylinder(hBot1 + hBot2, dBot2 / 2, dBot2 / 2);

    translate([0, 0, hTot - hTop2 - hTop1])
      cylinder(hTop1, dTop1 / 2, dTop1 / 2);
    translate([0, 0, hTot - hTop2])
      cylinder(hTop2, dTop2 / 2, dTop2 / 2);
  }
}

module building(width, length, height, plateHeight, portalLength, baseHeight, wallThickness, bottom1Length, bottom2Length, bottom1Width, 
                bottom2Width, bottom1Height, bottom2Height, buildingToMidthHeight, buildingMidthLength, buildingMidthWidth, buildingMidthHeight,
                buildingTopWidth, buildingTopLength, buildingTopHeight, buildingToPrismHeight, doorWidth, doorHeight) {
  difference() {
    buildingBlock(width, length, height, plateHeight, portalLength, baseHeight, bottom1Length, bottom2Length, bottom1Width, 
                bottom2Width, bottom1Height, bottom2Height, buildingToMidthHeight, buildingMidthLength, buildingMidthWidth,
                buildingMidthHeight, buildingTopWidth, buildingTopLength, buildingTopHeight, buildingToPrismHeight);
    translate([-((width / 2) - (wallThickness / 2)), -((length / 2) - (wallThickness / 2)) - (portalLength / 2), -1])
      cube([width - wallThickness, length - wallThickness, height + 50]);
    //door
    translate([-(doorWidth / 2), (length / 2) - (portalLength / 1.5), baseHeight - 0.1])
      cube([doorWidth, 10, doorHeight + 0.1]);
  }
}

module buildingBlock(width, length, height, plateHeight, portalLength, baseHeight, bottom1Length, bottom2Length, bottom1Width, 
                     bottom2Width, bottom1Height, bottom2Height, buildingToMidthHeight, buildingMidthLength, buildingMidthWidth, buildingMidthHeight,
                     buildingTopWidth, buildingTopLength, buildingTopHeight, buildingToPrismHeight) {
  buildingBase(width, length, height, plateHeight, portalLength);

  buildingBase(bottom1Width,       bottom1Length,       bottom1Height,       plateHeight,                                         portalLength);
  buildingBase(bottom2Width,       bottom2Length,       bottom2Height,       plateHeight + bottom1Height,                         portalLength);
  buildingBase(buildingMidthWidth, buildingMidthLength, buildingMidthHeight, plateHeight + bottom1Height + buildingToMidthHeight, portalLength);
  buildingBase(buildingTopWidth,   buildingTopLength,   buildingTopHeight,   plateHeight + height,                                portalLength);

  buildingPrism(buildingMidthWidth, buildingMidthWidth + 2, buildingMidthLength + 2, buildingMidthHeight, plateHeight + buildingToPrismHeight, portalLength);
  buildingPrism(buildingMidthWidth, buildingMidthWidth + 2, buildingMidthLength + 2, buildingMidthHeight, plateHeight + height + buildingTopHeight, portalLength);
}

module pillars(width, length, height, portalLength, baseHeight,
               pillarHeightTotal, pillarHeightBottom1, pillarHeightBottom2, pillarHeightTop1, 
               pillarHeightTop2, pillarDiagonalBottom, pillarDiagonalTop, pillarDiagonalBottom1,
               pillarDiagonalBottom2, pillarDiagonalTop1, pillarDiagonalTop2, pillar2Building) {
  firstPillarX = width;
  firstPillarY = (length - pillar2Building) / 4;
  translate([-(width / 2), -(length / 2) - (portalLength / 2) + pillar2Building, baseHeight]) {
    for (row = [1 : 2]) {
      for (col = [1 : 4]) {
        pillarX = ((firstPillarX * row) - width);
        pillarY = ((firstPillarY * col) - firstPillarY) + ((pillar2Building / 2) * (col-1));
        translate([pillarX, pillarY, 0])
          pillar(pillarHeightTotal, pillarHeightBottom1, pillarHeightBottom2, pillarHeightTop1, pillarHeightTop2,
                 pillarDiagonalBottom, pillarDiagonalTop, pillarDiagonalBottom1, pillarDiagonalBottom2, pillarDiagonalTop1, pillarDiagonalTop2);
      }
    }
  }
}

module buildingBase(width, length, height, baseHeight, portalLength) {
  yShift = -(length / 2) - (portalLength / 2);

  translate([-(width / 2), yShift, baseHeight])
    cube([width, length, height]);
}

module buildingPrism(base, top, length, height, baseHeight, portalLength) {
  yShift = -(length / 2) - (portalLength / 2);

  translate([-(getBigest(base, top) / 2), yShift, baseHeight])
    rectangularPrism(height, base, top, length, 4);
}

module rectangularPrism(height, base, top, length, basisvorm) {
  factor  = (2 * (sin(180 / basisvorm)));
  radBase = (base / 2) * factor;
  radTop  = (top  / 2) * factor;
  width   = getBigest(base, top);
  yShift  = length - (width);
  
  translate([width / 2, width / 2, 0])
  hull() {
    rotate([0, 0, 45])
      cylinder(height, radBase, radTop, $fn = basisvorm);
    translate([0, yShift, 0])
      rotate([0, 0, 45])
        cylinder(height, radBase, radTop, $fn = basisvorm);
  }
}
module rectangularPrism2(height, base, top, length, basisvorm, translateOnSmallest) {
  factor  = (2 * (sin(180 / basisvorm)));
  radBase = (base / 2) * factor;
  radTop  = (top  / 2) * factor;
  width   = getOneOfTwo(base, top, translateOnSmallest);
  yShift  = length - width;
  
  translate([width / 2, width / 2, 0])
  hull() {
    rotate([0, 0, 45])
      cylinder(height, radBase, radTop, $fn = basisvorm);
    translate([0, yShift, 0])
      rotate([0, 0, 45])
        cylinder(height, radBase, radTop, $fn = basisvorm);
  }
}

module roof(startHeight, portalLength, roofWidth, roofLength, roofHeight, topBaseWidth, topLength, topHeight, width, length, wallThickness) {
  color("red")
  difference() {
    roofblock(startHeight, portalLength, roofWidth, roofLength, roofHeight, topBaseWidth, topLength, topHeight, width, length, wallThickness);
    translate([0, 0, -1])
      buildingPrism(topBaseWidth, 0, topLength, topHeight, startHeight + roofHeight, portalLength);
    translate([0, 0, -12.99])
      buildingPrism(topBaseWidth, topBaseWidth, topLength, topHeight, startHeight + roofHeight, portalLength);
  }
}

module roofblock(startHeight, portalLength, roofWidth, roofLength, roofHeight, topBaseWidth, topLength, topHeight, width, length, wallThickness) {
  buildingBase(roofWidth, roofLength, roofHeight, startHeight, portalLength);
  buildingPrism(topBaseWidth, 0, topLength, topHeight, startHeight + roofHeight, portalLength);
  translate([-((width / 2) - (wallThickness / 2)), -((length / 2) - (wallThickness / 2)) - (portalLength / 2), startHeight - 2])
    cube([width - wallThickness - 0.05, length - wallThickness - 0.05, 2]);
}

module windows(windowWidth, windowHeight, windowTopWidth, windowTopHeight, windowDistance, windowThickness, window2Building, buildingWidth, buildingLength, portalLength, wallHeight, wallThickness) {
  firstWindowX = buildingWidth;// - (windowWidth / 2);//+ (wallThickness / 2);
  firstWindowY = (buildingLength - window2Building) / 4;
  translate([-(buildingWidth / 2), -(buildingLength / 2) - (portalLength / 2) + window2Building, wallHeight]) {
    for (row = [1 : 2]) {
      for (col = [1 : 3]) {
        windowX = ((firstWindowX * row) - buildingWidth);
        windowY = ((firstWindowY * col) - firstWindowY) + ((window2Building / 2) * (col - 1));
        translate([windowX, windowY, 0])
          window(windowWidth, windowHeight, windowTopWidth, windowTopHeight, windowDistance, windowThickness);
      }
    }
  }
}

module window(width, height, topWidth, topHeight, distance, thickness) {
  color("lime")
  translate([thickness / 2, 0, 0])
  rotate([0, 0, 90]) {
    hull() {
      cube([width, thickness, (height - (width / 2))]);
      translate([(width / 2), thickness, (height - (width / 2))])
        rotate([90, 0, 0])
          cylinder(thickness, (width / 2), (width / 2));
    }
    translate([0, 0, height + distance])
    cube([topWidth, thickness, topHeight]);
  }
}

module portalPillars(portalWidth, portalLength, baseHeight, plateLength,
               pillarHeightTotal, pillarHeightBottom1, pillarHeightBottom2, pillarHeightTop1, 
               pillarHeightTop2, pillarDiagonalBottom, pillarDiagonalTop, pillarDiagonalBottom1,
               pillarDiagonalBottom2, pillarDiagonalTop1, pillarDiagonalTop2) {
  firstPillarX = (portalWidth / 5);
  firstPillarY = plateLength - pillarDiagonalBottom;
  translate([0, -(plateLength / 2), baseHeight]) {
    for (row = [1 : 5]) {
      if (row != 3) {
        pillarX = (portalWidth / 2) - ((pillarDiagonalBottom / 2) * 1.6955) + ((firstPillarX * row) - portalWidth);
        //echo("row", row, "X", pillarX);
        translate([pillarX, firstPillarY, 0])
          pillar(pillarHeightTotal, pillarHeightBottom1, pillarHeightBottom2, pillarHeightTop1, pillarHeightTop2,
                 pillarDiagonalBottom, pillarDiagonalTop, pillarDiagonalBottom1, pillarDiagonalBottom2, pillarDiagonalTop1, pillarDiagonalTop2);

      }
    }
  }
}

module portalWalls(width, thickness, height, wallDistance, portalWidth, portalLength, doorWidth, doorDepth, buildingLength, baseHeight) {
  color("yellow")
  translate([0, (buildingLength / 2) - (portalLength / 2), baseHeight])
  difference() {
    union() {
      translate([(wallDistance / 2), 0, 0])
        cube([width, thickness, height]);
      translate([-(wallDistance / 2) - width, 0, 0])
        cube([width, thickness, height]);
      translate([(doorWidth / 2), -doorDepth, 0])
        cube([width, thickness, height]);
      translate([-(doorWidth / 2) - width, -doorDepth, 0])
        cube([width, thickness, height]);
    }
    translate([-25, -10, -4])
    cube([50, 10, height + 8]);
  }
}

module portalRoof(portalWidthBottom, portalWidthTop1, portalWidthTop2, portalDepthBottom, 
                  portalDepthMidPlus, portalDepthTop1, portalDepthTop2, baseHeight, buildingLength, portalLength) {
  portalDepthMid = portalDepthBottom + portalDepthMidPlus;
  portalWidthMid = portalWidthBottom + portalDepthMidPlus;
  translate([-(portalWidthBottom / 2), (buildingLength / 2) - (portalLength / 2), baseHeight]) {
    color("lightblue")
      cube([portalWidthBottom, portalDepthBottom, 2]);
    color("blue")
    translate([0, portalDepthBottom, 2])
      rotate([0, 0, -90])
      rectangularPrism2(2, portalDepthBottom, portalDepthMid, portalWidthBottom, 4, true);
    translate([-(portalDepthMidPlus / 2), -(portalDepthMidPlus / 2), 4]) {
      color("darkblue")
      cube([portalWidthMid, portalDepthMid, 2]);
      color("blue")
      translate([0, portalDepthMid, 2])
        rotate([0, 0, -90])
        rectangularPrism2(2, portalDepthMid, portalDepthMid + portalDepthMidPlus, portalWidthMid, 4, true);
    }
  }
  color("darkblue")
  translate([-(portalWidthTop1 / 2), (buildingLength / 2) - (portalLength / 2), baseHeight + 8])
    cube([portalWidthTop1, portalDepthTop1, 2]);
  color("blue")
  translate([-(portalWidthTop2 / 2), (buildingLength / 2) - (portalLength / 2), baseHeight + 10]) {
    cube([portalWidthTop2, portalDepthTop2, 2]);
    translate([-1, 0, 2])
      cube([portalWidthTop2 + 2, portalDepthTop2 + 1, 0.2]);
  }
  color("purple")
  difference()
  {
    driehoek(buildingLength, portalLength, baseHeight, portalDepthTop1, portalDepthTop2, portalWidthTop1, portalWidthTop2);
    translate([0, portalDepthTop1, -1])
      driehoek(buildingLength, portalLength, baseHeight, portalDepthTop1, portalDepthTop2, portalWidthTop1, portalWidthTop2);
  }
}

module driehoek(buildingLength, portalLength, baseHeight, portalDepthTop1, portalDepthTop2, portalWidthTop1, portalWidthTop2) {
  translate([0, (buildingLength / 2) - (portalLength / 2), baseHeight + 12])
  difference() {
    translate([0, (portalDepthTop2 + 1) / 2, 6])
      cube([80, portalDepthTop2 + 1, 12], true);
    translate([0, portalDepthTop1 + 5, 2])
      rotate([180, 0, 0])
        roofTop(portalWidthTop2 + 30, portalDepthTop2 * 2, 18, 20, -(20 - 5.3));
  }
}

module roofTop(portalWidthTop, portalDepthTop, angle, thickness, height) {
  roofTopPlate(portalWidthTop, portalDepthTop, angle, thickness, height);
  mirror([1, 0, 0]) roofTopPlate(portalWidthTop, portalDepthTop, angle, thickness, height);
}

module roofTopPlate(portalWidthTop, portalDepthTop, angle, thickness, height) {
  width = ((portalWidthTop / 2) / cos(angle));
  translate([-(portalWidthTop / 2), 0, height])
    rotate([0, angle, 0])
      cube([width, portalDepthTop, thickness]);
}

function getOneOfTwo(value1, value2, smallest) = smallest ? getSmallest(value1, value2) : getBigest(value1, value2);
function getBigest(value1,   value2) = value1 > value2 ? value1 : value2;
function getSmallest(value1, value2) = value1 < value2 ? value1 : value2;

