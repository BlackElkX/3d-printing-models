$fn = 64;

totalLength = 83;
totalWidth  = 30;
firstPart   = 0.5;
secondPart  = totalLength/2;
placeBetweenSeats = 6.4;
qtySeatsLeft = 4;

difference() {
  length = totalLength / 2;
  width  = totalWidth  / 2;
  interior(length, width);
  furnitureHoles(length, width);
}

module interior(length, width) {
  translate([0, 0, 0]) {
    plate(length,     width);
    furniture(length, width);
  }
}

module furniture(length, width) {
  translate([firstPart, 0, 1])
    bankrow(2, qtySeatsLeft, length, width);

  translate([firstPart, 15, 1])
    bankrow(3, qtySeatsLeft, length, width);
}

module muren(width) {
  translate([0, 0, 1])
    cube([1, width, 23]);
  translate([0, -width, 1])
    cube([1, 10, 23]);
  translate([0, -10, 21])
    cube([1, 15, 3]);
}

module muren2(width) {
  translate([0, 5, 1])
    cube([1, 10, 23]);
  translate([0, -width, 1])
    cube([1, 10, 23]);
  translate([0, -10, 21])
    cube([1, 16, 3]);
}

module plate(length, width) {
  difference() {
    union() {
      platepart(length, width);
      rotate([0, 0, 180])
        platepart(length, width);
      mirror([1, 0, 0]) {
        platepart(length, width);
        rotate([0, 0, 180])
          platepart(length, width);
      }
    }
  }
}

module hole() {
  translate ([19, 15, -1]) {
    cylinder(3, 1.5, 1.5);
  }
}


module bougie() {
  translate ([76, 15-11, -1]) {
    cylinder(3, 1.43, 1.43);
  }
  translate ([83, 15-7.5, -1]) {
    cylinder(3, 1.43, 1.43);
  }
  color("red")
  translate ([82.6, -0.625, -1]) {
    rotate([0, 0, 85.7])
    rotate_extrude(angle = 60) {
      translate([6.65, 0])
      square(size = 2.86);
    }
  }
  translate ([83, 0, -1]) {
    cylinder(3, 1.43, 1.43);
  }
}

module generalHoles() {
  translate([40, 0, -1])
    cylinder(3, 1.5, 1.5);
  translate([0, 0, -1])
    cylinder(3, 0.5, 0.5);
  translate([-5.5, 14, -1])
    cube([11, 2, 3]);
  translate([-88, 14, -1])
    cube([11, 2, 3]);
  translate([77, 14, -1])
    cube([11, 2, 3]);
}

module platepart(length, width) { 
  translate([-length, -width, 0]) {
    difference() {
      cube([length, width, 1]);
    //  side();
      hole();
    }
  }
}

module bankrowtables(seats, qty, length, width) {
  translate([-length, -width, 0]) {
    bank(seats);
    for (count = [1 : qty]) {
      translate([20 * count, 0, 0])
        doublebank(seats);
    }
    translate([-10, 0 ,0])
      for (count = [1 : qty + 1]) {
        translate([20 * count, 0, 0])
         table(seats);
      }
    translate([20 * (qty + 1), 5 * seats, 0])
      rotate([0, 0, 180])
        bank(seats);
  }
}

module bankrow(seats, qty, length, width) {
  translate([-length, -width, 0]) {
    bank(seats);
    for (count = [1 : qty]) {
      translate([(10 + placeBetweenSeats) * count, 0, 0])
        doublebank(seats);
    }
    translate([(10 + placeBetweenSeats) * (qty + 1), 5 * seats, 0])
      rotate([0, 0, 180])
        bank(seats);
  }
}

module bank(seats) {
  cube([5.1, 5 * seats, 4.9]);
  cube([1.21, 5 * seats, 10.1]);
}

module doublebank(seats) {
  bank(seats);
  translate([0, 5 * seats, 0])
    rotate([0, 0, 180])
      bank(seats);
}

module table(seats) {
  cube([1, 5 * seats, 6]);
  translate([-2, 0, 6])
    cube([5, 5 * seats, 1]);
}

module side() {
  translate([-1, -1, -1])
    rotate([0, 0, -15.5])
      cube([12, 4, 3]);
}

module furnitureHoles(length, width) {
  translate([firstPart, 0.4, -0.4])
    bankrowHoles(2, qtySeatsLeft, length, width);

  translate([firstPart, 15, -0.4])
    bankrowHoles(3, qtySeatsLeft, length, width);

/*  translate([secondPart, 0, -0.4])
    bankrowtableHoles(2, 4, length, width);

  translate([secondPart, 20, -0.4])
    bankrowtableHoles(2, 4, length, width);*/
}

module bankrowtableHoles(seats, qty, length, width) {
  translate([-length, -width, 0]) {
    bankHole(seats);
    for (count = [1 : qty]) {
      translate([20 * count, 0, 0])
        doublebankHole(seats);
    }
    translate([20 * (qty + 1), (5 * seats) - 0.4, 0])
      rotate([0, 0, 180])
        bankHole(seats);
  }
}

module bankrowHoles(seats, qty, length, width) {
  translate([-length, -width, 0]) {
    bankHole(seats);
    for (count = [1 : qty]) {
      translate([(10 + placeBetweenSeats) * count, 0, 0])
        doublebankHole(seats);
    }
    translate([(10 + placeBetweenSeats) * (qty + 1), (5 * seats) - 0.4, 0])
      rotate([0, 0, 180])
        bankHole(seats);
  }
}

module bankHole(seats) {
  translate([0.6, 0.4, 0])
    cube([4.2, (5 * seats) - (0.4 * seats) - 0.4, 4.9]);
}

module doublebankHole(seats) {
  translate([-4.4, 0.4, 0])
    cube([8.2, (5 * seats) - (0.4 * seats) - 0.4, 4.9]);
}
