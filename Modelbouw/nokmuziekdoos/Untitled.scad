$fn      = 1024;
diameter =   8.33;
lengte   =  14.62;
hoogte   =   4.96;

translate([diameter / 2, 0, 0]) {
  difference() {
    nok(diameter, lengte, hoogte);
    translate([0, 0, -1])
    cylinder(10, 3.5 / 2, 3.5 / 2);
  }
}

//de grootte moet in dit kader kunnen.
//color("red") translate([0, -(diameter / 2), -1]) cube([lengte, diameter, 1]);

module nok(diameter, lengte, hoogte) {
  yShift = 10.1;
  cylinder(hoogte, diameter / 2, diameter / 2);
  translate([2.98, -yShift, 0]) {
    difference() {
      cylinder(hoogte, lengte, lengte);
      translate([11, -2.5, -1])
        cylinder(hoogte + 2, lengte, lengte);
      translate([-20, -62.69, -1])
        cube([170, 70, hoogte + 2]);
      translate([-14, 4.6, -1])
        cube([10, 10, hoogte + 2]);
      translate([lengte - 2.98 - (diameter / 2), 5, -1])
        cube([10, 10, hoogte + 2]);
      translate([-10, yShift + (diameter / 2), -1])
        cube([20, 10, hoogte + 2]);
    }
  }
}