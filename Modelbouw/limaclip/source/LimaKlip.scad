$fn = 64;
hoogte = 0.2;
stevigheid = 0.1;

scale([10, 10, 10])
klip();
//suportSmall();
//suport();
suportRound();

translate([62, 17.2, 0])
rotate([0, 0, 180])
  //suportSmall();
  //suport();
  suportRound();
color("red")
translate([22.2, 0, 0]) cube([18, 1.6, hoogte * 10]);
color("green")
translate([28.84, 15.67, 0]) rotate([0, 0, -0.7]) cube([11, 1.6, hoogte * 10]);

color("blue") {
  translate([ 1,     4.45, 0]) rotate([0, 0, -38.5])  cube([3,   1.45, hoogte * 10]);
  translate([ 2.35, 12.24, 0]) rotate([0, 0,  35.6])  cube([2.1, 1.45, hoogte * 10]);  
  translate([57.9,  13.4,  0]) rotate([0, 0, -53.05]) cube([2.4, 1.45, hoogte * 10]);
  translate([59.08,  2.84, 0]) rotate([0, 0,  49.8])  cube([2.1, 1.45, hoogte * 10]);
}

module suportRound() {
  difference() {
    suport();
    roundings();
  }
}

module roundings() {
  color("cyan") {
    difference() {
      union() {
        translate([0,   0,    -1]) cube([4.45, 4.25, 4]);
        translate([0,  12.8,  -1]) cube([4.45, 4.25, 4]);
      }
      translate([4.45,  4.25, -2]) cylinder(r = 2, h = 6);
      translate([4.45, 13.07, -2]) cylinder(r = 2, h = 6);
    }
  }
}

module suport() {
  translate([2.3, 2.5, 0])
    rotate([0, 0, -7])
      cube([20, 2, hoogte * 10]);
  translate([2.3, 12.8, 0])
    rotate([0, 0, 7])
      cube([20, 2, hoogte * 10]);
}

module suportSmall() {
  translate([2.3, 3.96, 0])
    rotate([0, 0, -11.1])
      cube([20.5, 2, hoogte * 10]);
  translate([2.3, 11.6, 0])
    rotate([0, 0, 10])
      cube([20.5, 2, hoogte * 10]);
}

module klip() {
  translate([-0.137, -0.142, 0])
    linear_extrude(height = hoogte) {
      import("limaklip1.dxf");
    }
  translate([-0.68, -0.142, hoogte])
    linear_extrude(height = 0.1) {
      import("limaklip2.dxf");
    }
}