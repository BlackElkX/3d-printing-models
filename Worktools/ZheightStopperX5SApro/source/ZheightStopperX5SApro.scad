$fn = 64;

difference(){ 
  cube([10, 20, 45]);
  translate([5, 5, -1])
    cylinder(47, 1.5, 1.5);
  translate([5, 5, 44])
    cylinder(2, 3, 3);
  translate([5, 5, 42.01])
    cylinder(2, 1.5, 3);
}