plateLength    =  60;
plateWidth     =  20;
plateThickness =   5;
screwDiameter  =   6.5;
gripLength     = 120;
gripFreeHeight =  20;
gripThickness  =  20;

$fn = 128;

handGrip(plateLength, plateWidth, plateThickness, screwDiameter, gripLength, gripFreeHeight, gripThickness);

module handGrip(plateLength, plateWidth, plateThickness, screwDiameter, gripLength, gripFreeHeight, gripThickness) {
  plateStartX = (plateLength - plateWidth) / 2;
  gripRadius  = gripThickness / 2;
  gripHeight  = gripFreeHeight + gripRadius;

  translate([0, 0, gripHeight])
  hull() {
    sphere(gripRadius);
    translate([0, gripLength, 0])
      sphere(gripRadius);
  }

  cylinder(gripHeight, gripRadius, gripRadius);
  translate([0, gripLength, 0])
    cylinder(gripHeight, gripRadius, gripRadius);

  translate([-plateStartX, 0, 0])
    plate(plateLength, plateWidth, plateThickness, screwDiameter);

  translate([-plateStartX, gripLength, 0])
    plate(plateLength, plateWidth, plateThickness, screwDiameter);
}

module plate(length, width, thickness, screwDiameter) {
  difference() 
  {
    hull() {
      cylinder(thickness, width / 2, width / 2);
      translate([length - width, 0, 0])
        cylinder(thickness, width / 2, width / 2);
    }
    translate([0, 0, 0])
      screwhole(thickness + 2, screwDiameter);
    translate([length - width, 0, 0])
      screwhole(thickness + 2, screwDiameter);
  }
}

module screwhole(height, diameter) {
  translate([0, 0, -1])
    cylinder(height, diameter / 2, diameter / 2);
}
