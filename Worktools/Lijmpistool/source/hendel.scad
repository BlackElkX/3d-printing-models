$fn = 64;

dikte = 2;

difference() {
  vlak();
  translate([0, 0, -1])
    cylinder(10, 5.81 / 2, 5.81 / 2);
}

module vlak() {
  cylinder(dikte, (8.5 + 3.42) / 2, (8.5 + 3.42) / 2);
  cylinder(dikte+1.40, 8.5 / 2, 8.5  / 2);
  translate([0.7, -0.5, dikte]) {
    rotate([180, 0, 30]) {
      //cube([34.84, 10, dikte]);
      translate([1, 1, 0]) {
        minkowski() {
          cube([34.84-4, 10-4, dikte/2]);
          cylinder(dikte/2, 2, 2);
        }
      }
    }
  }
  afronding();
}

module afronding() {
  translate([23.5, 11.5, 0])
  rotate([0, 0, 34]) {
    translate([0, 0, -(13.4 / 2) + 2 + (dikte / 2)])
      cylinder(9.4, 5.8/2, 5.8/2);
    translate([0, 0, -(13.4 / 2) + (dikte / 2)])
      cylinder(2, 4/2, 5.8/2);
    translate([0, 0, (13.4 / 2) - 2 + (dikte / 2)])
      cylinder(2, 5.8/2, 4/2);
    scale([1, 1, dikte])
    rotate_extrude(angle = 55) {
      square([48.53, 1]);
    }
  }
  translate([33, 8.5, 0]) {
    rotate([0, 0, 30 + 14]) {
      cube([30, 10, dikte]);
      //translate([0,0,-4.5])
      //  cube([45, 3, 10]);
      translate([3, 3, -(10/5 - dikte/2)])
        rotate([90, 0, 0])
        minkowski() {
          cube([45 - 6, 10 - 6, 1.5]);
          cylinder(3/2, 3, 3);
        }
    }
  }
  translate([17.1, -8.3, 0]) {
    rotate([0, 0, 62.5]) {
      difference() {
        cube([64, 25, dikte]);
        translate([-1, -1, -1])
        cube([18, 9, dikte + 2]);
      }
    }
  }
}
