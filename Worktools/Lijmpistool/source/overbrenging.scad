$fn = 64;

breedte =  9.48;
dikte   =  3.82;
lengte  = 29.42;

boldiameter   = 6.13;
bolhoogte     = 6.42;
steundiameter = 4.10;

hull() {
  cylinder(dikte, breedte / 2, breedte / 2);
  translate([lengte - breedte, 0, 0])
    cylinder(dikte, breedte / 2, breedte / 2);
}

hoogteBollen = bolhoogte + dikte - (boldiameter / 2);

translate([0, 0, hoogteBollen])
  sphere(d = boldiameter);
translate([lengte - breedte, 0, hoogteBollen])
  sphere(d = boldiameter);

translate([0, 0, 1])
  cylinder(4, steundiameter / 2, steundiameter / 2);
translate([lengte - breedte, 0, 1])
  cylinder(4, steundiameter / 2, steundiameter / 2);

dikte2   = 1.89;
breedte2 = 2.75;
breedte3 = 2.17;
lengte2  = 7.05;
translate([0, 0, -2.98]) {
  hull() {
    cylinder(dikte2, breedte3 / 2, breedte3 / 2);
    translate([lengte2 - breedte2, 0, 0])
      cylinder(dikte2, breedte2 / 2, breedte2 / 2);
  }
  translate([lengte2 - breedte2, 0, 1])
    cylinder(4, breedte2 / 2, breedte2 / 2);
}