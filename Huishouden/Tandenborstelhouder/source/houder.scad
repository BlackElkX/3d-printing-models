$fn = 64;

houder(90, 140, 12, 4.4, 10);

module houder(width, length, height, rounding, rand) {
  difference() {
    basis (width, length, height, rounding);
    translate([-((width / 2)              - rand), -((length / 2) - (((9 * 10) + 2) / 2) - rand / 2), -1]) flosstokskes(height, 1.4, 9, 3, 10);
    translate([ ((width / 2) - (36   / 2) - rand), -20, -1]) mondspray(height, 37);
    translate([ ((width / 2) - (46.5 / 2) - rand),  35, -1]) oplader(height, 48, 62, 9, 32);
    translate([ -7,                                -10, -1]) tongschraper(height, 5, 18.1);
    translate([ -7,                                -35, -1]) soloborstel(height, 5.5, 11);
    translate([-((width / 2)      - (rand * 1.2)), -30, -1]) borstelkes(height, 12.5);
    translate([-((width / 2) - 15 - (rand * 1.2)), -30, -1]) borstelkes(height, 12.5);
    translate([-(width / 2) + 1.4, -(length / 2) + rand, height - 1]) naam("🎻 Arne 🎜", height, 14);
    //"Moeke & Vake"
    /*
      -🎜- 2x achtste noten omhoog
      -🎝- 2x achtste noten omlaag
      -🎵- 1x achtste noot
      -🎶- 3x achtste noten
      -🎹- pianoklavier
      -🎼- solsleutel
      -🎘- midi-keyboard
      -- gamecontroller
      -🍐- peer
      -🍒- kersen
      -🍓- aardbij
      -📷- fototoestel
      -🐑- schaap
      -🎻- altviool
      -🍎- appel
      -🍏- appel
      -🐧- pinguin
    */
  }
}

module basis(width, length, height, roundingRad) {
  hull() {
    X = ((width  / 2) - roundingRad);
    Y = ((length / 2) - roundingRad);
    translate([ X,  Y, 0]) cylinder(height, roundingRad, roundingRad);
    translate([-X,  Y, 0]) cylinder(height, roundingRad, roundingRad);
    translate([-X, -Y, 0]) cylinder(height, roundingRad, roundingRad);
    translate([ X, -Y, 0]) cylinder(height, roundingRad, roundingRad);
  }
}

module tongschraper(height, width, length) {
  hull() {
    cylinder(height + 2, width / 2, width / 2);
    translate([0, length - width, 0])
      cylinder(height + 2, width / 2, width / 2);
  }
}

module soloborstel(height, width, length) {
  hull() {
    cylinder(height + 2, width / 2, width / 2);
    translate([0, length - width, 0])
      cylinder(height + 2, width / 2, width / 2);
  }
}

module mondspray(height, diagonal) {
  cylinder(height + 2, diagonal / 2, diagonal / 2);
}

module flosstokskes(height, diagonal, space, rows, cols) {
  for (row = [0 : rows - 1]) {
    for (col = [0 : cols - 1]) {
      translate([row * space, col * space, 0]) cylinder(height + 2, diagonal / 2, diagonal / 2);
    }
  }
}

module oplader(height, width, length, cableHeight, cableWidth) {
  scale([1, length / width, 1]) cylinder(height + 2, width / 2, width / 2);
  translate([-(cableWidth / 2), length / 3, 0]) cube([cableWidth, 20, cableHeight]);
}

module borstelkes(height, diameter) {
  cylinder(height + 2, diameter / 2, diameter / 2);
}

module naam(tekst, height, fontSize) {
  textToPrint = tekst;
  fontName    = "SegoeUISymbol:style=Bold";
  //Herculanum Cooper GhotiMystFont Krungthep BrushScriptMT SegoePrint SegoeScript SegoeUISymbol
  fontHeight  = height;
  fontBottom  = 1;
  fontStart   = 2;
  labelHeight = fontSize + 3 + fontBottom;
  labelLength = 42;
  labelWidth  = 2;
  oogDiameter = 2.7;
  linear_extrude(fontHeight) scale([1, 1]) text(textToPrint, font = fontName, size = fontSize);
}