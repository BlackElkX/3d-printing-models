$fn = 72;

ronding = 20;
lengte  = 80 - ronding * 2;
dikte   = 2;

translate([ronding, ronding, 0]) 
hull() {
  cylinder(dikte, ronding, ronding);
  translate([lengte, 0, 0]) cylinder(dikte, ronding, ronding);
  translate([0, lengte, 0]) cylinder(dikte, ronding, ronding);
}