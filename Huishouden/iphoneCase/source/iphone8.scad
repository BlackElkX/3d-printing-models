use <Iphone7rand.scad>;
use <Iphone6plusrand.scad>;
use <iphone5c.scad>;

iphone5c();

//iphone678();
//iphone678p();

module iphone5c() {
  case5c();
  translate([140, 0, 1])
    back("5c");
}  

module iphone678() {
  case678();
  translate([140, 0, 1])
    back("6");
}

module iphone678p() {
  case678Plus();
  scale(1.15)
    translate([140, 0, 1])
      back("6p");
}

function plusLogoScale0(type) = ((type == "6p") ?   0.80 : (type == "6") ?   0.85 :   0.80);
function plusCasePoint0(type) = ((type == "6p") ?  36.00 : (type == "6") ?  35.00 :  33.00);
function plusCasePoint1(type) = ((type == "6p") ? 115.00 : (type == "6") ? 118.00 : 112.00);
function plusCasePoint2(type) = ((type == "6p") ? 115.00 : (type == "6") ? 110.00 : 112.00);
function plusCasePoint3(type) = ((type == "6p") ?  54.00 : (type == "6") ?  54.00 :  50.00);
function plusCasePoint4(type) = ((type == "6p") ?  54.00 : (type == "6") ?  54.00 :  50.00);
function plusCasePoint5(type) = ((type == "6p") ?   0.00 : (type == "6") ?   0.00 :   0.00);
function plusCasePoint6(type) = ((type == "6p") ?   0.00 : (type == "6") ?   0.00 :   0.00);
function plusCasePoint7(type) = ((type == "6p") ?   0.00 : (type == "6") ?   0.00 :  10.00);
function plusCasePoint8(type) = ((type == "6p") ?   0.00 : (type == "6") ?   0.00 :  10.00);

module back(type) {
  difference() {
    rotate([0, 0, 90])
      backplane(type);
    if ((type == "5c")) {
      translate([-140, -80, 0.82])
        cube([160, 160, 10]);
    } else {
      translate([-140, -80, 0.5])
        cube([160, 160, 10]);
    }
    translate([-60, plusCasePoint0(type), 0])
      rotate([0, 0, 90])
        mirror([1, 0, 0])
            logo(plusLogoScale0(type));
  }
}

module case5c() {
  translate([0, 0, -19.2])
    rotate([90, 0, 90])
      iPhone5cCase();
}

module case678() {
  translate([70, 35, 5])
    iphone7();
}

module case678Plus() {
  rotate([90, 0, 90])
    object1();
}


module backplane(type) {
  translate([8, 8, 5]) {
    hull() {
      translate([plusCasePoint6(type), plusCasePoint7(type), -1])
        sphere(5);
      translate([plusCasePoint5(type), plusCasePoint1(type), -1])
        sphere(5);
      translate([plusCasePoint3(type), plusCasePoint2(type), -1])
        sphere(5);
      translate([plusCasePoint4(type), plusCasePoint8(type), -1])
        sphere(5);
    }
    if ((type == "6") || (type == "5c")) {
      hull() {
        translate([0, 110, -1])
          sphere(5);
        translate([0, 122, -1])
          sphere(5);
        translate([28, 122, -1])
          sphere(5);
        translate([28, 110, -1])
          sphere(5);
        if ((type == "5c")) {
         // translate([21, 110, -5.9])
         //   cube([10, 10, 8]);
          translate([14, 116, -5.9])
            rotate([0, 0, -45])
              cube([20, 10, 5]);
        }
      }
    }
  }
}

module logo(schaal) {
  $fn     = 128;
  cirkel0 =  28;
  scale(schaal) {
    translate([0, 0, -5]) {
      difference() {
        cylinder(10, cirkel0, cirkel0);
        translate([0, 0, -1]) {
          cylinder(12, cirkel0 - 1, cirkel0 - 1);
          for (count = [0 : 7]) {
            rotate([0, 0, ((360 / 8) * count)])
              translate([cirkel0-2, -2.5, -10])
                cube([5, 5, 50]);
          }
        }
      }
    }
    difference() {
      binnenCirkels();
      translate([-50, 0, -6])
      cube([100, 100, 20]);
    }
    translate([-28.5, -8, -7])
    tekst("LMK", 13);
  }
  tekst("LMK2", 8);
}

module binnenCirkels() {
  cirkel1 = 17;
  cirkel2 = 13.5;
  cirkel3 = 10;
  translate([0, 0, -5])
  difference() {
    cylinder(10, cirkel1, cirkel1);
    translate([0, 0, -1])
      cylinder(12, cirkel1 - 1, cirkel1 - 1);
  }
  translate([0, 0, -5])
  difference() {
    cylinder(10, cirkel2, cirkel2);
    translate([0, 0, -1])
      cylinder(12, cirkel2 - 1, cirkel2 - 1);
  }
  translate([0, 0, -5])
  difference() {
    cylinder(10, cirkel3, cirkel3);
    translate([0, 0, -1])
      cylinder(12, cirkel3 - 1, cirkel3 - 1);
  }
}

module tekst(textToPrint, fontSize) {
  fontName   = "Avenir Next";
  fontHeight = 10;
  fontBottom = 10;
  fontStart  = 10;
  labelWidth = 2;
  
  translate([fontStart, fontBottom, labelWidth])
    linear_extrude(fontHeight)
      scale([1, 1])
        text(textToPrint, font = fontName, size = fontSize);
}