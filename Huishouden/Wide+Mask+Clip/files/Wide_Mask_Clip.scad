$fn       =  50;
offs      =  -0.12;
mink      =   0.12;
thickness =   0.9;
w         = 220;    //Minimum:  75, Standard: 120. Änderung nur am Mittelsteg
h         =  40;    //Standard: 40. Änderung verzerrt

module logo() {
  translate([0, 0, 0])
  scale([0.08, 0.08, 0.08])
  text("Wij houden van Oompie",
       size = 10, valign = "center", halign = "center");
}

module webdings() {
  scale([0.08, 0.08, 0.08])
  text("Y",
       size = 10, valign = "center", halign = "center",
       font = "Webdings");
}

module quadrant() {
  union(){
    translate([(w-80)/10,0,0])
    polygon([[0,0],[0,1.5],[1.5,1.5],[3,3],
      [4.5,3],[4.5,3.5],[4,4],[5,4],
      [5.8,3.2],[6.8,3.2],[6,4],[7,4],[8,3],[8,2],
      [7.2,1.2],[7.2,0]
    ]);
    polygon([[0,0],[0,1.5],[(w-80)/10,1.5],[(w-80)/10,0]]);
  }
}

module body() {
  union(){
    quadrant();
    mirror([1,0,0]) quadrant();
    mirror([0,1,0]) quadrant();
    mirror([1,0,0]) mirror([0,1,0]) quadrant();
  }
}
  
module loecher() {
  union(){
    translate([(w-80)/10,0,0])
    union(){
      polygon([[6,0],[6,1],[5,2],[4,1],[4,0]]);
      polygon([[3,0],[3,1],[2,0]]);
      polygon([[7,0],[7.8,0],[7.8,1.8],[7,1]]);
      polygon([[6,3],[7,3],[6.2,3.8],[5.2,3.8]]);
      polygon([[4,3],[5,3],[4.2,3.8],[3.2,3.8]]);
      polygon([[0,1],[1,1],[2,2],[0,2]]);
    }
    polygon([[0,1],[(w-80)/10,1],[(w-80)/10,2],[0,2]]);
  }
}

module holes() {
  union(){
    loecher();
    mirror([1,0,0]) loecher();
    mirror([0,1,0]) loecher();
    mirror([1,0,0]) mirror([0,1,0]) loecher();
  }
}

module bodyLogo() {
  backLogo();
  mirror([1,0,0]) backLogo();
  mirror([0,1,0]) backLogo();
  mirror([1,0,0]) mirror([0,1,0]) backLogo();  
}
module backLogo() {
  cube([(w / 2) - 30, 4, thickness / 2]);
}

linear_extrude(height = thickness, center = false, convexity = 5) {
  resize([w, h]) difference(){
    minkowski(){
      offset(r=offs) body();
      circle(mink);
    }
    minkowski(){
      offset(r=offs) holes();
      circle(mink);
    }
    logo();
    translate([10, 0, 0])
    webdings();
    translate([-10, 0, 0])
    mirror([1, 0, 0]) webdings();
  }
}//*/

bodyLogo();

