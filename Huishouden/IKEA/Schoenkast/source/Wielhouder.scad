$fn = 64;
breedte          = 56.05;
breedteversterkt = 57.05;
dikte            = 23.63;
dikteversterkt   = 24.63;
hoogte           = 11.06;

schroefdia       =  6.37;
verzinkdia       =  9.57;

openingdia       = 12.06;
wieldia          = 18.50;

plaatdikte       =  2.97;
schroefafstand   = 25.43 - schroefdia;
wieluitsparingx  = 21.00;
wieluitsparingy  =  9.86;
wielasdia        = 12.08;
wielasdoorgang   = 12.20;

beneden(plaatdikte, dikte);
translate([0, 0, plaatdikte - 1])
  midden(hoogte - (plaatdikte * 2) + 2, dikte, dikteversterkt);
translate([0, 0, hoogte - plaatdikte])
  boven(plaatdikte, dikte);

module plaat(breedte, hoogte, dikte) {
  hull() {
    translate([dikte / 2, 0, 0])
      cylinder(hoogte, dikte / 2, dikte / 2);
    translate([breedte - (dikte / 2), 0, 0])
      cylinder(hoogte, dikte / 2, dikte / 2);
  }
}

module beneden(hoogte, dikte) {
  difference() {
    plaat(breedte, hoogte, dikte);
    schroeven();
  }
}

module midden(hoogte, dikte, dikteversterkt) {
  color("green")
  difference() {
    translate([-((breedteversterkt - breedte) / 2), 0, 0])
      plaat(breedteversterkt, hoogte, dikteversterkt);
    uitsparingMidden(hoogte, dikte);
     translate([breedte, 0, 0])
      mirror([1, 0, 0])
        uitsparingMidden(hoogte, dikte);
    translate([0, dikte / 2, -1])
    cube([60, 15, hoogte + 2]);
  }
}

module uitsparingMidden(hoogte, dikte) {
  translate([dikte / 2, 0, -1])
    cylinder(hoogte + 2, wieldia / 2, wieldia / 2);
  translate([-5, 0, -1])
    cube([wieluitsparingx+5, 15, hoogte + 2]);
  translate([-5, -1, -1])
    cube([10, 15, 10]);
}

module boven(hoogte, dikte) {
  color("red")
  difference() {
    plaat(breedte, hoogte, dikte);
    uitsparingBoven(hoogte, dikte);
     translate([breedte, 0, 0])
      mirror([1, 0, 0])
        uitsparingBoven(hoogte, dikte);
  }
}

module uitsparingBoven(hoogte, dikte) {
  translate([dikte / 2, 0, -1])
    hull() {
      cylinder(hoogte + 2, wielasdia / 2, wielasdia / 2);
      translate([-10, 10, 0])
      cylinder(hoogte + 2, wielasdia / 2, wielasdia / 2);
    }
  translate([0, 0, -1])
    cube([wielasdoorgang, 15, hoogte + 2]);
}

module schroeven() {
  schroefGat(dikte / 2, 0, -1, plaatdikte + 2, schroefdia/2, plaatdikte - 1, verzinkdia/2, plaatdikte - 0.5);
  schroefGat(breedte - (dikte / 2), 0, -1, plaatdikte + 2, schroefdia/2, plaatdikte - 1, verzinkdia/2, plaatdikte - 0.5);
}

module schroefGat(schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ, hoogte, diameter, verzinkhoogte, verzinkdiameter, top) {
  translate([schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ]) {
    faktor = 0.1;
    cylinder(hoogte, diameter, diameter, false);
    translate([0, 0, top])
    cylinder(verzinkhoogte + faktor, diameter, verzinkdiameter + faktor, false);
  }
}