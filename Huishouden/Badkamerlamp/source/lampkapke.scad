use <../../../Common/source/Honinggraad.scad>;

$fn = 256;

lampkap(220, 50, 2, 15, 210);
//color ("green")  klipkes(220, 16.06, 136, 3, 11, 2, 1.2);

module lampkap(lengte, breedte, wanddikte, ledStripBreedte, ledStripLengte) {
  color ("yellow") lampkapke(lengte, breedte, wanddikte);
  color ("red")    vulling(lengte - wanddikte, breedte, wanddikte, ledStripBreedte, ledStripLengte);
  color ("blue")   bodem(breedte, lengte, wanddikte, ledStripBreedte, ledStripLengte);
}


module bodem(breedte, lengte, dikte, ledBreedte, ledLengte) {
  difference() {
    translate([-((breedte / 2) - (dikte / 4)), 0, 0])
      cube([breedte - (dikte / 2), lengte, dikte]);
    ledStrip(lengte, dikte, ledBreedte, ledLengte);
  }
}

module klipkes(lengte, xAfstand, yAfstand, hoogte, breedte, dikte, nokDiepte) {
  translate([0, lengte / 2, -hoogte]) {
    mirror([0, 0, 0])
      klip(xAfstand, yAfstand, hoogte, breedte, dikte, nokDiepte, 0.5);
    mirror([1, 0, 0])
      klip(xAfstand, yAfstand, hoogte, breedte, dikte, nokDiepte, 0.5);
    mirror([0, 1, 0]) {
      mirror([0, 0, 0])
        klip(xAfstand, yAfstand, hoogte, breedte, dikte, nokDiepte, 0.5);
      mirror([1, 0, 0])
        klip(xAfstand, yAfstand, hoogte, breedte, dikte, nokDiepte, 0.5);
    }
  }
}


module vulling(lengte, breedte, dikte, ledBreedte, ledLengte) {
  difference() {
    translate([-((breedte / 2) + 1) + 2, 0.5, 0])
      honeycombround(0, 0, (breedte - 2), (lengte + 1), ((breedte / 2) - 1));
    blok((lengte + 2), breedte, dikte);
    ledStrip(lengte, dikte, ledBreedte, ledLengte, false);
  }
}

module lampkapke(lengte, breedte, dikte) {
  rotate([-90, 0, 0])
  difference() {
    cylinder(lengte, (breedte / 2), (breedte / 2));
    translate([0,   0, 1])
      cylinder(lengte - 2, (breedte / 2) - (dikte / 2), (breedte / 2) - (dikte / 2));
    translate([-(breedte / 2) - 1, 0, -1])
      cube([breedte + 2, (breedte / 2) + 1, lengte + 2]);
  }
}

module blok(lengte, breedte, dikte) {
  translate([0, 220, 0])
  rotate([90, 0, 0])
  difference() {
    translate([-(breedte / 2), 0, 0])
      cube([breedte, breedte / 2, lengte]);
    translate([0, 0, -1])
      cylinder(lengte + 2, (breedte / 2) - (dikte / 2), (breedte / 2) - (dikte / 2));
  }
}

module klip(xAfstand, yAfstand, diepte, breedte, dikte, nokDiepte, nok) {
  translate([(xAfstand / 2), (yAfstand / 2), 0]) {
    cube([dikte, breedte, diepte]);
    translate([nok, 0, nokDiepte])
      rotate([-90, 0, 0])
        cylinder(breedte, dikte/2, dikte/2);
  }
}

module ledStrip(lengte, dikte, ledBreedte, ledLengte, blok = true) {
  if (blok) {
    translate([-(ledBreedte / 2), (lengte - ledLengte) / 2, -1])
      cube([ledBreedte, ledLengte, dikte + 2]);
  }
  translate([0, (lengte - ledLengte) / 2, -1])
    rotate([-90, 0, 0]) cylinder(ledLengte, ledBreedte / 2, ledBreedte / 2);
}