difference() {
    union() {
        cube([20, 39.8, 7]);
        //translate([0, -1, 6]) cube([20, 41.8, 1]);
        translate([0, -5, 11]) rotate([-45, 0, 0]) cube([20, 10, 2]);
        translate([0, 37.8, 4]) rotate([45, 0, 0]) cube([20, 10, 2]);
    }
    translate([-1, 3, 3]) cube([22, 33.8, 7]);
    translate([3, 15, -1]) cube([14, 10, 7]);
    translate([-1, -10, 8]) cube([22, 60, 7]);
    translate([-1, -10, 0]) cube([22, 9, 10]);
    translate([-1, 40.8, 0]) cube([22, 9, 10]);
    translate([10, 10, -2]) {
        cylinder(d = 3.5, h = 10, $fn = 65);
        translate([0, 0, 3.5]) cylinder(d1 = 3.5, d2 = 6.5, h = 2, $fn = 65);
    }
    translate([10, 30, -2]) {
        cylinder(d = 3.5, h = 10, $fn = 65);
        translate([0, 0, 3.5]) cylinder(d1 = 3.5, d2 = 6.5, h = 2, $fn = 65);
    }
}
