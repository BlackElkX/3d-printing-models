$fn = 64;

overgang(16, 40, 3, 18);

translate([-40, 0, 0])
overgang(16, 40, 3, 18);

module overgang(ingangradius, binnendiameter, wand, lengte) {
  binnenradius = (binnendiameter - wand) / 2;
  difference() {
    translate([1.5, 1.5, 0])
      cylinder(lengte, binnenradius, binnenradius);
    translate([0, 0, -1])
      cylinder(lengte + 2, ingangradius, ingangradius);
  }
}