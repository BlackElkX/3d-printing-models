$fn = 64;

overgang(32, 40, 3, 18, 1.5);

translate([-40, 0, 0])
overgang(32, 40, 0, 18, 1.5);

module overgang(ingangdiameter, binnendiameter, wand, lengte, asymetricValue) {
  ingangradius = ingangdiameter / 2;
  binnenradius = (binnendiameter - wand) / 2;
   
  difference() {
    translate([asymetricValue, asymetricValue, 0])
      cylinder(lengte, binnenradius, binnenradius);
    translate([0, 0, -1])
      cylinder(lengte + 2, ingangradius, ingangradius);
  }
}