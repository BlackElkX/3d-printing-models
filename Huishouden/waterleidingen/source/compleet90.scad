
use <Overgang40-32.scad>;
use <Bocht.scad>;

bocht(90);

translate([-20, 20, 0])
  rotate([90, -45, 90])
    overgang(30, 40, 3, 1.5);