
use <Overgang.scad>;
use <Bocht.scad>;

difference() {
  bochtje();
  translate([-32.000001, 20, 0])
    rotate([0, 90, 0])
      cylinder(20, 17, 17);
}

translate([-32, 20, 2.12])
  color("red")
    rotate([0, 90, 0])
      difference() {
        cylinder(30, 21.5, 21.5);
        translate([0, 0, -1])
          cylinder(32, 20, 20);
      }
      
module bochtje() {
  bocht(45);
  translate([-12, 20, 0])
    rotate([90, -45, 90])
      overgang(30, 42, 0, 10, 1.5);
}
