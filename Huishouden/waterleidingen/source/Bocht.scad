$fn = 64;

translate([-70, 0, 0])
bocht(45);

bocht(90);

module bocht(hoek) {
  difference() {
    translate([-20, 20, 0])
      rotate([0, 90, 0])
        cylinder(20, 16, 16);
    translate([-21, 20, 0])
      rotate([0, 90, 0])
        cylinder(42, 14, 14);
  }

  rotate([0, 0, 90 - hoek]) {
    difference() {
      rotate_extrude(angle = hoek)
        translate([20, 0, 0])
          circle(r = 16);
      rotate_extrude()
        translate([20, 0, 0])
          circle(r = 14);
    }

    difference() {
      translate([20, 0, 0])
        rotate([90, 0, 0])
          cylinder(10, 16, 16);
      translate([20, 1, 0])
        rotate([90, 0, 0])
          cylinder(42, 14, 14);
    }

    difference() {
      translate([20, 0, 0])
        rotate([90, 0, 0])
          cylinder(10, 17, 17);
      translate([20, 1, 0])
        rotate([90, 0, 0])
          cylinder(42, 15, 15);
    }

    difference() {
      translate([20, 0, 0])
        rotate([90, 0, 0])
          cylinder(40, 18, 18);
      translate([20, 1, 0])
        rotate([90, 0, 0])
          cylinder(42, 16, 16);
    }
  }
}
