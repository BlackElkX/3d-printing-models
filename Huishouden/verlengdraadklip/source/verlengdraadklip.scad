$fn = 64;

blokHoogte     = 20.00;
blokBreedte    = 10.00;
blokLengte     =  3.00;
kabel1diameter =  8.10;
kabel2diameter =  7.78;
klipdikte      =  2.48;
opening        =  6.40;

kabel1X =  0;
kabel1Y =  0;
kabel2X = 10.4;
kabel2Y =  0;

difference() {
  hoogte = blokHoogte + 2;
  blok();
  translate([kabel1X, kabel1Y, -1])
    kabel(kabel1diameter, hoogte);
  translate([kabel2X, kabel2Y, -1])
    kabel(kabel2diameter, hoogte);
  translate([-7, -(opening / 2), -1])
    cube([5, opening, hoogte]);
  translate([12, -(opening / 2), -1])
    cube([5, opening, hoogte]);
}

module blok() {
  translate([kabel2X - kabel2diameter + (klipdikte / 2), kabel2Y - (blokBreedte / 2), 0])
    cube([blokLengte, blokBreedte, blokHoogte]);
  translate([kabel1X, kabel1Y, 0])
    kabel(kabel1diameter + (klipdikte * 2), blokHoogte);
  translate([kabel2X, kabel2Y, 0])
    kabel(kabel2diameter + (klipdikte * 2), blokHoogte);
}

module kabel(diameter, hoogte) {
  cylinder(hoogte, diameter / 2, diameter / 2);
}