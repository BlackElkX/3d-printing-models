difference() {
  cube([8.3, 205, 35]);
  translate([2, -0.1, 2])
  cube([4.3, 200.1, 30]);
  translate([5, 0, 0])
    rotate([90, 0, 90])
      tekst("Broodmes van BlackElkX");
  translate([3, 205, 0])
    rotate([90, 0, -90])
      tekst("Broodmes van BlackElkX");
}

fontName   = "GhotiMystFont";
fontSize   = 11;
fontHeight = 3;
fontBottom = 10;
fontStart  = 10;
labelWidth = 2;

module tekst(textToPrint) {
  translate([fontStart, fontBottom, labelWidth])
    linear_extrude(fontHeight)
      scale([1, 1])
        text(textToPrint, font = fontName, size = fontSize);
}