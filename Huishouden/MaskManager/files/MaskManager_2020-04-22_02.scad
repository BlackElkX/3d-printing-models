$fn = 64;

thickness             = 2;

ribbonWidth           = 12;

magnetRadius          = 7.5;
magnetDepth           = 2;

offsetCubeWidth = magnetRadius + thickness * 4.5;
totalWidth      = max((magnetRadius * 2 + thickness * 2),
                      (ribbonWidth  * 2 + thickness * 3));

//$vpr = [55, 0, 25];
//$vpt = [ 0, 0,  1];
//$vpd = 65;

difference() {
  // The whole superstructure
  translate([0, 0, 0]) {
    cylinder(thickness + magnetDepth, (totalWidth / 2), (totalWidth / 2), center = false);
    translate([-offsetCubeWidth, (-0.5 * totalWidth), 0]) {
      cube([offsetCubeWidth, totalWidth, magnetDepth + thickness], center = false);
    }
  }
  // The cylindrical magnet
  translate([0, 0, thickness + 0.01]) {
    cylinder(magnetDepth, magnetRadius, magnetRadius, center = false);
  }
  // Slots for the ribbons
  translate([-(magnetRadius + thickness * 1),  (ribbonWidth / 2 + thickness / 2), 0]) {
    cube([thickness, ribbonWidth, magnetDepth + (thickness * 6)], center = true);
  }
  translate([-(magnetRadius + thickness * 3),  (ribbonWidth / 2 + thickness / 2), 0]) {
    cube([thickness, ribbonWidth, magnetDepth + thickness * 6], center = true);
  }
  translate([-(magnetRadius + thickness * 1), -(ribbonWidth / 2 + thickness / 2), 0]) {
    cube([thickness, ribbonWidth, magnetDepth + thickness * 6], center = true);
  }
  translate([-(magnetRadius + thickness * 3), -(ribbonWidth / 2 + thickness / 2), 0]) {
    cube([thickness, ribbonWidth, magnetDepth + thickness * 6], center = true);
  }
}