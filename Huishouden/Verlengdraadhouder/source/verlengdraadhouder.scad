
blok();

module blok() {
  difference() {
    ruweVorm();
    wegsnijden();  
  }
}

module ruweVorm() {
  //cube([100, 53.72, 37.63]);
  translate([0, 53.72/2, 37.63/2])
    rotate([0, 90, 0])
      scale([1.3, 1, 1])
        cylinder(100, 53.72/2, 53.72/2);
  translate([-10, 53.72/2, 37.63/2])
    rotate([0, 90, 0])
      cylinder(10, 14.9, 14.9);
}

module wegsnijden() {
  translate([-1, 0, 37.63])
    cube([102, 53.72, 37.63]);
  translate([-1, 0, -37.63])
    cube([102, 53.72, 37.63]);
}