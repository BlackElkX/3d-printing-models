$fn = 64;
// rotate_extrude() rotates a 2D shape around the Z axis. 
// Note that the 2D shape must be either completely on the 
// positive or negative side of the X axis.

/*
translate([-100, -100, 8.50])
  cube([200, 200, 2]);
/*
translate([92, 0, 0])
  cube([5, 5, 3.06]);
//*/


difference() {
  basis();
  wisserken();
}
//bovenkant();

difference() {
  houder();
  houdergat();
}

module basis() {
  color("green")
    translate([0, 0, -1.94])
      cylinder(5, 44, 44);
  color("blue")
    translate([0, 0, -1.94])
      cylinder(4.25, 46, 46);
  color("red")
    translate([0, 0, 2.5])
      rotate_extrude()
        translate([40, 0])
          circle(6);
}

module wisserken() {
  translate([0, 0, -7])
    cylinder(7, 50, 50);
}

module bovenkant() {
  color("orange")
  translate([0, 0, -1])
      cylinder(1, 0, 46);
}

module houder() {
  union() {
    color("lime")
      translate([0, 0, 3.06])
        scale([1, 1, 4.6])
        rotate_extrude()
          translate([14.565, 0])
            square(3.2);
    color("gray")
      translate([14.565-0.5, -7, 3.06])
        cube([1, 14, 14.7]);
    color("gray")
      translate([-14.565-0.5, -7, 3.06])
        cube([1, 14, 14.7]);
  }
}

module houdergat() {
  translate([-30, 0, 12])
  rotate([0, 90, 0])
  cylinder(60, 5.13/2, 5.13/2);
}