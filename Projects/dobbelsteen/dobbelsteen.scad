$fn = 32;

/*
  type:
   0 = normaal 
   1 = fate
   2 = D'ni 2x zero, 1, 2, 3, 4
   # = D'ni 4 dices without zeros
       3 =  1 -  6
       4 =  7 - 12
       5 = 13 - 18
       6 = 19 - 24
   # = D'ni 5 dices with each one 1 zero
       7 =  1 -  5
       8 =  6 - 10
       9 = 11 - 15
       10 = 16 - 20
       11 = 21 - 25
  20 = decimal 1 - 6
*/
type        = 1;

rounding    = 1;
size        = 8;
eyeRounding = rounding / 1.5;
eyePlace    = size / 1.8;
eyeDepth    = (size / 2) + rounding;
stoneColor  = "green";
symbolColor = "brown";
support     = false;

dicestone(type, rounding, size, eyeRounding, eyePlace, support, stoneColor, symbolColor);

translate([size + 10, -(size + 3.4), 0])
rotate([0, 90, 180])
dicestone(type, rounding, size, eyeRounding, eyePlace, support, stoneColor, symbolColor);

module dicestone(type, rounding, size, eyeRounding, eyePlace, support, stoneColor, symbolColor) {
  difference() {
    stone(rounding, size, stoneColor);
    if (type < 2) {
      ogen(type, eyeRounding, size, eyePlace, eyeDepth, symbolColor);
    } else {
      ogen(type, (eyeRounding / 1.5), size, eyePlace, eyeDepth, symbolColor);
    }
  }
  supports(rounding, size, support);
}

module ogen(type, eyeRounding, size, eyePlace, eyeDepth, symbolColor) {
  rotate([0,   0,   0]) one(type,   eyeRounding, size, eyePlace, eyeDepth, symbolColor);
  rotate([0,   0, 180]) two(type,   eyeRounding, size, eyePlace, eyeDepth, "blue");
  rotate([0,   0,  90]) three(type, eyeRounding, size, eyePlace, eyeDepth, "yellow");
  rotate([0,   0, -90]) four(type,  eyeRounding, size, eyePlace, eyeDepth, "orange");
  if (type == 2) {
    rotate([0, -90, 0]) zero(type,  eyeRounding, size, eyePlace, eyeDepth, "purple");
    rotate([0,  90, 0]) zero(type,  eyeRounding, size, eyePlace, eyeDepth, "lime");
  } else if ((type > 6) && (type < 12)) {
    rotate([0, -90, 0]) five(type,  eyeRounding, size, eyePlace, eyeDepth, "purple");
    rotate([0,  90, 0]) zero(type,  eyeRounding, size, eyePlace, eyeDepth, "lime");
  } else {
    rotate([0, -90, 0]) five(type,  eyeRounding, size, eyePlace, eyeDepth, "purple");
    rotate([0,  90, 0]) six(type,   eyeRounding, size, eyePlace, eyeDepth, "lime");
  }
}

module stone(rounding, size, stoneColor) {
  color(stoneColor)
  translate([-(size / 2), -(size / 2), -(size / 2)])
  hull() {
    sphere(rounding);
    translate([size, 0, 0])
      sphere(rounding);
    translate([size, size, 0])
      sphere(rounding);
    translate([0, 0, size])
      sphere(rounding);
    translate([0, size, size])
      sphere(rounding);
    translate([0, size, 0])
      sphere(rounding);
    translate([size, 0, size])
      sphere(rounding);
    translate([size, size, size])
      sphere(rounding);
  }
}

module supports(rounding, size, support) {
  if (support) {
    dia = 0.7;
    crd = (size / 2) - dia;
    translate([crd, crd, -2 - (size / 2) - rounding])
      cylinder(2, dia, dia);
    translate([crd, -crd, -2 - (size / 2) - rounding])
      cylinder(2, dia, dia);
    translate([-crd, -crd, -2 - (size / 2) - rounding])
      cylinder(2, dia, dia);
    translate([-crd, crd, -2 - (size / 2) - rounding])
      cylinder(2, dia, dia);
  }
}


module zero(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor) {
    dni00(rounding, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module one(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, 0, 0])
      sphere(rounding);
  } else if ((type == 2) || (type == 3) || (type == 7)) {
    dni01(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni07(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni13(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni19(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 8) {
    dni06(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 9) {
    dni11(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 10) {
    dni16(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 11) {
    dni21(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module two(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  } else if ((type == 2) || (type == 3) || (type == 7)) {
    dni02(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni08(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni14(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni20(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 8) {
    dni07(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 9) {
    dni12(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 10) {
    dni17(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 11) {
    dni22(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module three(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth, 0, 0])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  } else if (type == 1) {
    hull() {
      translate([-eyeDepth,  0, -(eyePlace / 2)])
        sphere(rounding);
      translate([-eyeDepth,  0,  (eyePlace / 2)])
        sphere(rounding);
    }
  }else if ((type == 2) || (type == 3) || (type == 7)) {
    dni03(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni09(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni15(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni21(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 8) {
    dni08(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 9) {
    dni13(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 10) {
    dni18(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 11) {
    dni23(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module four(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth, -(eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  }  else if (type == 1) {
    hull() {
      translate([-eyeDepth,  0, -(eyePlace / 2)])
        sphere(rounding);
      translate([-eyeDepth,  0,  (eyePlace / 2)])
        sphere(rounding);
    }
  } else if ((type == 2) || (type == 3) || (type == 7)) {
    dni04(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni10(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni16(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni22(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 8) {
    dni09(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 9) {
    dni14(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 10) {
    dni19(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 11) {
    dni24(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module five(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, 0, 0])
      sphere(rounding);
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth, -(eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  } else if (type == 1) {
    hull() {
      translate([-eyeDepth,  -(eyePlace / 2), 0])
        sphere(rounding);
      translate([-eyeDepth,   (eyePlace / 2), 0])
        sphere(rounding);
    }
    hull() {
      translate([-eyeDepth,  0, -(eyePlace / 2)])
        sphere(rounding);
      translate([-eyeDepth,  0,  (eyePlace / 2)])
        sphere(rounding);
    }
  } else if ((type == 2) || (type == 3) || (type == 7)) {
    dni05(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni11(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni17(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni23(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 8) {
    dni10(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 9) {
    dni15(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 10) {
    dni20(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 11) {
    dni25(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module six(type, rounding, size, eyePlace, eyeDepth, symbolColor) {
  //opgepast!! hier mag in het midden nieks staan! De support wordt hieraan vastgemaakt.
  color(symbolColor)
  if (type == 0) {
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth, -(eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  0, -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  0,  (eyePlace / 2)])
      sphere(rounding);
  } else if (type == 1) {
    hull() {
      translate([-eyeDepth,  -(eyePlace / 2), 0])
        sphere(rounding);
      translate([-eyeDepth,   (eyePlace / 2), 0])
        sphere(rounding);
    }
    hull() {
      translate([-eyeDepth,  0, -(eyePlace / 2)])
        sphere(rounding);
      translate([-eyeDepth,  0,  (eyePlace / 2)])
        sphere(rounding);
    }
  } else if ((type == 2) || (type == 3)) {
    dni06(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 4) {
    dni12(rounding, eyePlace, eyeDepth, size);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 5) {
    dni18(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  } else if (type == 6) {
    dni24(rounding, eyePlace, eyeDepth);
    dniFrame(rounding, size, eyePlace, eyeDepth);
  }
}

module dniFrame(rounding, size, eyePlace, eyeDepth, symbolColor) {
  hull() {
    translate([-eyeDepth,  -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  -(eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth,  (eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth, -(eyePlace / 2)-1, -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2)+1, -(eyePlace / 2)])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth, -(eyePlace / 2)-1, (eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2)+1, (eyePlace / 2)])
      sphere(rounding);
  }
}
module dni00(rounding, eyeDepth) {
  translate([-eyeDepth, 0, 0])
    sphere(rounding);
}

module dni01(rounding, eyePlace, eyeDepth) {
  hull() {
    translate([-eyeDepth,  0, -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  0,  (eyePlace / 2)])
      sphere(rounding);
  }
}

module dni02(rounding, eyePlace, eyeDepth, size) {
  verschuiving = (size / 2) - 0.2;// size 8 => 3.8 size 16 => 7.8
  translate([-eyeDepth, verschuiving + (eyePlace / 5), 0])
    rotate([-125, 0, 0])
      rotate([0, 90, 0])
        rotate_extrude(angle = 70)
          translate([verschuiving, 0, 0])
            circle(r = rounding);
}

module dni03(rounding, eyePlace, eyeDepth) {
  hull() {
    translate([-eyeDepth,  0, -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  0])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth,  (eyePlace / 2), 0])
      sphere(rounding);
    translate([-eyeDepth,  0,  (eyePlace / 2)])
      sphere(rounding);
  }
}

module dni04(rounding, eyePlace, eyeDepth) {
  hull() {
    translate([-eyeDepth,  0, -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  0,  (eyePlace / 5)])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth,  -(eyePlace / 2), (eyePlace / 5)])
      sphere(rounding);
    translate([-eyeDepth,  0,  (eyePlace / 5)])
      sphere(rounding);
  }
}

module dni05(rounding, eyePlace, eyeDepth) {
  rotate([90, 0, 0])
    dni01(rounding, eyePlace, eyeDepth);
}

module dni06(rounding, eyePlace, eyeDepth) {
  dni01(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni01(rounding, eyePlace, eyeDepth);
}

module dni07(rounding, eyePlace, eyeDepth, size) {
  dni02(rounding, eyePlace, eyeDepth, size);
  rotate([90, 0, 0])
    dni01(rounding, eyePlace, eyeDepth);
}

module dni08(rounding, eyePlace, eyeDepth) {
  dni03(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni01(rounding, eyePlace, eyeDepth);
}

module dni09(rounding, eyePlace, eyeDepth) {
  dni04(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni01(rounding, eyePlace, eyeDepth);
}

module dni10(rounding, eyePlace, eyeDepth, size) {
  rotate([90, 0, 0])
    dni02(rounding, eyePlace, eyeDepth, size);
}

module dni11(rounding, eyePlace, eyeDepth, size) {
  dni01(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni02(rounding, eyePlace, eyeDepth, size);
}

module dni12(rounding, eyePlace, eyeDepth, size) {
  dni02(rounding, eyePlace, eyeDepth, size);
  rotate([90, 0, 0])
    dni02(rounding, eyePlace, eyeDepth, size);
}

module dni13(rounding, eyePlace, eyeDepth, size) {
  dni03(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni02(rounding, eyePlace, eyeDepth, size);
}

module dni14(rounding, eyePlace, eyeDepth, size) {
  dni04(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni02(rounding, eyePlace, eyeDepth, size);
}

module dni15(rounding, eyePlace, eyeDepth) {
  rotate([90, 0, 0])
    dni03(rounding, eyePlace, eyeDepth);
}

module dni16(rounding, eyePlace, eyeDepth) {
  dni01(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni03(rounding, eyePlace, eyeDepth);
}

module dni17(rounding, eyePlace, eyeDepth, size) {
  dni02(rounding, eyePlace, eyeDepth, size);
  rotate([90, 0, 0])
    dni03(rounding, eyePlace, eyeDepth);
}

module dni18(rounding, eyePlace, eyeDepth) {
  dni03(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni03(rounding, eyePlace, eyeDepth);
}

module dni19(rounding, eyePlace, eyeDepth) {
  dni04(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni03(rounding, eyePlace, eyeDepth);
}

module dni20(rounding, eyePlace, eyeDepth) {
  rotate([90, 0, 0])
    dni04(rounding, eyePlace, eyeDepth);
}

module dni21(rounding, eyePlace, eyeDepth) {
  dni01(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni04(rounding, eyePlace, eyeDepth);
}

module dni22(rounding, eyePlace, eyeDepth, size) {
  dni02(rounding, eyePlace, eyeDepth, size);
  rotate([90, 0, 0])
    dni04(rounding, eyePlace, eyeDepth);
}

module dni23(rounding, eyePlace, eyeDepth) {
  dni03(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni04(rounding, eyePlace, eyeDepth);
}

module dni24(rounding, eyePlace, eyeDepth) {
  dni04(rounding, eyePlace, eyeDepth);
  rotate([90, 0, 0])
    dni04(rounding, eyePlace, eyeDepth);
}

module dni25(rounding, eyePlace, eyeDepth) {
  hull() {
    translate([-eyeDepth,  (eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth, -(eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  }
  hull() {
    translate([-eyeDepth, -(eyePlace / 2), -(eyePlace / 2)])
      sphere(rounding);
    translate([-eyeDepth,  (eyePlace / 2),  (eyePlace / 2)])
      sphere(rounding);
  }
}
