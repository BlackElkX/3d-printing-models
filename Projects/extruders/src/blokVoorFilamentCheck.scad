use <../../../Common/source/Honinggraad.scad>;

difference() {
  cube([30, 37.37, 5.5]);
  gaten();
  translate([10,  5,    -1]) cube([10, 27.37, 10]);
  translate([5,  10,    -1]) cube([20, 17.37, 10]);
}

difference() {
  translate([0, 0, 2.75]) honeycomb(30, 37, 5.5, 5, 1);
  gaten();
}

module gaten() {
  translate([ 5,  5,    -1]) cylinder(10, d = 3.2, $fs = 0.25);
  translate([25,  5,    -1]) cylinder(10, d = 3.2, $fs = 0.25);
  translate([25, 32.37, -1]) cylinder(10, d = 3.2, $fs = 0.25);
  translate([ 5, 32.37, -1]) cylinder(10, d = 3.2, $fs = 0.25);
}