$fn = 64;
    //"Moeke & Vake"
    /*
      -🎜-  2x achtste noten omhoog      -🎝- 2x achtste noten omlaag
      -🎵- 1x achtste noot             -🎶- 3x achtste noten
      -♬-  16de noot                   -♭-  mol
      -♮-  hersteld                    -♯-  kruis
      -🎹- pianoklavier                -🎼- solsleutel
      -🎘-  midi-keyboard                -- gamecontroller
      -🍐- peer                        -🍒- kersen
      -🍓- aardbij                     -📷- fototoestel
      -🐑- schaap                      -🎻- altviool
      -🍎- appel                       -🍏- appel
      -🐧- pinguin                     -🐎- paard1
      -🏇- paard2                      -🐴- paard3
      -🎠- paard4                      -⛵- boot
      -⚽- voetbal                     -🐬- dolfijn
      -🐇- konijn                      -🐕- hond
      -🏊- zwemmen                     -💃- dansen
      -🕮-📚- boeken                    -🖉-  potlood
      -🎸- gitaar                      -🐆- panter
      -🐖- varken                      -⛷- skie
      -🎾- tennis                      -🐼- panda
      -🐹- hamster                     -🐈- kat
      -🏀- basket                      --  fototoestel
      --  camera                      --  sleutel
      --  huis                        --  schilderspalet
      --  headset                     --  koptelefoon
      --  tv                          -🎢- rollercoaster
      -🐘- olifant                     -✈-  vliegtuig
      -⛴- boot
    */

labelLength  = 60;
labelWidth   =  2.0;

oogDiameter  =  2.7;
uitgesneden  = false;
alleenTekst  = true;

textToPrint1 = "TIV";
textToPrint2 = "";
textToPrint3 = "";
fontName1    = "Caranda:style=Bold";
fontName2    = "Caranda:style=Bold";
fontName3    = "SegoeUISymbol";

//Herculanum Cooper GhotiMystFont Krungthep
//BrushScriptMT SegoePrint SegoeScript SegoeUISymbol

fontSize1    = 10;
fontSize2    = 10;
fontSize3    = 10;
fontHeight1  =  0.5;
fontHeight2  =  1;
fontHeight3  =  1;
fontBottom1  =  5;
fontBottom2  =  9;
fontBottom3  =  7;//1.5
fontStart1   =  2;
fontStart2   =  4;
fontStart3   = 45;
fontDepth1   =  1;
fontDepth2   =  1;
fontDepth3   =  1;
labelHeight  = fontSize1 + 2 + max(fontBottom1, fontBottom2);

//h = 1, r1 = 1, r2 = 1, center = false

if (!alleenTekst) {
  sleutelhanger();
}
tekst();
//pianoklavier(6);

module pianoklavier(aantal) {
  for (blok = [0 : aantal - 1]) {
    x = 22.9 * blok;
    translate([x, 0, 0]) {
      linear_extrude(fontHeight3 + fontDepth3)
        scale([1, 1])
          text("🎹", font = fontName3, size = fontSize3);
      translate([13.4, 0, 0]) kortpianoklavier();
    }
  }
}

module kortpianoklavier() {
  difference() {
    linear_extrude(fontHeight3 + fontDepth3)
      scale([1, 1])
        text("🎹", font = fontName3, size = fontSize3);
    translate([fontSize3 - 1, -1, -1]) cube([5, fontSize3 + 3, fontHeight3 + 3]);
  }
  translate([fontSize3 - 14.019, 0, 0])
  difference() {
    linear_extrude(fontHeight3 + fontDepth3)
      scale([1, 1])
        text("🎹", font = fontName3, size = fontSize3);
    translate([-0.1, -1, -1]) cube([12.5, fontSize3 + 3, fontHeight3 + 3]);
  }
}

module sleutelhanger() {
  difference() {
    hull() {
      translate([0, (labelHeight / 2) - 1, 0])
        cylinder(labelWidth, labelHeight / 2, labelHeight / 2, false);
      translate([labelLength, (labelHeight / 2) - 1, 0])
        cylinder(labelWidth, labelHeight / 2, labelHeight / 2, false);
    }
    translate([-2, (labelHeight / 2) - 1, -0.5])
      cylinder(labelWidth + 1, oogDiameter, oogDiameter, false);
    if (uitgesneden) {
      teksten(labelWidth, textToPrint1, fontName1, fontStart1, fontBottom1, fontDepth1, fontHeight1, fontSize1,
                          textToPrint2, fontName2, fontStart2, fontBottom2, fontDepth2, fontHeight2, fontSize2,
                          textToPrint3, fontName3, fontStart3, fontBottom3, fontDepth3, fontHeight3, fontSize3);
    }
  }
  if (!uitgesneden) {
    teksten(labelWidth, textToPrint1, fontName1, fontStart1, fontBottom1, fontDepth1, fontHeight1, fontSize1,
                        textToPrint2, fontName2, fontStart2, fontBottom2, fontDepth2, fontHeight2, fontSize2,
                        textToPrint3, fontName3, fontStart3, fontBottom3, fontDepth3, fontHeight3, fontSize3);
  }
}

module tekst() {
  if (uitgesneden || alleenTekst) {
    translate([0, 25, 0])
      teksten(fontDepth1, textToPrint1, fontName1, fontStart1, fontBottom1, fontDepth1, fontHeight1, fontSize1,
                          textToPrint2, fontName2, fontStart2, fontBottom2, fontDepth2, fontHeight2, fontSize2,
                          textToPrint3, fontName3, fontStart3, fontBottom3, fontDepth3, fontHeight3, fontSize3);
  }
}

module teksten(labelWidth, textToPrint1, fontName1, fontStart1, fontBottom1, fontDepth1, fontHeight1, fontSize1,
                           textToPrint2, fontName2, fontStart2, fontBottom2, fontDepth2, fontHeight2, fontSize2,
                           textToPrint3, fontName3, fontStart3, fontBottom3, fontDepth3, fontHeight3, fontSize3) {
  color("red")
    translate([fontStart1, fontBottom1, labelWidth - fontDepth1])
      linear_extrude(fontHeight1 + fontDepth1)
        scale([1, 1])
          text(textToPrint1, font = fontName1, size = fontSize1);
  color("red")
    translate([fontStart2, fontBottom2, labelWidth - fontDepth2])
      linear_extrude(fontHeight2 + fontDepth2)
        scale([1, 1])
          text(textToPrint2, font = fontName2, size = fontSize2);
  color("green")
    translate([fontStart3, fontBottom3, labelWidth - fontDepth3])
      linear_extrude(fontHeight3 + fontDepth3)
        scale([1, 1])
          text(textToPrint3, font = fontName3, size = fontSize3);
}
