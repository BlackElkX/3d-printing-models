$fn = 360;

difference()
{
  buitenkant();
  translate([0, 0, 1]) binnenkant();
  translate([0, 0, 2.2]) gaten(9, 16.5, 2, 8, 25);
}
/*translate([0, 0, 15]) {
  binnenkant();
  translate([0, 0, 2.2]) gaten(18, 16.5, 2, 8, 25);
}//*/

module buitenkant() {
  difference() {
    union() {
      translate([15, -21, 0]) cube([22, 42, 4]);
      cylinder(4, 27, 27);
      translate([0, 0, 2.2]) gaten(9, 16.5, 3, 4, 25);
    }
    translate([0, 0, -1]) cylinder(8, 18, 18);
  }
}

module binnenkant() {
  lengte = 2;
  difference() {
    union() {
      cylinder(2, 25, 25);
      translate([15, -19, 0]) cube([15 + lengte, 38, 2]);
      translate([26 + lengte, -19, 1]) {
        translate([0, 0, 0  ]) cube([2  , 38, 2]);
        translate([1, 0, 1.5]) cube([5.5, 38, 4]);
        translate([1, 0, 0  ]) cube([2  , 38, 4]);
        translate([4, 0, 1.5]) rotate([-90, 0, 0]) cylinder(38, 2.5, 2.5);
      }
    }
    translate([0, 0, -1]) cylinder(4, 20, 20);
    translate([26 + lengte, -20, 3]) rotate([-90, 0, 0]) cylinder(42, 1, 1);
  }
}

module gaten(aantal, cirkelStraal, straal, hoogte, hoek) {
  for (count = [0:20:(20 * aantal)]) {
    rotate([0, 0, count]) translate([cirkelStraal, cirkelStraal, 0]) rotate([hoek, -hoek, 0]) cylinder(hoogte, straal, straal);
  }
}