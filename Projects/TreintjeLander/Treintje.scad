$fn = 64;

inschuifspeling = 0.2;

kabine();
bodem();
pin();
ketel();
neus();
dak();

module pin(speling = 0) {
  translate([0, 0, -2]) cylinder(d = 3 + speling, h = 10);
}

module ketel(speling = 0, tekenMond = true) {
  translate([0, 0, speling])
  difference() {
    translate([-5,    0,  8]) rotate([0, 90,   0]) cylinder(d = 16 + speling, h = 22);
    translate([25.50, 0, 10]) rotate([0, -7.5, 0]) cube([20, 30, 20], center = true);
    if (tekenMond) {
      mond();
    }
    neus(inschuifspeling);
    pin(inschuifspeling);
  }
}

module bodem() {
  difference() {
    translate([ 5, 0, 2.50]) cube([20,  20, 5], center = true);
    ketel(inschuifspeling, false);
    pin(inschuifspeling);
  }
}

module kabine() {
  color("red")
  difference() {
    translate([-12.5, 0, 11])   cube([15, 20, 22], center = true);
    translate([-20,   6, 16.5]) rotate([0, 90, 0]) cylinder(d = 4, h = 22);
    translate([-20,  -6, 16.5]) rotate([0, 90, 0]) cylinder(d = 4, h = 22);
    ketel(inschuifspeling);
    dak();
  }
}

module dak() {
  color("cyan")
  translate([-10, 0, 4]) {
    difference() {
      union() {
        translate([0, -4, 10]) rotate([15, 0, 0]) cube([20, 14, 20], center = true);
        translate([0,  4, 10]) rotate([-15, 0, 0]) cube([20, 14, 20], center = true);
      }
      cube([22, 40, 20], center = true);
      translate([0, 0, -4])
      union() {
        translate([0, -5, 10]) rotate([ 15, 0, 0]) cube([22, 16, 20], center = true);
        translate([0,  5, 10]) rotate([-15, 0, 0]) cube([22, 16, 20], center = true);
      }
    }
  }
}

module neus(speling = 0) {
  color("blue")
  translate([12, 0, 12])
  union() {
    rotate([0, 90, 0]) cylinder(d = 4 + speling, h = 4);
    translate([4, 0, 0]) sphere(d = 4 + speling);
  }
}

module mond() {
  color("orange")
  translate([12, 0, 9])
  rotate([0, 90, 0])
  difference() {
    translate([0, 0,  0]) cylinder(d = 11, h = 5);
    translate([0, 0, -1]) cylinder(d =  8, h = 7);
    translate([-2, 0,  1]) cube([7, 12, 10], center = true);
  }
}