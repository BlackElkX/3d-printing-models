/*
Original Code by Joerg Scheuermann: https://www.thingiverse.com/Joerg
Original on Thingiverse: https://www.thingiverse.com/thing:8239

Clip module, Toothpick diameter Variable,
OpenSCAD/Thingiverse Customizer optimization,
Multicolor Modifier Block and extra comments
added by Gasolin (Christian Riedl): https://www.gasolin.com/
                            https://www.thingiverse.com/Gasolin
Start and end clip - more readable code 
added by BlackElkX (Frederik De Kegel): https://www.ghoti.be
                            https://www.thingiverse.com/BalckElkX

Licensed under CC BY-SA 3.0
https://creativecommons.org/licenses/by-sa/3.0/
*/

/* [Size] */
// Width of the Chain Link
width              = 30;
// Length of the Chain Link
length             = 45;
// Height of the Chain Link
height             = 15;

/* [Hidden] */
// maximum allowed bending-angle
max_angle          = 50;

/* [Angles] */
// max. angle allowed to bend downwards
under_angle        = 10; // [50]
// max. angle allowed to bend upwards
over_angle         = 50; // [50]

/* [Variant] */
// true - connectors go from outside to inside; false - vise versa
inner_axis         =  0; // [1: true, 0: false]
// true - top is closed, no holes/clip; false - top is open
closed             =  0; // [1: true, 0: false]
// true - a clip is used an generated instead of the toothpick hole; only works if closed = false
clip               =  1; // [1: true, 0: false]
whatToPrint        =  0; // [0: clip & chackle, 1: schackle, 2: clip]

/* [start and end chackle] */
chackle_kind       =  3; // [0: normal, 1: start, 2: end, 3: normal with holes (to mount something on it)]
chackle_screw_dia  =  3.2;
chackle_screw_dist =  8;
chackle_screw_angl =  5; //the ribon will hang at this angle, to prevent touching other parts, like the time belts on a core-xy printer.

/* [Further Settings] */
// Tolerance for better fit
tolerance          =  0.2;
// Diameter of the toothpick
toothpick          =  2.1;
// Generate Modifier Block for Multicolor Stripe
multicolor_block   =  0; // [1:true, 0:false]
//Height of the Modifier (Multicolor) Block
mcb_height         =  2;
/*
lengthOfChackle    = (length - (height / 4.4));
startOfChackle     = lengthOfChackle - height / 7.5;// + (((max(length, 2 * (height + min(2, max(1, 0.1 * width))))) - (height)) / 1) + 0.1;
echo("Length of the chackle", lengthOfChackle);
echo("Start  of the chackle", startOfChackle);
color("blue") translate([-((width / 2) + 1), -startOfChackle, 0]) cube([1, lengthOfChackle, 1]);
*/
//debug(15, 18, 10, 30, 30, true, false);
//debug(width, length, height, under_angle, over_angle, true, false);
shackle();

// show three chain-links connected at maximum bending angles
module debug(width, length, height, under_angle, over_angle, inner_axis, closed) {
  thick  = min(2, max(1, 0.1 * width));
  radius = height / 2;
  len1   = max(length, 2 * (height + thick));
  len2   = len1 - (2 * radius);
  y_ofs  = (len2 / 2) + 0.1;
  z_ofs  = radius;

  chain_link(width, length, height, under_angle, over_angle, inner_axis, closed, 0, 0, 0, 0);
  translate(v = [0, -y_ofs, z_ofs]) rotate(a = [min(max_angle, under_angle), 0, 0]) translate(v = [0, -y_ofs, -z_ofs]) chain_link(width, length, height, under_angle, over_angle, inner_axis, closed, 0, 0, 0, 0);
  translate(v = [0,  y_ofs, z_ofs]) rotate(a = [min(max_angle, over_angle),  0, 0]) translate(v = [0,  y_ofs, -z_ofs]) chain_link(width, length, height, under_angle, over_angle, inner_axis, closed, 0, 0, 0, 0);
}

//Multicolor Block or Chain Link
module shackle() {
  if (multicolor_block) {
    translate([0, 0, height / 2]) cube([width + 2, length + 2, mcb_height], center = true);
  } else {
    chain_link(width, length, height, under_angle, over_angle, inner_axis, closed, clip, chackle_kind, chackle_screw_dia, chackle_screw_dist, chackle_screw_angl);
  }
}

module outline(width, length, height, radius, len2, angle, chackle_kind) {
  y_ofs = len2   / 2;
  t     = radius / 2;

  difference() {
    union() {
      translate([0, 0, height / 2]) cube([width, len2, height], true);
      if ((chackle_kind == 0) || (chackle_kind == 1) || (chackle_kind == 3)) { 
        translate([0,  y_ofs, radius]) rotate([0,          90, 0])                    cylinder(width, r = radius, center = true, $fs = 0.5);
        translate([0,  y_ofs, radius]) rotate([360 - angle, 0, 0]) translate([0, t, -t]) cube([width, radius, radius], true);
      }
      if ((chackle_kind == 0) || (chackle_kind == 2) || (chackle_kind == 3)) { 
        translate([0, -y_ofs, radius]) rotate([0,    90, 0])                     cylinder(width, r = radius, center = true, $fs = 0.5);
        translate([0, -y_ofs, radius]) rotate([angle, 0, 0]) translate([0, -t, -t]) cube([width, radius, radius], true);
      }
      if (chackle_kind == 1) {  //left or start chackle
        translate([0, -(len2 / 2), height / 2]) cube([width, len2 * 2, height], true);
      } else if (chackle_kind == 2) {  //right or end chackle
        translate([0,  (len2 / 2), height / 2]) cube([width, len2 * 2, height], true);
      }
    }
    color("green") translate([0, 0, -(t / 2)]) cube([width, length * 2, t], true);
  }
}

module axis(x_ofs, y_ofs, z_ofs, length, radius) {
  rad = (radius * 0.25);
  translate([x_ofs, y_ofs, z_ofs]) rotate([0, 90, 0]) cylinder(length + tolerance, r1 = rad * 1.25, r2 = rad, center = true, $fs = 0.25);
}

module axis_hole(x_ofs, y_ofs, z_ofs, length, radius) {
  rad = (radius * 0.25) * 1.25;
  translate([x_ofs, y_ofs, z_ofs]) rotate([0, 90, 0]) cylinder(length + tolerance, r1 = rad + tolerance, r2 = rad, center = true, $fs = 0.25);
}

module incut(width, radius, len2, h1, thick, angle, inner_axis, chackle_kind) {
  width2 = width - 2 * (thick - tolerance);

  translate([0, -(len2 / 2), h1]) {
    difference() {
      union() {
        if (chackle_kind != 1) {
          translate([0, 0, -(h1 / 2)])                                                   cube([width2, radius * 2, h1], true);
          translate([0, 0, 0]) rotate([180 - angle, 0, 0]) translate([0, radius / 2, 0]) cube([width2, radius, radius * 2], true);
          translate([0, 0, 0]) rotate([0,          90, 0])                            cylinder(width2, r = radius, center = true, $fs = 0.5);
          if (!inner_axis) {
            axis_hole((width - thick) / 2 + tolerance, -(tolerance / 2), 0, thick, radius);
            mirror([1, 0, 0]) axis_hole((width - thick) / 2 + tolerance, -(tolerance / 2), 0, thick, radius);
          }
        }
      }
      if (inner_axis) {
        if (chackle_kind != 1) {
          axis(-((width2 - thick) / 2), -(tolerance / 2), 0, thick, radius);
          mirror([1, 0, 0]) axis(-((width2 - thick) / 2), -(tolerance / 2), 0, thick, radius);
        }
      }
    }
  }
}

module outcut(width, radius, len2, h1, thick, angle, inner_axis) {
  translate([0, len2 / 2, 0]) union() {
    outcut0(width, radius, h1, thick, angle, inner_axis);
    mirror([1, 0, 0]) outcut0(width, radius, h1, thick, angle, inner_axis);
  }
}

module outcut0(width, radius, h1, thick, angle, inner_axis) {
  w = (width - thick) / 2;
  t = thick + tolerance / 2;

  translate([w, 0, 0]) union() {
    difference() {
      union() {
        translate([0, 0, h1 / 2]) cube([t, radius * 2, h1], true);
        translate([0, 0, h1]) rotate([angle, 0, 0]) translate([0, radius / 2, 0]) cube([t, radius, radius * 2], true);
        translate([0, 0, h1]) rotate([0,    90, 0]) cylinder(t, r = radius, center = true, $fs = 0.5);
        if (inner_axis) {
          axis_hole(-(1.5 * w), tolerance / 2, h1, w, radius);
        }
      }
      if (!inner_axis) {
        axis(-(tolerance) / 2, tolerance / 2, h1, thick, radius);
      }
    }
  }
}

module middle(width, radius, length, height, len2, h1, thick, over, under, inner_axis, closed) {
  l  = length;
  th = max(thick, thick * height / width);
  h  = height - (closed ? 2 : 1) * th;
  w  = width - 4 * thick;
  r  = h / 2;
  scale(v = [w / h, 1, 1]) translate(v = [0, 0, r + th]) union() {
    translate([0, h1, 0]) middle_0(l, r, th, closed);
    translate([0, (len2 / 2) + 0.1, 0]) {
      difference() {
        rotate([over, 0, 0]) union() {
          translate([0, l / 4,  0]) middle_0(l / 2, r, th, closed);
          translate([0, l / 4, -r]) cube([2 * r, l / 2, 2 * r], true);
        }
        translate([0, l / 4, -r]) cube([2 * r, l / 2, 2 * r], true);
      }
      difference() {
        rotate([-under, 0, 0]) union() {
          translate([0, l / 4, 0]) middle_0(l / 2, r, th, closed);
          translate([0, l / 4, r]) cube([2 * r, l / 2, 2 * r], true);
        }
        translate([0, l / 4, r]) cube([2 * r, l / 2, 2 * r], true);
      }
    }
  }
}

module middle_0(length, radius, thick, closed) {
  if (closed) {
    cube([radius * 2, length, radius * 2], true);
  } else {
    rotate([90, 90, 0])                              cylinder(length, r = radius, center = true, $fn = 50);
    translate([0, 0, thick])     rotate([90, 90, 0]) cylinder(length, r = radius, center = true, $fn = 50);
    translate([0, 0, thick / 2])                         cube([2 * radius, length, thick], true);
  }
}

module clip(width, height, length, tol = 0) {
  tolen2       = tol * 2;
  cwidth     = (length / 10) + tol;
  cthickness = 1.5 + tol;
  clength    = 5;
  ccylinder  = 2 + tolen2;
  translate([0, 0, height - (cthickness - tolen2) / 2]) union() {
    cube([width + tolen2, cwidth, cthickness], true);
    translate([width / 2 - (cthickness - tolen2) / 2, 0, -clength / 2]) union() {
      cube([cthickness, cwidth, clength+tol], true);
      translate([-cthickness / 2, 0, -(clength / 2 - ccylinder / 2)]) rotate([90, 0, 0]) cylinder(cwidth, r = ccylinder / 2, center = true, $fs = 0.25);
    }
    translate([-(width / 2 - (cthickness - tolen2) / 2), 0, -clength / 2]) rotate([0, 0, 180]) union() {
      cube([cthickness, cwidth, clength + tol], true);
      translate([-cthickness / 2, 0, -(clength / 2 - ccylinder / 2)]) rotate([90, 0, 0]) cylinder(cwidth, r = ccylinder / 2, center = true, $fs = 0.25);
    }
  }
}

module hole(width, height, closed) {
  if (!closed) {
    translate([0, 0, height - 1.5]) rotate([0, 90, 0]) cylinder(width + 1, r = toothpick / 2, center = true, $fs = 0.25);
  }
}

module chain_link(width, length, height, under_angle, over_angle, inner_axis, closed, clip, chackle_kind, chackle_screw_dia, chackle_screw_dist, chackle_screw_angle) {
  thick  = min(2, max(1, 0.1 * width));
  radius = height / 2;
  len1   = max(length, 2 * (height + thick));
  len2   = len1 - (2 * radius);
  h1     = radius;
  under  = min(max_angle, under_angle);
  over   = min(max_angle, over_angle);

  if (whatToPrint != 2) {
    difference() {
      if (chackle_kind != 1) {
        outline(width, len1, height, radius, len2, under, chackle_kind);
      } else {
        outline(width, len1 * 2, height, radius, len2, under, chackle_kind);
      }
      if (chackle_kind != 2) {
        outcut(width, radius, len2, h1, thick, over,  inner_axis); //left side
      }
      middle(width, radius, len1 * 3, height, len2, h1, thick, over, under, inner_axis, closed);
      incut(width, radius, len2, h1, thick, over, inner_axis, chackle_kind);   //right side
      if (clip && !closed) {
        clip(width, height, len1, tolerance);
      } else {
        hole(width, height, closed);
      }
      chain_screwholes(width, length, height, under_angle, over_angle, inner_axis, closed, clip, chackle_kind, chackle_screw_dia, chackle_screw_dist, chackle_screw_angle);
      if (chackle_kind == 1) {
        //start chackle
        translate([0, -len1 / 1.2, h1 * 2.2]) rotate([10, 0, 0]) cube([width + 2, len1 * 1.5, h1 * 2], true);
      } else if (chackle_kind == 2) {
        //end chackle
        translate([0, len1 / 1.2, h1 * 2.2]) rotate([-10, 0, 0]) cube([width + 2, len1 * 1.5, h1 * 2], true);
      }
    }
  }
  if (whatToPrint != 1) {
    if (clip && !closed) {
      if (chackle_kind == 2) {
        translate([0, 2 + height + len1,     (len1 / 10) / 2]) rotate([90, 0, 0]) clip(width, height, len1);
      } else {
        translate([0, 2 + height + len1 / 2, (len1 / 10) / 2]) rotate([90, 0, 0]) clip(width, height, len1);
      }
    }
  }
}

module chain_screwholes(width, length, height, under_angle, over_angle, inner_axis, closed, clip, chackle_kind, chackle_screw_dia, chackle_screw_dist, chackle_screw_angle) {
  if (chackle_kind == 1) {
    translate([0, -length / 2, -height / 2]) {
      cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
      //translate([0, chackle_screw_dist, 0]) cylinder(height, chackle_screw_dia / 2, $fs = 0.25);
      rotate([0, 0, chackle_screw_angle]) translate([0, -chackle_screw_dist, 0]) cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
    }
  } else if (chackle_kind == 2) {
    translate([0, length / 2, -height / 2]) {
      cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
      //translate([0, chackle_screw_dist, 0]) cylinder(height, chackle_screw_dia / 2, $fs = 0.25);
      rotate([0, 0, -chackle_screw_angle])  translate([0, chackle_screw_dist, 0]) cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
    }
  } else if (chackle_kind == 3) {
    translate([0, length / 9, -height / 2]) {
      cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
      // rotate([0, 0, chackle_screw_angle]) 
      translate([0, chackle_screw_dist, 0]) cylinder(height, r = chackle_screw_dia / 2, $fs = 0.25);
    }
  }
}
