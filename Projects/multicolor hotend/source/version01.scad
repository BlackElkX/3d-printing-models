$fn = 256;

difference() {
  cylinder(36, 20, 20);
  translate([0, 0, -1]) cylinder(40, 2.4, 2.4);
  stiften(3, 7.3, 3.5);
}

module stiften(aantal, radius, stiftradius) {
  for (stift = [0 : aantal - 1]) {
    rotate([0, 0, (360 / aantal) * stift])
  translate([radius, 0, -1])
    cylinder(40, stiftradius, stiftradius);
  }
}