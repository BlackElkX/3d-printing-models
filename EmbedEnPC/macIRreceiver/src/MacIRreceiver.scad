rj11width  = 9.8;
rj11length = 12.24;
rj11height = 6.7;

rj11phWidth  = 15;
rj11phLength = 13.7;
rj11phHeight = 18.5;
rj11phLong1 =  2.38;
rj11phLong2 =  3.6;
rj11phWidth1 = 12.3;
rj11phWidth2 =  9;
rj11phHigh1  = 14.3;
rj11phHigh2  = 11.19;
rj11phLong   = 10.2;

/*translate([-50,  0, 0]) rj11placeholder();
translate([-50, 20, 0]) rj11();
translate([-50, 40, 0]) rj11plug();
translate([-90,  0, 0]) irmodule();*/
//demo();

difference() {
    cube([30, 45, 25]);
    translate([2,    40, 2]) cube([26,  3, 27]);
    translate([2,     2, 2]) cube([26,  4, 27]);
    translate([2,     2, 2]) cube([26, 41,  1]);
    translate([2,     2, 9]) cube([26, 41, 25]);
    translate([6,     2, 2]) cube([22, 41, 25]);
    translate([0,     0, 23]) deksel(0.2);
    translate([17.5, 10, 2]) rj11placeholder();
    translate([2,     3, 2.1]) irmodule();
}

translate([33, 0, 0]) deksel(0);

module deksel(extra) {
    color("cyan") {
        translate([2,  2, 0]) cube([26, 41, 2 + extra]);
        translate([0 - extra, 22, 0]) cube([30 + extra * 2,  4, 2 + extra]);
        translate([13, 0 - extra, 0]) cube([4,  45 + extra * 2, 2 + extra]);
    }
}

module demo() {
    translate([17.5, 10,  2])   rj11();
    translate([ 2,    3,  2.1]) irmodule();
    translate([-0,  0, 23])   deksel(0);
}

module rj11placeholder() {
    color("lime") {
          cube([rj11phLong1, rj11phWidth, rj11phHigh1]);
          translate([rj11phLong - rj11phLong2,         0, 0]) cube([rj11phLong2,  rj11phWidth,  rj11phHigh1]);
          translate([0, (rj11phWidth - rj11phWidth1) / 2, 0]) cube([rj11phLong,   rj11phWidth1, rj11phHigh1]);
          translate([0, (rj11phWidth - rj11phWidth1) / 2, 0]) cube([rj11phLength, rj11phWidth1, rj11phHigh2]);
          translate([0, (rj11phWidth - rj11phWidth2) / 2, 0]) cube([rj11phLong,   rj11phWidth2, rj11phHeight]);
    }
}

module rj11() {
    difference() {
        rj11placeholder();
        rj11plug();
    }
}

module rj11plug() {
    color("red") {
        translate([ 0.1, (rj11phWidth - rj11width) / 2, 4.5]) cube([rj11length + 10, rj11width, rj11height]);
        translate([-0.1, (rj11phWidth - 6.5) / 2, 1.5]) cube([rj11length - 2, 6.5, 5.2]);
        translate([-0.1, (rj11phWidth - 6.5) / 2, 3])   cube([rj11length + 10, 6.5, 5.2]);
        translate([-0.1, (rj11phWidth - 4.1) / 2, 1.5]) rotate([0, 3, 0]) cube([rj11length + 10, 4.1, 5.2]);
    }
}

module irmodule() {
    color("blue") {
        difference() {
            union() {
                cube([7, 25.8, 16.1]);
                cube([7, 38.1,  8]);
            }
            translate([-0.5, 4.59, 5.55]) rotate([0, 90, 0]) cylinder(d=1.9, h=10, $fn=64);
            translate([-0.5, 38.1 - 2.44, 3.97]) rotate([0, 90, 0]) cylinder(d=1.9, h=10, $fn=64);
        }
        translate([-5, 20, 12]) rotate([0, 90, 0]) cylinder(d=3.4, h=12, $fn=64);
    }
}