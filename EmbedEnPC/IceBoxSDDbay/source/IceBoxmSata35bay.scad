use <../../../Common/source/Honinggraad.scad>;

highprofile = false;
fillType    = 2;     //0=honey 1=full //2=beams
innerHole35 = 2.45;  //3.5
innerHole25 = 2.33;
outerHole25 = 2.92;
screwTop25  = 6.56;
screwDriver = 4.2;

difference() {
  hdd();
  translate([-0.001, -0.001, -0.001]) ssd(0, 100);
  translate([-0.001, -0.001,  6.918]) cube([69.92, 100.38, 6.92]);
  if (highprofile) {
    translate([8.5, 9.55, 10]) cube([85.52, 125.90, 24.47]);
  } else {
    translate([-1, -1, 10]) cube([160, 160, 50]);
  }
  if (fillType != 2) {
    translate([72, 13.1, 3.13]) rotate([0, 90, 0]) cylinder(d = screwDriver, h = 40, $fn = 64);
    translate([72, 90.1, 3.13]) rotate([0, 90, 0]) cylinder(d = screwDriver, h = 40, $fn = 64);
    //screwholder 2.5"
    translate([77, 13.1, 3.13]) cube([10, 10, 14], true);
    translate([77, 90.1, 3.13]) cube([10, 10, 14], true);
  } else {
    translate([77, 13.1, 3.13]) rotate([0, 90, 0]) cylinder(d = screwDriver, h = 40, $fn = 64);
    translate([77, 90.1, 3.13]) rotate([0, 90, 0]) cylinder(d = screwDriver, h = 40, $fn = 64);
  }
}

//if (system("render")) {
if ($preview) {
  color("white") ssd(0, 20, true);
  color("cyan")  screwholes(10);
  translate([0, 0, 10]) {
    color("blue") ssd(0, 20, true);
    color("lime") screwholes(10);
  }
}


module ssd(extraDia, extraLength, demo = false) {
  if (demo) {
    difference() {
      ssdDisk(extraDia,  extraLength);
      ssdHoles(extraDia, extraLength);
    }
  } else {
    ssdDisk(extraDia,  extraLength);
    ssdHoles(extraDia, extraLength);
  }
}

module ssdDisk(extraDia, extraLength) {
  cube([69.92, 100.38, 6.92]);
}

module ssdHoles(extraDia, extraLength) {
  translate([-5, 13.1, 3.13]) rotate([0, 90, 0]) cylinder(d = outerHole25 + extraDia, h = 40 + extraLength, $fn = 64);
  translate([-5, 90.1, 3.13]) rotate([0, 90, 0]) cylinder(d = outerHole25 + extraDia, h = 40 + extraLength, $fn = 64);

  translate([4.15, 13.1, -5]) cylinder(d = innerHole25, h = 10, $fn = 64);
  translate([4.15, 90.1, -5]) cylinder(d = innerHole25, h = 10, $fn = 64);

  translate([65.7, 13.1, -5]) cylinder(d = innerHole25, h = 10, $fn = 64);
  translate([65.7, 90.1, -5]) cylinder(d = innerHole25, h = 10, $fn = 64);
}

module hdd() {
  difference() {
    union() {
      if (fillType == 1) {
        cube([101.52, 145.34, 24.47]);
        //screwholders 2.5"
        translate([77, 13.1, 6]) cube([12, 12, 12], true);
        translate([77, 90.1, 6]) cube([12, 12, 12], true);
      } else if (fillType == 2) {
        difference() {
          cube([101.52, 145.34, 24.47]);
          xmax = 3;
          ymax = 6;
          for (x = [1 : xmax]) {
            xvalue = ((101.52 / xmax) * (x - 1));
            for (y = [1 : ymax]) {
              yvalue = ((145.34 / ymax) * (y - 1));
              translate([(2 * x) + xvalue, (2 * y) + yvalue, -1]) cube([(101.52 / xmax), (145.34 / ymax), 28]);
            }
          }
        }
      } else {
        honeycombround(0, 0, 101.52, 145.34, 24.47);
        //screwholders 2.5"
        translate([77, 13.1, 6]) cube([12, 12, 12], true);
        translate([77, 90.1, 6]) cube([12, 12, 12], true);
      }
      //translate([0, 0, 24.47]) rotate([-90, 0, 0]) honeycombround(0, 0, 101.52, 24.47, 145.34);
      translate([ 5,    127.985, 5]) cube([10, 10, 10], true);
      translate([96.52, 127.985, 5]) cube([10, 10, 10], true);
      translate([96.52,  69.985, 5]) cube([10, 10, 10], true);
      translate([96.52,  28.485, 5]) cube([10, 10, 10], true);
      difference() {
        cube([101.52, 145.34, 24.47]);
        translate([2, 2, -1]) cube([97.52, 141.34, 26.47]);
      }
      translate([69.92,   0,    0]) cube([ 2,    102.38, 10]);
      translate([0,     100.38, 0]) cube([71.92,   2,    10]);
      //screwplace 3.5 bottom
      translate([ 5,     40.805, 5]) cube([10, 10, 10], true);
      translate([ 5,     85.195, 5]) cube([10, 10, 10], true);
      if (fillType != 2) {
        translate([96.52,  40.805, 5]) cube([10, 10, 10], true);
        translate([96.135, 85.195, 5]) cube([10, 10, 10], true);
      } else {
        translate([96.52,  40.805, 5]) cube([10, 10, 10], true);
        translate([96.135, 85.195, 5]) cube([10, 10, 10], true);
      }
    }
    screwholes(10);
  }
}

module screwholes(depth) {
  //screwholes
  translate([-1,  28.485, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);
  translate([-1,  69.985, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);
  translate([-1, 127.985, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);

  translate([83 + depth,  28.485, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);
  translate([83 + depth,  69.985, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);
  translate([83 + depth, 127.985, 6.105]) rotate([0, 90, 0]) cylinder(d = innerHole35, h = depth, $fn = 64);
    
  //screw holes bottom side
  translate([ 3.095, 40.805, -5]) cylinder(d = innerHole35, h = 30, $fn = 64);
  translate([ 3.095, 85.195, -5]) cylinder(d = innerHole35, h = 30, $fn = 64);
  translate([98.135, 40.805, -5]) cylinder(d = innerHole35, h = 30, $fn = 64);
  translate([98.135, 85.195, -5]) cylinder(d = innerHole35, h = 30, $fn = 64);
}