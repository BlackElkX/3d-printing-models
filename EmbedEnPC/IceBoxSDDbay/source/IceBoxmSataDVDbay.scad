$fn = 64;

difference() {
  union() {
    cube([105.68, 2.6, 13.2]);
    translate([105.68, 0, 13.2 - 7.4])
      cube([23.83,  2.6,  7.4]);
    translate([2.6, 0, 0])
    cube([105.68 - 2.6, 127, 13.2 - 9.02]);
    
    cube([1.82, 85.7, 6.1]);
    translate([0, 0, 13.2 - 9.05])
      cube([1.82, 128, 9.05]);
    translate([105.68 - 1.82, 0, 0])
      cube([1.82, 128, 6.1]);
  }
  translate([-1, -1, -0.4])
    cube([105.68 + 2, 128 + 2, 2]);
}
/*
radius = 0.84 / 2;
color("green")
translate([2.6 + (10.3 + radius), 0, 13.2 - (3.06 + radius)])
  rotate([-90, 0, 0]) {
    cylinder(4, radius, radius);
    translate([73 - radius, 0, 0])
    cylinder(4, radius, radius);
}
*/
color("lime")
union() {
translate([2.6 + 5.88, 0, 13.2 - 2.28 - 4.27])
  cube([1.98, 4, 4.27]);

translate([(105.68 + 23.83) - (11.3 + 7.15), 0, 13.2 - 1.69 - 1.3])
  cube([7.15, 5.5, 1.3]);
}