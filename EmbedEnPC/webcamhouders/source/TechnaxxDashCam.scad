use <../../../Common/source/Honinggraad.scad>;

plaatbreedte = 61.75;
plaatlengte  = 60;
plaatdikte   =  3;
opening      = 15;
openingRand  =  4;
afkaprand    = 30;

haak();
plaat();
translate([-15, 0, 0]) voet(8, 8);
translate([-30, 0, 0]) voet(4, 8);
translate([-45, 0, 0]) voet(4, 8);

module plaat() {
  translate([15, 0, 0]) {
    difference() {
      union() {
        translate([0, 0, (plaatdikte / 2)]) honeycomb(plaatbreedte, plaatlengte, plaatdikte, opening, openingRand);
        randen(plaatdikte, opening, openingRand, 4, 90);
        cube([plaatbreedte, plaatdikte,  plaatdikte]);
        cube([plaatdikte,   plaatlengte, plaatdikte]);
        translate([0, plaatlengte - plaatdikte,  0]) cube([plaatbreedte, plaatdikte,  plaatdikte]);
        translate([plaatbreedte - plaatdikte, 0, 0]) cube([plaatdikte,   plaatlengte, plaatdikte]);
        translate([(opening * 2) + 2.5, 0, 0]) randen(plaatdikte, opening, openingRand, 1, 90);
        translate([(opening * 2) + 2.5, plaatlengte - (opening / 4), 0]) randen(plaatdikte, opening, openingRand, 1, 90);
      }    
      translate([(plaatbreedte - 8) / 2, plaatdikte + 2, -1]) rotate([90, 0, 0]) cube([8, 23, 2]);

      translate([2, plaatlengte - 2, -1]) rotate([90, 0, 0]) cube([4, 23, 2]);
      translate([(plaatbreedte - 8) / 2, plaatlengte - 2, -1]) rotate([90, 0, 0]) cube([8, 23, 2]);
      translate([plaatbreedte - 6, plaatlengte - 2, -1]) rotate([90, 0, 0]) cube([4, 23, 2]);

      difference() {
        translate([-(afkaprand / 2), -(afkaprand / 2), -5]) cube([plaatbreedte + afkaprand, plaatlengte + afkaprand, 10]);
        translate([ 0,  0, -6]) cube([plaatbreedte,     plaatlengte,     12]);
      }
    }
  }
}

module haak() {
  cube([8, 23, 2]);
  translate([0, (7.5 / 2) - (2.35 / 2),       2]) cube([8, 2.35, 2.4]);
  translate([0,                      0, 2 + 2.4]) cube([8,  7.5, 1.6]);
}

module randen(dikte, inner, border, qty, hoek) {
  offsetx = inner / 2.5;
  offsety = inner / 7;
  color("green")
  translate([offsetx, offsety, 0])
  rotate([0, 0, hoek]) {
    for (index = [0: qty - 1]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
}

module voet(breedte, nokbreedte) {
  cube([breedte, 34, 2]);
  translate([((breedte - nokbreedte) / 2), 4, 0]) cube([nokbreedte, 30, 4]);
}