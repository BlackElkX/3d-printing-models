use <../../../Common/source/TextAndLogos.scad>;

$fn = 64;

stickbreedte   = 14.00;
stickdikte     =  4.70;
stickhoogte    = 18.50;
breedte        = 20;
dikte          = 10;
hoogte         = 30;
dekselhoogte   = 12;
usbhoogte      = 10;
usbbreedte     = 13;
usbovergang    = 4;
dieptesiersels = 5.01;
figuurgrootte  = 0.059;
figuurdikte    = 0.02;


difference() {
  translate([0, 0, hoogte])
    rotate([0, 180, 0])
      usbstick(breedte, dikte, hoogte, stickbreedte, stickdikte, stickhoogte);
  translate([0, dieptesiersels, 15])
    fallingMan(figuurgrootte, figuurdikte, 60);
  translate([0, -dieptesiersels, 15])
    rotate([0, 0, 180])
      fallingMan(figuurgrootte, figuurdikte, 60);
}//*/

//fallingMan(figuurgrootte, figuurdikte, 0);

translate([40, 0, dekselhoogte + usbovergang]) rotate([0, 180, 0]) {
  difference() {
    union() {
      translate([0, 0, usbovergang])
        usbstick(breedte, dikte, dekselhoogte, usbbreedte, stickdikte, usbhoogte);
      usbstick(breedte, dikte, usbovergang+.01, stickbreedte, stickdikte, usbovergang+0.1);
    }
    translate([0, 5.1, 6.5])
      rotate([0, 0, 180])
        tekst("Myst", -10, 6.3);
    translate([0, -5.1, 9.5])
      tekst("Cyan", -10, 6.7);
    translate([0, -5.1, 2])
      tekst("Worlds", -10, 5.2);
  }
}
//*/

module usbstick(breedte, dikte, hoogte, stickbreedte, stickdikte, stickhoogte) {
  difference() {
    hull() {
      translate([-(breedte / 2), 0, 0])
        cylinder(hoogte, dikte / 2, dikte / 2);
      translate([ (breedte / 2), 0, 0])
        cylinder(hoogte, dikte / 2, dikte / 2);
    }
    translate([0, 0, -1]) stick(stickbreedte, stickdikte, stickhoogte + 1);
  }
}
module stick(breedte, dikte, hoogte) {
  translate([-(breedte / 2), -(dikte / 2), 0])
    cube([breedte, dikte, hoogte]);
}
