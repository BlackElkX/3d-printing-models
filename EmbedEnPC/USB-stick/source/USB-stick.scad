$fn=64;

/*
translate ([50, 0, 0]) {
  usbKeyCaseComplete(0,   0, 0, 35, 15, 12, 12, 12, 10, 2, 2);
  usbKeyCaseComplete(0,  40, 0, 45, 15, 12, 12, 12, 10, 2, 2);
  usbKeyCaseComplete(0,  80, 0, 55, 15, 12, 12, 12, 10, 2, 2);
  usbKeyCaseComplete(0, 120, 0, 65, 15, 12, 12, 12, 10, 2, 2);
}

translate ([90, 0, 0]) {
  cardan(10, 10, 5, 180, 2.5, 2.5, 25, 25);
}
//*/


//rotate(a = -90, v = [0, 0, 1])
//translate ([0, 0, 0]) {
//  bougieKlip(1);
//}


module cardan(rBol1, rBol2, rAs, lAs, rBolAs1, rBolAs2, lBolAs1, lBolAs2) {
  rotate(a = -90, v = [1, 0, 0])
    cylinder(r = rAs, h = lAs);
  sphere(rBol1);
  translate([0, lAs, 0])
    sphere(rBol2);
  translate([lBolAs1 / 2, 0, 0])
    rotate(a = -90, v = [0, 1, 0])
      cylinder(r = rBolAs1, h = lBolAs1);
  translate([lBolAs2 / 2, lAs, 0])
    rotate(a = -90, v = [0, 1, 0])
      cylinder(r = rBolAs2, h = lBolAs2);
}

module cardan2(rBol1, rBol2, rAs, lAs, rBolAs1, rBolAs2, lBolAs1, lBolAs2) {
  cardan(rBol1, rBol2, rAs, lAs, rBolAs1, rBolAs2, lBolAs1, lBolAs2);
}

module usbInside(xbase, ybase, zbase, length, width, height, thickness, bottomthickness) {
  radius = (width - thickness) / 2;
  translate([xbase, ybase, zbase + thickness])
    cylinder(r = radius, h = height);
  translate([xbase + length, ybase, zbase + thickness])
    cylinder(r = radius, h = height);
  translate([xbase, ybase - radius, zbase + thickness])
    cube([length, width - thickness, height]);
}

module usbKeyCase(xbase, ybase, zbase, length, width, height, thickness, bottomthickness, open) {
  difference() {
    usbInside(xbase, ybase, zbase, length, width, height, 0, 0);
    if (open) {
      usbInside(xbase, ybase, zbase - (thickness * 2), length, width, height + (thickness * 2), thickness, 0);
      echo("open");
    } else {
      usbInside(xbase, ybase, zbase, length, width, height, thickness, bottomthickness);
      echo("closed");
    }
  }
}

module usbKeyCaseComplete(x, y, z, boxheight, capheight, boxlength, boxwidth, flatlength, ringheight, bottomthickness, thickness) {
  usbKeyCase(x, y,                      z, flatlength, boxwidth,     boxheight,  2, bottomthickness, false);   //outside box
  usbKeyCase(x, y +  1 + boxwidth,      z, flatlength, boxwidth - 2, ringheight, 2, thickness,       true);    //inside box
  usbKeyCase(x, y + (1 + boxwidth) * 2, z, flatlength, boxwidth,     capheight,  2, thickness,       false);   //cap
}

module bougieKlip(dikte) {
  x01 = -30; x02 = -22; x03 = -20; x04 = -15; x09 = -52; x10 = -59; x11 = -61; x12 = -66;
  y01 =  10; y02 =  60; y03 =  75; y05 =  97; y06 = 128; y07 = 143; y08 = 191;
  difference() {
    clip();
    #translate([-4, -2, 0])
    cube([60, 195, 10]);
  }
  translate([2, 0, 10])
  difference() {
    polygon(points = [[x01, y01], [x02, y02], [x03, y03], [x04, y03], [x04, y05],
                      [x03, y05], [x03, y06], [x02, y07], [x01, y08],
                      [x09, y08], [x10, y07], [x11, y06], [x11, y05], [x12, y05],
                      [x12, y03], [x11, y03], [x10, y02], [x09, y01]]
           );
    {
      polygon(points = [[x01 - dikte, y01 + dikte], [x02 - dikte, y02 - dikte],
                        [-57 - dikte, y02 - dikte], [-50 - dikte, y01 + dikte]]);

      polygon(points = [[x01 - dikte, y08 - dikte], [x02 - dikte, y07 + dikte],
                        [-57 - dikte, y07 + dikte], [-50 - dikte, y08 - dikte]]);
/*
      polygon(points = [
                        [-24,  62], [x02,  77], [-17,  77], [-17, 95],
                        [x02,  95], [x02, y06], [-24, y07], 
                        [-57, y07], [x10, y06], [x10,  95], [-64, 95],
                        [-64,  77], [x10,  77], [-57,  62]
                       ]);
*/
    }
  }
}

module clip() {
  translate([25, 95, 10])
  rotate(a = 90, v = [0, 0, 1])
  scale([1, 1, 0.1])
  surface(file = "image2.png", center = true, invert = true);
}
