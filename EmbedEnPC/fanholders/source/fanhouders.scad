use <../../../Common/source/Honinggraad.scad>;

$fn   =  64;

modul = 120;   //breedte van een module
bhole = 105;   //afstand tussen de bevestigingsgaten
frand =   2;   //rand tot opening ventilator
prand =   6;   //rand rondom alles
space =   2;   //plaats tussen de modules
thick =   5;   //dikte van de houder
dhole =   5;   //diameter van de bevestigingsgaten
xqty  =   2;   //aantal modules naast elkaar
yqty  =   1;   //aantal modules boven elkaar

houder(modul, bhole, frand, prand, space, thick, dhole, xqty, yqty);

//hulplijnen();

module hulplijnen() {
  translate([-25, -25, thick + 1]) rotate([0, 0,  45]) cube([250, 1, 1]); //ok
  translate([ 95, 151, thick + 1]) rotate([0, 0, -45]) cube([250, 1, 1]);
  translate([ 95, -25, thick + 1]) rotate([0, 0,  45]) cube([250, 1, 1]); //ok
  translate([-25, 149, thick + 1]) rotate([0, 0, -45]) cube([250, 1, 1]);
}

module houder(modul, bhole, frand, prand, space, thick, dhole, xqty, yqty) {
  xhole = (modul - bhole) / 2;
  yhole = (modul - bhole) / 2;
  rhole = dhole / 2;

  xfan = modul / 2;
  yfan = modul / 2;
  rfan = (modul - (frand * 2)) / 2;
  
  l = (modul * xqty) + (space * (xqty + 1));
  b = (modul * yqty) + (space * (yqty + 1));

  xplaat = -prand/2;
  yplaat = -prand/2;
  lplaat = l + (prand * 2);
  bplaat = b + (prand * 2);
  
  difference() {
    union() {
      honeycombround(0, 0, l, b, thick);
      difference() {
        //cube([l, b, thick]);
        plaat(xplaat, yplaat, lplaat, bplaat, thick, 10);
        for (cx = [0 : (xqty - 1)]) {
          for (cy = [0 : (yqty - 1)]) {
            x = (cx * modul) + ((cx + 1) * space);
            y = (cy * modul) + ((cy + 1) * space);
            fan(x, y, xfan, yfan, rfan, thick);
          }
        }
      }
    }
    for (cx = [0 : (xqty - 1)]) {
      for (cy = [0 : (yqty - 1)]) {
        x = (cx * modul) + ((cx + 1) * space);
        y = (cy * modul) + ((cy + 1) * space);
        holes(x, y, xhole, yhole, rhole, thick);
      }
    }
    plaatgaten(xplaat, yplaat, lplaat, bplaat, thick, 2);
  }
  //plaatgaten(0, 0, l, b, thick, 2);
}

module fan(x, y, xfan, yfan, rfan, thick) {
  translate([x + xfan, y + yfan, -1])
    cylinder(thick + 2, rfan, rfan);
}

module holes(x, y, xhole, yhole, rhole, thick) {
  schroefGat(x + xhole, y + yhole, -1, thick + 2, rhole, thick-1, rhole*4, thick-0.5);
  schroefGat(x + xhole + bhole, y + yhole, -1, thick + 2, rhole, thick-1, rhole*4, thick-0.5);
  schroefGat(x + xhole + bhole, y + yhole + bhole, -1, thick + 2, rhole, thick-1, rhole*4, thick-0.5);
  schroefGat(x + xhole, y + yhole + bhole, -1, thick + 2, rhole, thick-1, rhole*4, thick-0.5);
}

module plaat(x, y, l, b, h, r) {
  translate([x, y, 0])
    hull() {
      translate([x, y, 0]) cylinder(h, r, r);
      translate([l, y, 0]) cylinder(h, r, r);
      translate([l, b, 0]) cylinder(h, r, r);
      translate([x, b, 0]) cylinder(h, r, r);
    }
}

module plaatgaten(x, y, l, b, h, r) {
  translate([x, y, -1]) {
    schroefGat(x, y, 0, h + 2, r, h-1, r*4, h-0.5);
    schroefGat(l, y, 0, h + 2, r, h-1, r*4, h-0.5);
    schroefGat(l, b, 0, h + 2, r, h-1, r*4, h-0.5);
    schroefGat(x, b, 0, h + 2, r, h-1, r*4, h-0.5);
  }
}


module schroefGat(schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ, hoogte, diameter, verzinkhoogte, verzinkdiameter, top) {
  translate([schroefGatPlaatsX, schroefgatPlaatsY, schroefGatPlaatsZ]) {
    faktor = 0.1;
    cylinder(hoogte, diameter, diameter, false);
    translate([0, 0, top])
    cylinder(verzinkhoogte + faktor, diameter, verzinkdiameter + faktor, false);
  }
}