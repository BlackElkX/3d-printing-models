use <../../../Common/source/Honinggraad.scad>;

hoogte      = 380;
breedte     = 330;
diepte      = 300;//285
extrahoogte =   0;//20
extradiepte =   0;
opening     =  15;//15
openingRand =   4;
dikte       =   5;
rand        =   5;

bovenkant(hoogte, breedte, diepte, extrahoogte, extradiepte, rand, dikte, opening, openingRand);
zijkant(diepte, breedte, diepte, extrahoogte, extradiepte, rand, dikte, opening, openingRand);
translate([hoogte-dikte, 0, 0])
zijkant(diepte, breedte, diepte, extrahoogte, extradiepte, rand, dikte, opening, openingRand);

module zijkant(hoogte, breedte, diepte, extrahoogte, extradiepte, rand, dikte, opening, openingRand) {
  overlap = 100;
  difference()
  {
    union() {
      translate([dikte / 2, -(opening + openingRand) + 5.5, dikte])
        rotate([0, -90, 0])
        honeycomb(hoogte + (overlap / 2), diepte + (overlap / 2), dikte, opening, openingRand);
//      design(dikte, opening, openingRand, hoogte, breedte);
//      randen(dikte, opening, openingRand, hoogte, breedte);
    }
//    translate([-(overlap / 2), -(overlap / 2), -dikte])
//      rand(hoogte + overlap + extrahoogte, diepte + overlap + extradiepte, dikte * 3, (overlap / 2) + rand/2);
  }
//  rand(hoogte + extrahoogte, diepte + extradiepte, dikte, rand);
}

//bovenkant
module bovenkant(hoogte, breedte, diepte, extrahoogte, extradiepte, rand, dikte, opening, openingRand) {
  overlap = 100;
  difference() {
    union() {
      translate([-(opening + openingRand) + 7, -(opening + openingRand) + 5.5, dikte / 2])
        honeycomb(hoogte + (overlap / 2), diepte + (overlap / 2), dikte, opening, openingRand);
      design(dikte, opening, openingRand, hoogte + extrahoogte, breedte);
      randen(dikte, opening, openingRand, hoogte + extrahoogte, breedte);
    }
    translate([-(overlap / 2), -(overlap / 2), -dikte])
      rand(hoogte + overlap + extrahoogte, diepte + overlap + extradiepte, dikte * 3, (overlap / 2) + rand / 2);
  }
  rand(hoogte + extrahoogte, diepte + extradiepte, dikte, rand);
}

module design(dikte, inner, border, hoogte, breedte) {
  color("yellow")
  translate([hoogte - (border / 1.4), breedte - ((inner + border) * 1.65), 0])
  rotate([0, 0, -150]) {
    for (index = [0: 25]) {//25
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  color("red")
  translate([(inner * 10) - 7, breedte - ((inner + border) * 2), 0])
  rotate([0, 0, -90]) {
    for (index = [0: 6]) {
      translate([index * ((inner + border)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  color("purple")
  translate([(inner * 14) + 2.5, breedte - ((inner + border) * 8) - 6.5, 0])
  rotate([0, 0, -30]) {
    for (index = [0: 9]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
}

module randen(dikte, inner, border, hoogte, breedte) {
  //bovenkant
  translate([(border / 3) * 2, breedte - ((inner + border) * 2) + (border / 2), 0])
  rotate([0, 0, 0]) {
    for (index = [0: (hoogte / ((inner + border) * 1.6))]) {
      translate([index * ((inner + border + 13.9)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  //onderkant
  translate([(border / 3) * 2, -((inner + border) / 2), 0])
  rotate([0, 0, 0]) {
    for (index = [0: (hoogte / ((inner + border) * 1.6))]) {
      translate([index * ((inner + border + 13.9)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  //linkerkant
  color("green")
  translate([-4.83,  breedte - ((inner + border) * 2) + (inner / 2) + 2, 0])
  rotate([0, 0, -90]) {
    for (index = [0: 15]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  //rechterkant
  color("blue")
  translate([hoogte - ((inner + border) / 3) + 0.47, breedte - ((inner + border) * 2) - (inner) - 2, 0])
  rotate([0, 0, -90]) {
    for (index = [0: 15]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
}

//algemeen
module rand(lengte, diepte, dikte, rand) {
  difference() {
    translate([0, 0, 0]) cube([lengte, diepte, dikte]);
color("red")
    translate([rand, rand, -1]) cube([lengte - (rand * 2), diepte - (rand * 2), dikte + 2]);
  }
}

