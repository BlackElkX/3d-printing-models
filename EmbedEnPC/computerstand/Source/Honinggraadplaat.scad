use <../../../Common/source/Honinggraad.scad>;

difference() {
  union() {
    translate([-45, -42, 2.5])
      honeycomb(480, 370, 5, 15, 4);
    design(5, 15, 4);
    randen(5, 15, 4);
  }
  translate([-50, -50, -5])
    rand(500, 400, 15, 55);
}

rand(400, 300, 5, 5);

module rand(lengte, diepte, dikte, rand) {
  difference() {
    translate([0, 0, 0]) cube([lengte, diepte, dikte]);
    translate([rand, rand, -1]) cube([lengte - (rand * 2), diepte - (rand * 2), dikte + 2]);
  }
}


module design(dikte, inner, border) {
  translate([(inner * 10) - 7, 302, 0])
  rotate([0, 0, -90]) {
    for (index = [0: 8]) {
      translate([index * ((inner + border)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  translate([393, 299, 0])
  rotate([0, 0, -150]) {
    for (index = [0: 25]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  translate([(inner * 14) + 2.5, 162.5, 0])
  rotate([0, 0, -30]) {
    for (index = [0: 11]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
}

module randen(dikte, inner, border) {
  translate([19, 294, 0])
  rotate([0, 0, 0]) {
    for (index = [0: 11]) {
      translate([index * ((inner + border + 13.9)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  translate([18.8, -9, 0])
  rotate([0, 0, 0]) {
    for (index = [0: 11]) {
      translate([index * ((inner + border + 13.9)), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  translate([-5, 292, 0])
  rotate([0, 0, -90]) {
    for (index = [0: 15]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
  translate([390, 292, 0])
  rotate([0, 0, -90]) {
    for (index = [0: 15]) {
      translate([index * (inner + border), inner / 2, 0])
        cylinder(dikte, (inner + border) / 2, (inner + border) / 2);
    }
  }
}
