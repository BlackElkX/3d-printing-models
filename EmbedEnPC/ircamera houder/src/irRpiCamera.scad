use <../../../Common/source/LegoBlocks.scad>;

$fn = 64;

height = 10;
rond   = false;

//translate([0, 40, 5]) houder(0.1, true, rond, height, true);
//translate([0,  0, (height - 2.99)]) rotate([0, 180, 0]) clip(0.2, height + 2);
translate([60, 0, 8]) legoplaat(0.1, height);
//plate(4, 4, 3, false, true);
//translate([0, 100, 5]) complete(0.1, height);

module complete(speling, hoogte) {
  clip(speling, hoogte + 2);
  houder(speling, true, rond, hoogte, true);
  camera(64, 0);
  legoplaat(speling, hoogte);
}

module camera(fnvalue, speling) {
  union() {
    color("orange") hull() {
      translate([-26.5, 0, 0]) cylinder(d = 20 + speling, h = 1 + speling);
      translate([ 26.5, 0, 0]) cylinder(d = 20 + speling, h = 1 + speling);
    }
    color("red") {
      translate([-26.5,  0.0, 1]) cylinder(d = 20 + speling, h = 14 + speling);
      translate([ 26.5,  0.0, 1]) cylinder(d = 20 + speling, h = 14 + speling);
    }
    color("darkblue") {
      translate([-16.0,  7.5, 1]) cylinder(d =  5 + speling, h =  9 + speling);
      translate([ 16.0, -7.5, 1]) cylinder(d =  5 + speling, h =  9 + speling);
    }
    color("black") {
      hull() {
        translate([-7, 0, 1]) cylinder(d = 4 + speling, h = 3 + speling);
        translate([ 7, 0, 1]) cylinder(d = 4 + speling, h = 3 + speling);
      }
      translate([-6, -6,  1])   cube([12 + speling, 12 + speling, 3 + speling]);
    }
    color("lightgrey") {
      translate([ 0,  0,  4])   cylinder(d = 12 + speling, h = 8.5 + speling);
      translate([ 0,  0, 17.5]) cylinder(d = 13 + speling, h = 4 + speling);
    }
    color("darkgrey") translate([  0,     0, 12.5]) cylinder(d = 11 + speling, h = 5 + speling);
    color("blue")     translate([-12.5, -10, -1])   cube([25 + speling, 25 + speling, 1 + speling]);
    color("green")    translate([-12.5,  10,  0])   cube([25 + speling,  5 + speling, 4 + speling]);
    color("white")    translate([ -8.0,  15,  0])   cube([16 + speling, 35 + speling, 2 + speling]);
    color("lime") {
      translate([-10.5, -7, -4]) cylinder(d = 4 + speling, h = 6 + speling, $fn = fnvalue);
      translate([-10.5,  7, -4]) cylinder(d = 4 + speling, h = 6 + speling, $fn = fnvalue);
      translate([ 10.5,  7, -4]) cylinder(d = 4 + speling, h = 6 + speling, $fn = fnvalue);
      translate([ 10.5, -7, -4]) cylinder(d = 4 + speling, h = 6 + speling, $fn = fnvalue);
    }
  }
}

module houder(speling, cutoff, round, hoogte, truehole = false) {
  difference() {
    color("pink") {
      if (round) {
        hull() {
          translate([-26.5, 0, -5]) cylinder(d = 26 + speling, h = hoogte);
          translate([ 26.5, 0, -5]) cylinder(d = 26 + speling, h = hoogte);
        }
      } else {
        translate([-40, -13, -5]) cube([80, 26, hoogte]);
      }
    }
    if (cutoff) {
      camera(64, speling);
      translate([-27, -(10 + speling), -2]) cube([54, 20 + speling * 2, hoogte]);
      if (truehole) {
        if (round) {
          translate([-26.5,  0.0, -6]) cylinder(d = 20 + speling, h = 14);
          translate([ 26.5,  0.0, -6]) cylinder(d = 20 + speling, h = 14);
        } else {
          translate([-26.5,  0.0, -6]) cube([20 + speling, 20 + speling, 14], center = true);
          translate([ 26.5,  0.0, -6]) cube([20 + speling, 20 + speling, 14], center = true);
        }
      }
    }
    translate([-15, -(13 + speling), -6]) cube([5, 2, 2]);
    translate([ 10, -(13 + speling), -6]) cube([5, 2, 2]);
    translate([ 10,  (11 + speling), -6]) cube([5, 2, 2]);
    translate([-15,  (11 + speling), -6]) cube([5, 2, 2]);
  }
}

module clip(speling, hoogte, cutoff = true) {
  lengte = 80;
  color("lightblue")
  difference() {
    translate([-(lengte / 2), -15, -4.99]) cube([lengte, 30, hoogte]);
    if (cutoff) {
      houder(speling, false, true, hoogte - 2);
      camera(64, speling * 10);
      verzakking = -7;
      translate([-16,           -12.7, verzakking]) cube([(lengte/2), 25.4, hoogte]);
      translate([-45, -(13 + speling), verzakking]) cube([30, 26 + (speling * 2), hoogte]);
      translate([ 15, -(13 + speling), verzakking]) cube([30, 26 + (speling * 2), hoogte]);
    }
  }
}

module legoplaat(speling, hoogte) {
  difference() {
    translate([-15, -17.5, -8]) cube([30, 35, hoogte + 7.5]);
    translate([-1, 0, 0]) clip(speling, hoogte + 2, false);
    translate([ 1, 0, 0]) clip(speling, hoogte + 2, false);
    translate([-16, -11, -5]) cube([32, 22, hoogte + 5]);
    camera(64, speling);
  }
  color("red")
  translate([-11.7, 21, -8]) plate(4, 6, 3, false, true);
}