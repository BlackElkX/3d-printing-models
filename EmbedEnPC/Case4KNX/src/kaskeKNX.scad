$fn = 64;

printX = 54;
printY = 42;
printZ = 26;
screwD =  3;
knxX   = 16;
knxY   = 44;
knxZ   = 29;
knxCon = 15;
connY  = 15;
rand   =  2;
montD1 = 17;
montD2 =  9.5;
extraZ =  20;
ledX   = 45;
ledY   = 28;

breedte = printX + (screwD * 2) + knxX;
diepte  = max(printY, knxY) + connY;
hoogte  = max(printZ, knxZ) + extraZ;

/*translate([0, 0, -(hoogte - rand - 15)]) difference() {
  union() {
    bakske(false);
    houders(2);
  }
  translate([-20, -10, -1]) cube([breedte * 2, diepte * 2, 15]);
}//*/
//bakske();
//houders();
translate([0, diepte + 20, 0]) deksel();


//translate([rand + knxX + (screwD * 2), rand, rand]) color("blue") pcb();

module bakske(knxButtons = true) {
  difference() {
    union() {
      hull() {
        translate([                      -7,                       montD2, 0]) cylinder(d = montD1, h = 5);
        translate([breedte + (rand * 2) + 7,                       montD2, 0]) cylinder(d = montD1, h = 5);
      }
      hull() {
        translate([breedte + (rand * 2) + 7, diepte + (rand * 2) - montD2, 0]) cylinder(d = montD1, h = 5);
        translate([                      -7, diepte + (rand * 2) - montD2, 0]) cylinder(d = montD1, h = 5);
      }
      cube([breedte + (rand * 2), diepte + (rand * 2), hoogte]);
    }
    translate([rand, rand, rand]) elec();
    translate([-0.1, rand, rand]) knxConection(knxButtons);
    vijzen();
    highVcon();
  }
}

module houders(extraDia = 0) {
  difference() {
    union() {
      difference() {
        translate([rand + knxX - (extraDia), rand, 0]) cube([(screwD + extraDia) * 2, diepte, hoogte]);
        translate([rand + knxX - (extraDia) -1, 10, 1]) cube([((screwD + extraDia) * 2) + 2, diepte + (rand * 2) - 20, hoogte]);
      }
      translate([rand + knxX + screwD,                          10, 0]) cylinder(d = (screwD + extraDia) * 2, h = hoogte);
      translate([rand + knxX + screwD,    diepte + (rand * 2) - 10, 0]) cylinder(d = (screwD + extraDia) * 2, h = hoogte);
      translate([breedte - rand - screwD, diepte + (rand * 2) - 10, 0]) cylinder(d = (screwD + extraDia) * 2, h = hoogte);
    }
    translate([0, 0, hoogte - 19]) houderVijzen(extraDia);
  }
  translate([knxX + (screwD * 2), 0, 0]) pcbHouder();
}

module led() {
  cylinder(d = 5, h = hoogte + 2);
}

module elec() {
  cube([breedte, diepte, hoogte]);
}

module pcb() {
  cube([printX, printY, printZ]);
}

module pcbHouder() {
  difference() {
    cube([printX + rand, printY + rand * 2, 10]);
    translate([rand, rand, 0]) pcb();
    color("cyan") translate([rand, printY, 0]) cube([10, 10, 11]);
  }
}

module vijzen() {
  translate([                      -7,                       montD2,    -1]) cylinder(d = 3, h = 10);
  translate([breedte + (rand * 2) + 7,                       montD2,    -1]) cylinder(d = 3, h = 10);
  translate([breedte + (rand * 2) + 7, diepte + (rand * 2) - montD2,    -1]) cylinder(d = 3, h = 10);
  translate([                      -7, diepte + (rand * 2) - montD2,    -1]) cylinder(d = 3, h = 10);
  translate([                      -7,                       montD2, 2.501]) cylinder(d1 = 3, d2 = 6, h = 2.5);
  translate([breedte + (rand * 2) + 7,                       montD2, 2.501]) cylinder(d1 = 3, d2 = 6, h = 2.5);
  translate([breedte + (rand * 2) + 7, diepte + (rand * 2) - montD2, 2.501]) cylinder(d1 = 3, d2 = 6, h = 2.5);
  translate([                      -7, diepte + (rand * 2) - montD2, 2.501]) cylinder(d1 = 3, d2 = 6, h = 2.5);
}

module knx() {
  cube([16, 44, 29]);
}

module knxConection(knxButtons) {
  cube([knxX, knxCon, hoogte + 2]);
  if (knxButtons) {
    translate([-1, 27, 20]) rotate([0, 90, 0]) cylinder(d = 3, h = rand * 2);
    translate([-1, 20, 20]) rotate([0, 90, 0]) cylinder(d = 5, h = rand * 2);
  }
}

module highVcon() {
  translate([rand + knxX + screwD + 7, diepte - 1, rand * 4]) {
    rotate([0, 90, 90]) {
      for (x = [0 : 5]) {
        translate([0, -(x * 7), 0]) cylinder(d = 3, h = 6);
      }
    }
  }
}

module houderVijzen(extraDia = 0) {
  color("blue") {
    translate([rand + knxX + screwD,                          10, 0]) cylinder(d = (screwD + extraDia), h = 20);
    translate([rand + knxX + screwD,    diepte + (rand * 2) - 10, 0]) cylinder(d = (screwD + extraDia), h = 20);
    translate([breedte - rand - screwD, diepte + (rand * 2) - 10, 0]) cylinder(d = (screwD + extraDia), h = 20);
  }
  //led opening
  color("red") translate([rand + ledX, rand + ledY, -1]) cylinder(d = 5, h = 20);
}

module deksel() {
  translate([breedte + (rand * 3), 0, rand * 7]) rotate([0, 180, 0]) difference() {
    translate([-2, -2, rand * 4]) cube([breedte + (rand * 4), diepte + (rand * 4), rand * 3]);
    cube([breedte + (rand * 2), diepte + (rand * 2), rand + 10]);
    translate([0, 0, 0]) houderVijzen();
  }
}
