translate([0, 0,  0]) support(true, 20, 200);
translate([0, 0, 50]) support(false, 20, 200);

module support(middensteun, width, length) {
  difference(){
    triangle(true, width, length);
    color("red") translate([-1, -10, -10]) triangle(false, width + 2, length);
  }
  cube([20, 10, 10]);

  if (middensteun) {
    translate([0, (length / 2) - 4, 0]) cube([20, 8, 21]);
  }
  translate([0, (length - 10), 0]) rotate([45, 0, 0]) cube([20, 8, 42]);
}

module triangle(stop, width, length) {
  difference() {
    cube([width, length, 50]);
    translate([-1, 7, 10]) rotate([10, 0, 0]) cube([width + 2, length + 10, 55]);
    if (stop) {
      translate([-1, -7, 20]) cube([width + 2, 30, 55]);
    }
  }
}
