$fn = 64;

webcamholder();


module webcamholder() {
  difference() {
    cube([50, 60, 4]);
    block(1);
    translate([-10,   80,    -1]) rotate([0,0,-31.5]) cube([100,   50, 10]);
    translate([  9.4, 33.25, -1]) rotate([0,0,-31.5]) cube([ 23.5, 20, 10]);
  }
  color("blue") translate([14, 55, 4]) rotate([0,0,-31.5]) {
    difference() {
      cube([42, 6, 3]);
    }
  }
}


module block(extra) {
  translate([- extra, - extra, -1]) {
    cube([20.12 + extra, 50.2 + extra, 10]);
    cube([40.00 + extra, 38.0 + extra, 10]);
  }
}

//sample
module sample() {
  difference() {
    union() {
      translate([ 0, 0, -2.78]) cube ([20, 50, 2.78]);
      translate([20, 0, -2.78]) cube ([20, 38, 2.78]);
    }
    holes();
  }
}
//holder
module version1() {
  difference() {
    color("lime")
      translate([0, 0, 2.78]) cube([50, 50, 2.5]);
    color("red") {
      translate([-1, -1, -1]) cube([21, 31, 10]);
      translate([19, 20, -1]) cube([10, 10, 10]);
    }
    holes();
    color("cyan")
      translate([80, 0, 0]) rotate([0, 0, 45]) cube([30, 102, 10]);
  }
}
module holes() {
  color("blue") {
    //translate([50, 10, -4]) cylinder(r = 2, h = 10);
    translate([25, 10, -4]) cylinder(r = 2, h = 10);
    translate([35, 10, -4]) cylinder(r = 2, h = 10);
    translate([10, 40, -4]) cylinder(r = 2, h = 10);
  }
}