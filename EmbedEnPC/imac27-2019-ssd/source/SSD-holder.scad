use <../../../Common/source/Honinggraad.scad>;

$fn = 64;

//thickness
outerThickness   = 3;
innerThickness   = 3;

//outer drive is 3.5 inch disk
outerDriveWidth  = 101.60;
outerDriveLength = 147.00;
outerDriveHeight =  17.80;  //26.10

//outer drive holes
outScrewholeRad  =   1.60;
outScrewholeTop  =   6.35;
outScrewholeY1   =  28.50;
outScrewholeY2   = 101.60 + outScrewholeY1;

//inner drive is 2.5 inch disk
innerDriveWidth  =  69.85;
innerDriveLength = 100.45;
innerDriveHeight =  15.00;

innerSupportWidth  =  14;//(innerDriveHeight / 3) * 2;
innerSupportOffset =   3;

//inner drive holes
inScrewholeRad   =   1.60;
inScrewholeXdist =  61.72;
inScrewholeX1    =  (innerDriveWidth - inScrewholeXdist) / 2;
inScrewholeX2    =  inScrewholeXdist + inScrewholeX1;
inScrewholeY1    =  14.00;
inScrewholeY2    =  90.60;

//inner drive offset
inOutX = 15;
inOutY = 40;

//vulling
difference() {
  union() {
    cube([outerThickness, outerDriveLength, outerDriveHeight]);
    translate([outerDriveWidth - outerThickness, 0, 0]) cube([outerThickness, outerDriveLength, outerDriveHeight]);
  }
  translate([-1, outScrewholeY1, outScrewholeTop]) rotate([0, 90, 0]) cylinder(outerDriveWidth + 2, outScrewholeRad, outScrewholeRad);
  translate([-1, outScrewholeY2, outScrewholeTop]) rotate([0, 90, 0]) cylinder(outerDriveWidth + 2, outScrewholeRad, outScrewholeRad);
}

difference() {
  union() {
    honeycombround(0, 0, outerDriveWidth, outerDriveLength, innerThickness);
    translate([inOutX, inOutY, 0]) {  
      translate([-innerSupportOffset, 0, 0]) cube([innerSupportWidth, innerDriveLength, innerThickness]);
      translate([innerDriveWidth - innerSupportWidth + innerSupportOffset, 0, 0]) cube([innerSupportWidth, innerDriveLength, innerThickness]);
    }
  }
  translate([inOutX, inOutY, 0]) {
    translate([inScrewholeX1, inScrewholeY1, -1]) cylinder(innerThickness + 2, inScrewholeRad, inScrewholeRad);
    translate([inScrewholeX1, inScrewholeY2, -1]) cylinder(innerThickness + 2, inScrewholeRad, inScrewholeRad);
    translate([inScrewholeX2, inScrewholeY1, -1]) cylinder(innerThickness + 2, inScrewholeRad, inScrewholeRad);
    translate([inScrewholeX2, inScrewholeY2, -1]) cylinder(innerThickness + 2, inScrewholeRad, inScrewholeRad);
  }
}

cube([outerDriveWidth, outerThickness, outerThickness]);
translate([0, outerDriveLength - outerThickness, 0]) cube([outerDriveWidth, outerThickness, outerThickness]);

/*
color("blue")
translate([inOutX, inOutY, outerThickness]) {
  cube([innerThickness, innerDriveLength, innerDriveHeight]);
  translate([innerDriveWidth - innerThickness, 0, 0]) cube([innerThickness, innerDriveLength, innerDriveHeight]);
}//*/

function fnOuterThickness()   = outerThickness;
function fnOuterDriveWidth()  = outerDriveWidth;
function fnOuterDriveLength() = outerDriveLength;
function fnOuterDriveHeight() = outerDriveHeight;
