use <../../../Common/source/Honinggraad.scad>;
use <SSD-holder.scad>;

hoek = 15;

hoogte =  22;//fnOuterDriveHeight()
dikte  =   3;//fnOuterThickness()
lengte =   ;//fnOuterDriveLength()
rand   =   6;//fnOuterThickness()
links  =  21;
rechts =  15;
midden =   4;

gatH   =   8;
gatL   = 101.6;// 12;
gatR   =   6;

color("yellow")
honeycombround(0, 0, hoogte, fnOuterDriveLength(), fnOuterThickness());

//onder
cube([fnOuterThickness(), fnOuterDriveLength(), fnOuterThickness()]);

//boven
translate([hoogte - fnOuterThickness(), 0, 0])
cube([fnOuterThickness(), fnOuterDriveLength(), fnOuterThickness()]);

//links
translate([0, fnOuterDriveLength() - (links), 0])
cube([hoogte - fnOuterThickness(), links, fnOuterThickness()]);

//rechts
cube([hoogte - fnOuterThickness(), rechts, fnOuterThickness()]);

//midden
translate([0, (fnOuterDriveLength() / 2) - (midden / 2), 0])
cube([hoogte - fnOuterThickness(), midden, fnOuterThickness()]);

translate([gatH, gatR, -1]) {
  cylinder(10, 1, 1);
  translate([0, gatL, 0]) cylinder(10, 1, 1);
}

/*
color("green") {
translate([-fnOuterThickness(), 0,  -(fnOuterThickness() / 2)])
rotate([0, 0, hoek])
cube([fnOuterDriveHeight() + fnOuterThickness() * 2, fnOuterThickness() * 2, fnOuterThickness() * 2]);

translate([-fnOuterThickness(), fnOuterDriveLength() - fnOuterThickness(), -(fnOuterThickness() / 2)])
rotate([0, 0, -hoek])
cube([fnOuterDriveHeight() + fnOuterThickness() * 2, fnOuterThickness() * 2, fnOuterThickness() * 2]);
}
//*/