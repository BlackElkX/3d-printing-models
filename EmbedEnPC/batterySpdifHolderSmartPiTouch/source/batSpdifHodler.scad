rounding    = 10;
height      = 10;

vesawidth   = 74.8;
vesadia     =  5.5;

spdifstart  = 10;
spdiflength = 76.67;
spdifwidth  = 12.72;
spdifdia    =  3;

battwidth   = 62;
battheight  =  5;
battlength  = 90;

vesamount1  = [0,                 0, 0];
vesamount2  = [vesawidth,         0, 0];
vesamount3  = [vesawidth, vesawidth, 0];
vesamount4  = [0,         vesawidth, 0];
spdifmount1 = [spdifstart,                        0, 0];
spdifmount2 = [spdifstart,              spdiflength, 0];
spdifmount3 = [spdifstart + spdifwidth,           0, 0];
spdifmount4 = [spdifstart + spdifwidth, spdiflength, 0];

$fn = 90;

difference() {
  color("red")
  hull() {
    translate(vesamount1) cylinder(height, d = rounding);
    translate(vesamount2) cylinder(height, d = rounding);
    translate(vesamount3) cylinder(height, d = rounding);
    translate(vesamount4) cylinder(height, d = rounding);
  }
  color("green")
  translate([0, 0, -0.5])
  hull() {
    translate([10,                         10, 0]) cylinder(height + 1, d = vesadia);
    translate([vesawidth - 10,             10, 0]) cylinder(height + 1, d = vesadia);
    translate([vesawidth - 10, vesawidth - 10, 0]) cylinder(height + 1, d = vesadia);
    translate([10,             vesawidth - 10, 0]) cylinder(height + 1, d = vesadia);
  }
  color("yellow")
  translate([0, 0, -0.5]) {
    translate(vesamount1) cylinder(height + 1, d = vesadia);
    translate(vesamount2) cylinder(height + 1, d = vesadia);
    translate(vesamount3) cylinder(height + 1, d = vesadia);
    translate(vesamount4) cylinder(height + 1, d = vesadia);
  }
  color("blue")
  hull() {
    translate([10,             battlength, 0]) rotate([90, 0, 0]) cylinder(100, d = 10);
    translate([vesawidth - 10, battlength, 0]) rotate([90, 0, 0]) cylinder(100, d = 10);
  }
  color("orange")
  hull() {
    translate([-10,             10, 0]) rotate([0, 90, 0]) cylinder(100, d = 10);
    translate([-10, vesawidth - 10, 0]) rotate([0, 90, 0]) cylinder(100, d = 10);
  }
  color("lime")
  translate([0, 0, -0.5]) {
    translate(spdifmount1) cylinder(height + 1, d = spdifdia);
    translate(spdifmount2) cylinder(height + 1, d = spdifdia);
    translate(spdifmount3) cylinder(height + 1, d = spdifdia);
    translate(spdifmount4) cylinder(height + 1, d = spdifdia);
  }
  color("cyan")
  translate([vesawidth - spdifstart - spdifstart - spdifwidth, 0, -0.5]) {
    translate(spdifmount1) cylinder(height + 1, d = spdifdia);
    translate(spdifmount2) cylinder(height + 1, d = spdifdia);
    translate(spdifmount3) cylinder(height + 1, d = spdifdia);
    translate(spdifmount4) cylinder(height + 1, d = spdifdia);
  }
}